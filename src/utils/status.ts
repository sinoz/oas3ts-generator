export function* generateAllStatusCodes() {
    for (let statusCode = 100; statusCode < 600; statusCode++) {
        yield statusCode;
    }
}

export function* takeStatusCodes(availableStatusCodes: Set<number>, statusKind: string) {
    let statusCodeOffset = 0;
    let statusCodeCount = 0;

    if (statusKind === "default") {
        statusCodeOffset = 100;
        statusCodeCount = 500;
    }
    else if (/^[1-5]XX$/.test(statusKind)) {
        statusCodeOffset = Number(statusKind[0]) * 100;
        statusCodeCount = 100;
    }
    else if (/^\d\d\d$/.test(statusKind)) {
        statusCodeOffset = Number(statusKind);
        statusCodeCount = 1;
    }
    else throw new Error(`bad statusKind '${statusKind}'`);

    for (
        let statusCode = statusCodeOffset;
        statusCode < statusCodeOffset + statusCodeCount;
        statusCode++
    ) {
        // if this statusCode is not avaialble, continue;
        if (!availableStatusCodes.delete(statusCode)) continue;

        yield statusCode;
    }
}

export function isStatusCode(statusKey: string) {
    return /[1-5][0-9][0-9]/.test(statusKey);
}

export function isStatusClass(statusKey: string) {
    return /[1-5]XX/.test(statusKey);
}

export function isStatusDefault(statusKey: string) {
    return statusKey === "default";
}

