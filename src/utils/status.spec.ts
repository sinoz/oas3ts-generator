import test from "tape-promise/tape.js";
import { takeStatusCodes } from "./status.js";

test("takeStatusCodes", async t => {
    const availableStatusCodes = new Set<number>([100, 200, 201, 300, 400, 500]);

    t.deepEqual(
        [...takeStatusCodes(availableStatusCodes, "100")],
        [100],
    );

    t.deepEqual(
        [...takeStatusCodes(availableStatusCodes, "2XX")],
        [200, 201],
    );

    t.deepEqual(
        [...takeStatusCodes(availableStatusCodes, "default")],
        [300, 400, 500],
    );

    t.deepEqual(
        [...availableStatusCodes],
        [],
    );

});
