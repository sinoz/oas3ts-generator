import { OpenAPIV3 } from "openapi-types";
import ts from "typescript";
import { PackageConfig } from "../index.js";
import { createClientClass, createClientCredentialsTypeDeclaration, generateRequestTypeDeclarations, generateResponseTypeDeclarations } from "../types/index.js";

export function createClientSourceFile(
    factory: ts.NodeFactory,
    document: OpenAPIV3.Document,
    config: PackageConfig,
    nameIndex: Record<string, string>,
) {
    const nodes = [
        ...emitImports(),
        createClientCredentialsTypeDeclaration(
            factory,
            document,
            nameIndex,
        ),
        createClientClass(
            factory,
            document,
            config,
            nameIndex,
        ),
        ...generateResponseTypeDeclarations(
            factory,
            document,
            config,
            nameIndex,
            "incoming",
        ),
        ...generateRequestTypeDeclarations(
            factory,
            document,
            config,
            nameIndex,
            "outgoing",
        ),
    ];

    const sourceFile = factory.createSourceFile(
        nodes,
        factory.createToken(ts.SyntaxKind.EndOfFileToken),
        ts.NodeFlags.None,
    );

    return sourceFile;

    function* emitImports() {
        yield factory.createImportDeclaration(
            undefined,
            undefined,
            factory.createImportClause(
                false,
                undefined,
                factory.createNamespaceImport(factory.createIdentifier("lib")),
            ),
            factory.createStringLiteral("@oas3/oas3ts-lib"),
        );

        // yield factory.createImportDeclaration(
        //     undefined,
        //     undefined,
        //     factory.createImportClause(
        //         false,
        //         undefined,
        //         factory.createNamedImports([factory.createImportSpecifier(
        //             undefined,
        //             factory.createIdentifier("Router"),
        //         )]),
        //     ),
        //     factory.createStringLiteral("goodrouter"),
        // );

        yield factory.createImportDeclaration(
            undefined,
            undefined,
            factory.createImportClause(
                false,
                undefined,
                factory.createNamespaceImport(factory.createIdentifier("shared")),
            ),
            factory.createStringLiteral("./shared.js"),
        );
        yield factory.createImportDeclaration(
            undefined,
            undefined,
            factory.createImportClause(
                false,
                undefined,
                factory.createNamespaceImport(factory.createIdentifier("internal")),
            ),
            factory.createStringLiteral("./client-internal.js"),
        );

    }

}

