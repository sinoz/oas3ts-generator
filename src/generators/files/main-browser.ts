import ts from "typescript";

export function createMainBrowserSourceFile(
    factory: ts.NodeFactory,
) {
    const nodes = [
        ...emitExports(),
    ];

    const sourceFile = factory.createSourceFile(
        nodes,
        factory.createToken(ts.SyntaxKind.EndOfFileToken),
        ts.NodeFlags.None,
    );

    return sourceFile;

    function* emitExports() {
        yield factory.createExportDeclaration(
            undefined,
            undefined,
            false,
            undefined,
            factory.createStringLiteral("./shared.js"),
        );

        yield factory.createExportDeclaration(
            undefined,
            undefined,
            false,
            undefined,
            factory.createStringLiteral("./client.js"),
        );

    }

}

