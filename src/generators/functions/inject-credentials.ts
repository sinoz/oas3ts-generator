import { OpenAPIV3 } from "openapi-types";
import ts from "typescript";
import { selectAllSecuritySchemes } from "../../selectors/index.js";
import { stringifyIdentifier, toReference } from "../../utils/index.js";

export function* generateInjectCredentialsDeclarations(
    factory: ts.NodeFactory,
    document: OpenAPIV3.Document,
    nameIndex: Record<string, string>,
) {
    for (
        const { securityScheme, securitySchemeObject, nameParts, pointerParts } of
        selectAllSecuritySchemes(document)
    ) {
        yield createFunctionDeclaration(
            securityScheme,
            securitySchemeObject,
            pointerParts,
        );
    }

    function createFunctionDeclaration(
        name: string,
        securitySchemeObject: OpenAPIV3.SecuritySchemeObject,
        pointerParts: string[],

    ) {
        const credentialTypeReference = toReference(pointerParts);
        const credentialTypeName = nameIndex[credentialTypeReference];
        const functionName = stringifyIdentifier([
            "inject",
            credentialTypeName,
        ]);

        const statements = [...emitStatements(name, securitySchemeObject)];

        return factory.createFunctionDeclaration(
            undefined,
            [
                factory.createToken(ts.SyntaxKind.ExportKeyword),
            ],
            undefined,
            functionName,
            undefined,
            [
                factory.createParameterDeclaration(
                    undefined,
                    undefined,
                    undefined,
                    "parameters",
                    undefined,
                    factory.createTypeReferenceNode(
                        factory.createQualifiedName(
                            factory.createIdentifier("lib"),
                            factory.createIdentifier("Parameters"),
                        ),
                    ),
                ),
                factory.createParameterDeclaration(
                    undefined,
                    undefined,
                    undefined,
                    "value",
                    undefined,
                    factory.createUnionTypeNode([
                        factory.createKeywordTypeNode(ts.SyntaxKind.UndefinedKeyword),
                        factory.createTypeReferenceNode(
                            factory.createQualifiedName(
                                factory.createIdentifier("shared"),
                                credentialTypeName,
                            ),
                        ),
                    ]),
                ),
            ],
            undefined,
            factory.createBlock(statements, true),
        );

    }

    function* emitStatements(
        parameterName: string,
        securitySchemeObject: OpenAPIV3.SecuritySchemeObject,
    ): Iterable<ts.Statement> {
        yield factory.createIfStatement(
            factory.createBinaryExpression(
                factory.createIdentifier("value"),
                factory.createToken(ts.SyntaxKind.EqualsEqualsToken),
                factory.createNull(),
            ),
            factory.createReturnStatement(),
        );

        switch (securitySchemeObject.type) {
            case "apiKey": {
                yield factory.createExpressionStatement(factory.createCallExpression(
                    factory.createPropertyAccessExpression(
                        factory.createIdentifier("lib"),
                        factory.createIdentifier("addParameter"),
                    ),
                    undefined,
                    [
                        factory.createIdentifier("parameters"),
                        createdEncodeParameterNameExpression(
                            securitySchemeObject,
                            factory.createStringLiteral(securitySchemeObject.name),
                        ),
                        createdEncodedParameterValueExpression(
                            securitySchemeObject,
                            factory.createIdentifier("value"),
                        ),
                    ],
                ));
                break;
            }
            case "http":
                switch (securitySchemeObject.scheme.toLowerCase()) {
                    case "basic":
                        yield factory.createVariableStatement(
                            undefined,
                            factory.createVariableDeclarationList(
                                [factory.createVariableDeclaration(
                                    factory.createIdentifier("parameterValue"),
                                    undefined,
                                    undefined,
                                    factory.createCallExpression(
                                        factory.createPropertyAccessExpression(
                                            factory.createIdentifier("lib"),
                                            factory.createIdentifier("stringifyBasicAuthorizationHeader"),
                                        ),
                                        undefined,
                                        [
                                            factory.createIdentifier("value"),
                                        ],
                                    ),
                                )],
                                ts.NodeFlags.Const,
                            ),
                        );
                        break;

                    default:
                        yield factory.createVariableStatement(
                            undefined,
                            factory.createVariableDeclarationList(
                                [factory.createVariableDeclaration(
                                    factory.createIdentifier("parameterValue"),
                                    undefined,
                                    undefined,
                                    factory.createCallExpression(
                                        factory.createPropertyAccessExpression(
                                            factory.createIdentifier("lib"),
                                            factory.createIdentifier("stringifyAuthorizationHeader"),
                                        ),
                                        undefined,
                                        [
                                            factory.createStringLiteral(
                                                securitySchemeObject.scheme,
                                            ),
                                            factory.createIdentifier("value"),
                                        ],
                                    ),
                                )],
                                ts.NodeFlags.Const,
                            ),
                        );
                        break;
                }
                yield factory.createExpressionStatement(factory.createCallExpression(
                    factory.createPropertyAccessExpression(
                        factory.createIdentifier("lib"),
                        factory.createIdentifier("addParameter"),
                    ),
                    undefined,
                    [
                        factory.createIdentifier("parameters"),
                        factory.createStringLiteral("authorization"),
                        createdEncodedParameterValueExpression(
                            securitySchemeObject,
                            factory.createIdentifier("parameterValue"),
                        ),
                    ],
                ));
                break;

            default: throw new Error("cannot happen");
        }
    }

    function createdEncodedParameterValueExpression(
        securitySchemeObject: OpenAPIV3.SecuritySchemeObject,
        parameterValueExpression: ts.Expression,
    ) {
        switch (securitySchemeObject.type) {
            case "apiKey": {
                switch (securitySchemeObject.in) {
                    case "query":
                        return factory.createCallExpression(
                            factory.createIdentifier("encodeURIComponent"),
                            undefined,
                            [
                                parameterValueExpression,
                            ],
                        );

                    default:
                        return parameterValueExpression;
                }
            }

            default:
                return parameterValueExpression;

        }

    }

    function createdEncodeParameterNameExpression(
        securitySchemeObject: OpenAPIV3.SecuritySchemeObject,
        parameterNameExpression: ts.Expression,
    ) {
        switch (securitySchemeObject.type) {
            case "apiKey":
                switch (securitySchemeObject.in) {
                    case "header":
                        return factory.createCallExpression(
                            factory.createPropertyAccessExpression(
                                parameterNameExpression,
                                "toLowerCase",
                            ),
                            undefined,
                            [],
                        );

                    default: return parameterNameExpression;
                }

            case "http":
                return factory.createStringLiteral("authorization");

            default: return parameterNameExpression;
        }
    }
}
