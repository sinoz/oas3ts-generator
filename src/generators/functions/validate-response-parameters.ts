import { OpenAPIV3 } from "openapi-types";
import ts from "typescript";
import { selectAllResponses, selectHeadersFromResponse } from "../../selectors/index.js";
import { stringifyIdentifier, stringifyType, toReference } from "../../utils/index.js";

export function* generateValidateResponseParametersDeclarations(
    factory: ts.NodeFactory,
    document: OpenAPIV3.Document,
    nameIndex: Record<string, string>,
) {
    for (
        const { pointerParts, responseObject } of
        selectAllResponses(document)
    ) {
        yield* createFunctionDeclaration(
            pointerParts,
            responseObject,
        );
    }

    function* createFunctionDeclaration(
        pointerParts: string[],
        responseObject: OpenAPIV3.ResponseObject,
    ) {
        const responseReference = toReference(pointerParts);
        const responseName = nameIndex[responseReference];
        const typeName = stringifyType([
            responseName,
            "response",
            "parameters",
        ]);

        const functionName = stringifyIdentifier(["validate", typeName]);

        const statements = [...emitStatementsFromResponse(
            pointerParts,
            responseObject,
        )];

        yield factory.createFunctionDeclaration(
            undefined,
            [
                factory.createToken(ts.SyntaxKind.ExportKeyword),
            ],
            factory.createToken(ts.SyntaxKind.AsteriskToken),
            functionName,
            undefined,
            [
                factory.createParameterDeclaration(
                    undefined,
                    undefined,
                    undefined,
                    "parameters",
                    undefined,
                    factory.createTypeReferenceNode(
                        typeName,
                    ),
                ),
                factory.createParameterDeclaration(
                    undefined,
                    undefined,
                    undefined,
                    "path",
                    undefined,
                    factory.createArrayTypeNode(
                        factory.createKeywordTypeNode(ts.SyntaxKind.StringKeyword),
                    ),
                    factory.createArrayLiteralExpression([]),
                ),
            ],
            undefined,
            factory.createBlock(statements, true),
        );

        const properties = [...emitPropertiesFromResponse(
            pointerParts,
            responseObject,
        )];
        yield factory.createExpressionStatement(factory.createBinaryExpression(
            factory.createPropertyAccessExpression(
                factory.createIdentifier(functionName),
                factory.createIdentifier("parameters"),
            ),
            factory.createToken(ts.SyntaxKind.EqualsToken),
            factory.createObjectLiteralExpression(properties, true),
        ));

    }

    function* emitStatementsFromResponse(
        pointerParts: string[],
        responseObject: OpenAPIV3.ResponseObject,
    ) {
        yield* emitStatementsFromResponseObject(
            factory,
            document,
            nameIndex,
            pointerParts,
            responseObject,
        );
    }

    function* emitStatementsFromResponseObject(
        factory: ts.NodeFactory,
        document: OpenAPIV3.Document,
        nameIndex: Record<string, string>,
        pointerParts: string[],
        responseObject: OpenAPIV3.ResponseObject,
    ) {
        for (
            const { header, headerObject, schemaObject, schemaPointerParts } of
            selectHeadersFromResponse(document, responseObject, pointerParts)
        ) {
            const parameterRequired = headerObject.required;
            const parameterName = stringifyIdentifier([header]);
            const parameterTypeReference = toReference(schemaPointerParts);
            const parameterTypeName = nameIndex[parameterTypeReference];

            const validateFn = stringifyIdentifier(["validate", parameterTypeName]);

            if (parameterRequired) {
                yield factory.createIfStatement(
                    factory.createBinaryExpression(
                        factory.createPropertyAccessExpression(
                            factory.createIdentifier("parameters"),
                            factory.createIdentifier(parameterName),
                        ),
                        factory.createToken(ts.SyntaxKind.EqualsEqualsToken),
                        factory.createNull(),
                    ),
                    factory.createBlock([
                        factory.createExpressionStatement(factory.createBinaryExpression(
                            factory.createIdentifier("yield"),
                            factory.createToken(ts.SyntaxKind.AsteriskToken),
                            factory.createArrayLiteralExpression(
                                [factory.createIdentifier("path")],
                            ),
                        )),
                        factory.createReturnStatement(),
                    ], true),
                );

                yield factory.createExpressionStatement(factory.createYieldExpression(
                    factory.createToken(ts.SyntaxKind.AsteriskToken),
                    factory.createCallExpression(
                        factory.createIdentifier(validateFn),
                        undefined,
                        [
                            factory.createPropertyAccessExpression(
                                factory.createIdentifier("parameters"),
                                factory.createIdentifier(parameterName),
                            ),
                            factory.createArrayLiteralExpression([
                                factory.createSpreadElement(factory.createIdentifier("path")),
                                factory.createStringLiteral(parameterName),
                            ]),
                        ],
                    ),
                ));

            }
        }
    }

    function* emitPropertiesFromResponse(
        pointerParts: string[],
        responseObject: OpenAPIV3.ResponseObject,
    ) {
        yield* emitPropertiesFromResponseObject(
            factory,
            document,
            nameIndex,
            pointerParts,
            responseObject,
        );
    }

    function* emitPropertiesFromResponseObject(
        factory: ts.NodeFactory,
        document: OpenAPIV3.Document,
        nameIndex: Record<string, string>,
        pointerParts: string[],
        responseObject: OpenAPIV3.ResponseObject,
    ) {
        for (
            const { header, headerObject, schemaObject, schemaPointerParts } of
            selectHeadersFromResponse(document, responseObject, pointerParts)
        ) {
            const parameterName = stringifyIdentifier([header]);
            const parameterTypeReference = toReference(schemaPointerParts);
            const parameterTypeName = nameIndex[parameterTypeReference];
            const validateFn = stringifyIdentifier(["validate", parameterTypeName]);

            yield factory.createPropertyAssignment(
                factory.createStringLiteral(parameterName),
                factory.createIdentifier(validateFn),
            );
        }
    }
}
