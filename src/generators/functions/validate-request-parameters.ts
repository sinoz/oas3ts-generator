import assert from "assert";
import { OpenAPIV3 } from "openapi-types";
import ts from "typescript";
import { selectAllOperations, selectParametersFromContainer } from "../../selectors/index.js";
import { stringifyIdentifier, stringifyType, toReference } from "../../utils/index.js";

export function* generateValidateRequestParametersDeclarations(
    factory: ts.NodeFactory,
    document: OpenAPIV3.Document,
    nameIndex: Record<string, string>,
) {
    for (
        const { pathObject, operationObject, path, method } of
        selectAllOperations(document)
    ) {
        yield* createFunctionDeclaration(
            pathObject,
            operationObject,
            path,
            method,
        );
    }

    function* createFunctionDeclaration(
        pathObject: OpenAPIV3.PathItemObject,
        operationObject: OpenAPIV3.OperationObject,
        path: string,
        method: OpenAPIV3.HttpMethods,
    ) {
        assert(operationObject.operationId);

        const typeName = stringifyType([
            operationObject.operationId,
            "request",
            "parameters",
        ]);
        const functionName = stringifyIdentifier(["validate", typeName]);

        const statements = [...emitStatementsFromOperation(
            pathObject,
            operationObject,
            path,
            method,
        )];

        yield factory.createFunctionDeclaration(
            undefined,
            [
                factory.createToken(ts.SyntaxKind.ExportKeyword),
            ],
            factory.createToken(ts.SyntaxKind.AsteriskToken),
            functionName,
            undefined,
            [
                factory.createParameterDeclaration(
                    undefined,
                    undefined,
                    undefined,
                    "parameters",
                    undefined,
                    factory.createTypeReferenceNode(
                        typeName,
                    ),
                ),
                factory.createParameterDeclaration(
                    undefined,
                    undefined,
                    undefined,
                    "path",
                    undefined,
                    factory.createArrayTypeNode(
                        factory.createKeywordTypeNode(ts.SyntaxKind.StringKeyword),
                    ),
                    factory.createArrayLiteralExpression([]),
                ),
            ],
            undefined,
            factory.createBlock(statements, true),
        );

        const properties = [...emitPropertiesFromOperation(
            pathObject,
            operationObject,
            path,
            method,
        )];
        yield factory.createExpressionStatement(factory.createBinaryExpression(
            factory.createPropertyAccessExpression(
                factory.createIdentifier(functionName),
                factory.createIdentifier("parameters"),
            ),
            factory.createToken(ts.SyntaxKind.EqualsToken),
            factory.createObjectLiteralExpression(properties, true),
        ));

    }

    function* emitStatementsFromOperation(
        pathObject: OpenAPIV3.PathItemObject,
        operationObject: OpenAPIV3.OperationObject,
        path: string,
        method: OpenAPIV3.HttpMethods,
    ) {
        yield* emitStatementsFromParametersContainer(
            factory,
            document,
            nameIndex,
            pathObject,
            ["paths", path],
        );
        yield* emitStatementsFromParametersContainer(
            factory,
            document,
            nameIndex,
            operationObject,
            ["paths", path, method],
        );
    }

    function* emitStatementsFromParametersContainer(
        factory: ts.NodeFactory,
        document: OpenAPIV3.Document,
        nameIndex: Record<string, string>,
        parametersContainer: OpenAPIV3.PathItemObject | OpenAPIV3.OperationObject,
        pointerParts: string[],
    ) {
        for (
            const { parameterObject, schemaObject, schemaPointerParts } of
            selectParametersFromContainer(document, parametersContainer, pointerParts)
        ) {
            const parameterRequired = parameterObject.required;
            const parameterName = stringifyIdentifier([parameterObject.name]);
            const parameterTypeReference = toReference(schemaPointerParts);
            const parameterTypeName = nameIndex[parameterTypeReference];

            const validateFn = stringifyIdentifier(["validate", parameterTypeName]);

            if (parameterRequired) {
                yield factory.createIfStatement(
                    factory.createBinaryExpression(
                        factory.createPropertyAccessExpression(
                            factory.createIdentifier("parameters"),
                            factory.createIdentifier(parameterName),
                        ),
                        factory.createToken(ts.SyntaxKind.EqualsEqualsToken),
                        factory.createNull(),
                    ),
                    factory.createBlock([
                        factory.createExpressionStatement(factory.createBinaryExpression(
                            factory.createIdentifier("yield"),
                            factory.createToken(ts.SyntaxKind.AsteriskToken),
                            factory.createArrayLiteralExpression(
                                [factory.createIdentifier("path")],
                            ),
                        )),
                        factory.createReturnStatement(),
                    ], true),
                );

                yield factory.createExpressionStatement(factory.createYieldExpression(
                    factory.createToken(ts.SyntaxKind.AsteriskToken),
                    factory.createCallExpression(
                        factory.createIdentifier(validateFn),
                        undefined,
                        [
                            factory.createPropertyAccessExpression(
                                factory.createIdentifier("parameters"),
                                factory.createIdentifier(parameterName),
                            ),
                            factory.createArrayLiteralExpression([
                                factory.createSpreadElement(factory.createIdentifier("path")),
                                factory.createStringLiteral(parameterName),
                            ]),
                        ],
                    ),
                ));
            }

        }
    }

    function* emitPropertiesFromOperation(
        pathObject: OpenAPIV3.PathItemObject,
        operationObject: OpenAPIV3.OperationObject,
        path: string,
        method: OpenAPIV3.HttpMethods,
    ) {
        yield* emitPropertiesFromParametersContainer(
            factory,
            document,
            nameIndex,
            pathObject,
            ["paths", path],
        );
        yield* emitPropertiesFromParametersContainer(
            factory,
            document,
            nameIndex,
            operationObject,
            ["paths", path, method],
        );
    }

    function* emitPropertiesFromParametersContainer(
        factory: ts.NodeFactory,
        document: OpenAPIV3.Document,
        nameIndex: Record<string, string>,
        parametersContainer: OpenAPIV3.PathItemObject | OpenAPIV3.OperationObject,
        pointerParts: string[],
    ) {
        for (
            const { parameterObject, schemaObject, schemaPointerParts } of
            selectParametersFromContainer(document, parametersContainer, pointerParts)
        ) {
            const parameterName = stringifyIdentifier([parameterObject.name]);
            const parameterTypeReference = toReference(schemaPointerParts);
            const parameterTypeName = nameIndex[parameterTypeReference];
            const validateFn = stringifyIdentifier(["validate", parameterTypeName]);

            yield factory.createPropertyAssignment(
                factory.createStringLiteral(parameterName),
                factory.createIdentifier(validateFn),
            );
        }
    }
}
