import * as fs from "fs";
import { OpenAPIV3 } from "openapi-types";
import * as path from "path";
import { PackageJson } from "type-fest";
import ts from "typescript";
import { selectAllRequestContentTypes, selectAllRequestParameters, selectAllResponseContentTypes, selectAllResponseParameters, selectAllResponses, selectAllSecuritySchemes, selectModel } from "../selectors/index.js";
import { generateNamePairs, packageInfo } from "../utils/index.js";
import * as files from "./files/index.js";

export interface PackageConfig {
    packageDir: string;
    packageName: string;
    requestTypes: string[];
    responseTypes: string[];
}

export function generatePackage(
    document: OpenAPIV3.Document,
    config: PackageConfig,
) {
    const banner = "/* eslint-disable */\n\n";

    const nameIndex = Object.fromEntries(
        generateNamePairs([
            ...selectModel(document, config),
            ...selectAllResponses(document),
            ...selectAllRequestParameters(document),
            ...selectAllResponseParameters(document),
            ...selectAllSecuritySchemes(document),
            ...selectAllRequestContentTypes(document, config.requestTypes),
            ...selectAllResponseContentTypes(document, config.responseTypes),
        ]),
    );

    const factory = ts.factory;
    const srcDir = path.join(config.packageDir, "src");

    const printer = ts.createPrinter({
        newLine: ts.NewLineKind.LineFeed,
    });

    fs.mkdirSync(config.packageDir, { recursive: true });
    fs.writeFileSync(
        path.join(config.packageDir, "tsconfig.json"),
        JSON.stringify(getTsconfigJson(), undefined, 2),
    );
    fs.writeFileSync(
        path.join(config.packageDir, "package.json"),
        JSON.stringify(getPackageJson(document, config.packageName), undefined, 2),
    );
    fs.writeFileSync(
        path.join(config.packageDir, ".npmrc"),
        getNpmrc().join("\n") + "\n",
    );

    fs.mkdirSync(srcDir, { recursive: true });

    fs.writeFileSync(
        path.join(srcDir, "main.ts"),
        banner +
        printer.printFile(
            files.createMainSourceFile(factory),
        ),
    );

    fs.writeFileSync(
        path.join(srcDir, "main-browser.ts"),
        banner +
        printer.printFile(
            files.createMainBrowserSourceFile(factory),
        ),
    );

    fs.writeFileSync(
        path.join(srcDir, "shared.ts"),
        banner +
        printer.printFile(
            files.createSharedSourceFile(factory, document, config, nameIndex),
        ),
    );

    fs.writeFileSync(
        path.join(srcDir, "client.ts"),
        banner +
        printer.printFile(
            files.createClientSourceFile(factory, document, config, nameIndex),
        ),
    );
    fs.writeFileSync(
        path.join(srcDir, "client-internal.ts"),
        banner +
        printer.printFile(
            files.createClientInternalSourceFile(factory, document, config, nameIndex),
        ),
    );

    fs.writeFileSync(
        path.join(srcDir, "server.ts"),
        banner +
        printer.printFile(
            files.createServerSourceFile(factory, document, config, nameIndex),
        ),
    );
    fs.writeFileSync(
        path.join(srcDir, "server-internal.ts"),
        banner +
        printer.printFile(
            files.createServerInternalSourceFile(factory, document, config, nameIndex),
        ),
    );
}

function getPackageJson(
    document: OpenAPIV3.Document,
    name: string,
): PackageJson {

    return {
        "name": name,
        "version": document.info.version,
        "description": document.info.description,
        "sideEffects": false,
        "type": "module",
        "main": "out/main.js",
        "types": "out/main.d.ts",
        "exports": {
            ".": {
                "node": "./out/main.js",
                "browser": "./out/main-browser.js",
            },
        },
        "files": [
            "out/*",
        ],
        "scripts": {
            "prepare": "tsc",
        },
        "author": document.info.contact?.name ?? "",
        "license": "ISC",
        "dependencies": withDependencies(
            "@oas3/oas3ts-lib",
        ),
        "devDependencies": withDependencies(
            "typescript",
        ),
    };
}

function withDependencies(...names: string[]) {
    return names.reduce(
        (o, name) => Object.assign(o, {
            [name]:
                packageInfo.dependencies?.[name] ??
                packageInfo.devDependencies?.[name],
        }),
        {},
    );
}

function getTsconfigJson() {
    return {
        "compilerOptions": {
            "target": "ES2020",
            "module": "ES2020",
            "moduleResolution": "node",
            "declaration": true,
            "sourceMap": true,
            "rootDir": "src",
            "outDir": "out",
            "importHelpers": true,
            "strict": true,
            "forceConsistentCasingInFileNames": true,
            "esModuleInterop": true,
            "skipLibCheck": true,
        },
        "include": [
            "src",
        ],
    };
}

function getNpmrc() {
    return [
        "@oas3:registry=https://gitlab.com/api/v4/packages/npm/",
    ];
}
