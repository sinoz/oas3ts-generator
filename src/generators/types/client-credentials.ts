import { OpenAPIV3 } from "openapi-types";
import ts from "typescript";
import { selectAllSecuritySchemes } from "../../selectors/index.js";
import { stringifyIdentifier } from "../../utils/name.js";
import { toReference } from "../../utils/reference.js";

export function createClientCredentialsTypeDeclaration(
    factory: ts.NodeFactory,
    document: OpenAPIV3.Document,
    nameIndex: Record<string, string>,
) {

    return factory.createInterfaceDeclaration(
        undefined,
        [
            factory.createToken(ts.SyntaxKind.ExportKeyword),
        ],
        "ClientCredentials",
        undefined,
        undefined,
        [...emitPropertySignatures()],
    );

    function* emitPropertySignatures() {
        const securitySchemes = [...selectAllSecuritySchemes(document)];

        for (const { securityScheme, pointerParts } of securitySchemes) {
            const securitySchemeTypeReference = toReference(pointerParts);
            const securitySchemeTypeName = nameIndex[securitySchemeTypeReference];

            yield factory.createPropertySignature(
                undefined,
                stringifyIdentifier([securityScheme]),
                factory.createToken(ts.SyntaxKind.QuestionToken),
                factory.createTypeReferenceNode(factory.createQualifiedName(
                    factory.createIdentifier("shared"),
                    factory.createIdentifier(securitySchemeTypeName),
                )),
            );
        }
    }

}
