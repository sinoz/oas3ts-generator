import assert from "assert";
import { OpenAPIV3 } from "openapi-types";
import ts from "typescript";
import { selectAllOperations } from "../../selectors/index.js";
import { stringifyType } from "../../utils/index.js";

export function* generateOperationHandlerTypeDeclarations(
    factory: ts.NodeFactory,
    document: OpenAPIV3.Document,
) {
    for (
        const { operationObject, method, path } of
        selectAllOperations(document)
    ) {
        yield createHandlerType(
            operationObject,
        );
    }

    function createHandlerType(
        operationObject: OpenAPIV3.OperationObject,
    ) {
        assert(operationObject.operationId);

        const handlerTypeName = stringifyType([operationObject.operationId, "operation", "handler"]);

        const outgoingResponseTypeName = stringifyType([operationObject.operationId, "outgoing", "response"]);
        const incomingRequestTypeName = stringifyType([operationObject.operationId, "incoming", "request"]);
        const acceptContentTypesTypeName = stringifyType([operationObject.operationId, "accept", "types"]);
        const authorizationTypeName = stringifyType([operationObject.operationId, "authorization"]);

        const functionType = factory.createFunctionTypeNode(
            undefined,
            [
                factory.createParameterDeclaration(
                    undefined,
                    undefined,
                    undefined,
                    factory.createIdentifier("incomingRequest"),
                    undefined,
                    factory.createTypeReferenceNode(
                        incomingRequestTypeName,
                    ),
                    undefined,
                ),
                factory.createParameterDeclaration(
                    undefined,
                    undefined,
                    undefined,
                    factory.createIdentifier("authorization"),
                    undefined,
                    factory.createTypeReferenceNode(
                        authorizationTypeName,
                        [
                            factory.createTypeReferenceNode(
                                "Authorization",
                            ),
                        ],
                    ),
                    undefined,
                ),
                factory.createParameterDeclaration(
                    undefined,
                    undefined,
                    undefined,
                    factory.createIdentifier("acceptTypes"),
                    undefined,
                    factory.createArrayTypeNode(
                        factory.createTypeReferenceNode(
                            factory.createQualifiedName(
                                factory.createIdentifier("shared"),
                                acceptContentTypesTypeName,
                            ),
                        ),
                    ),
                    undefined,
                ),
            ],
            factory.createTypeReferenceNode(
                factory.createIdentifier("Promisable"),
                [
                    factory.createTypeReferenceNode(
                        outgoingResponseTypeName,
                    ),
                ],
            ),
        );

        return factory.createTypeAliasDeclaration(
            undefined,
            [
                factory.createToken(ts.SyntaxKind.ExportKeyword),
            ],
            handlerTypeName,
            [
                factory.createTypeParameterDeclaration(
                    "Authorization",
                    factory.createTypeReferenceNode("ServerAuthorization"),
                    factory.createTypeReferenceNode("ServerAuthorization"),
                ),
            ],
            functionType,
        );
    }

}
