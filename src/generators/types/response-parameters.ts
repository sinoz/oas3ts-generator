import { OpenAPIV3 } from "openapi-types";
import ts from "typescript";
import { selectAllResponses, selectHeadersFromResponse } from "../../selectors/index.js";
import { stringifyIdentifier, stringifyType, toReference } from "../../utils/index.js";

export function* generateResponseParametersTypeDeclarations(
    factory: ts.NodeFactory,
    document: OpenAPIV3.Document,
    nameIndex: Record<string, string>,
) {
    for (
        const { responseObject, pointerParts } of
        selectAllResponses(document)
    ) {
        yield* emitFromResponse(
            responseObject,
            pointerParts,
        );
    }

    function* emitFromResponse(
        responseObject: OpenAPIV3.ResponseObject,
        pointerParts: string[],
    ) {
        const responseReference = toReference(pointerParts);
        const responseName = nameIndex[responseReference];
        const typeName = stringifyType([
            responseName,
            "response",
            "parameters",
        ]);

        const members = [
            ...emitMembersFromResponseObject(
                factory,
                document,
                nameIndex,
                responseObject,
                pointerParts,
            ),
        ];

        yield factory.createInterfaceDeclaration(
            undefined,
            [
                factory.createToken(ts.SyntaxKind.ExportKeyword),
            ],
            typeName,
            undefined,
            undefined,
            members,
        );
    }

    function* emitMembersFromResponseObject(
        factory: ts.NodeFactory,
        document: OpenAPIV3.Document,
        nameIndex: Record<string, string>,
        responseObject: OpenAPIV3.ResponseObject,
        pointerParts: string[],
    ) {
        for (
            const { header, headerObject, schemaObject, schemaPointerParts } of
            selectHeadersFromResponse(document, responseObject, pointerParts)
        ) {
            const parameterRequired = headerObject.required;
            const parameterName = stringifyIdentifier([header]);
            const parameterTypeReference = toReference(schemaPointerParts);
            const parameterTypeName = nameIndex[parameterTypeReference];

            yield factory.createPropertySignature(
                undefined,
                parameterName,
                parameterRequired ?
                    undefined :
                    factory.createToken(ts.SyntaxKind.QuestionToken),
                factory.createTypeReferenceNode(
                    parameterTypeName,
                ),
            );
        }
    }

}
