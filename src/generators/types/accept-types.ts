import assert from "assert";
import { OpenAPIV3 } from "openapi-types";
import ts from "typescript";
import { selectAllOperations, selectResponseContentTypes } from "../../selectors/index.js";
import { stringifyType } from "../../utils/index.js";
import { PackageConfig } from "../index.js";

export function* generateAcceptTypesTypeDeclarations(
    factory: ts.NodeFactory,
    document: OpenAPIV3.Document,
    config: PackageConfig,
) {
    for (
        const { operationObject } of
        selectAllOperations(document)
    ) {
        yield* emitFromOperation(operationObject);
    }

    function* emitFromOperation(
        operationObject: OpenAPIV3.OperationObject,
    ) {
        assert(operationObject.operationId);

        const typeName = stringifyType([operationObject.operationId, "accept", "types"]);

        const responseTypeSet = selectResponseContentTypes(
            document, config.responseTypes, operationObject,
        );

        yield factory.createTypeAliasDeclaration(
            undefined,
            [
                factory.createToken(ts.SyntaxKind.ExportKeyword),
            ],
            typeName,
            undefined,
            responseTypeSet.size > 0 ?
                factory.createUnionTypeNode(
                    [...responseTypeSet].map(type => factory.createLiteralTypeNode(
                        factory.createStringLiteral(type),
                    )),
                ) :
                factory.createKeywordTypeNode(ts.SyntaxKind.NeverKeyword),
        );
    }

}
