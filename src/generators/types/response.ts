import assert from "assert";
import { OpenAPIV3 } from "openapi-types";
import ts from "typescript";
import { selectAllOperations, selectContentTypesFromContentContainer, selectReponsesFromOperation } from "../../selectors/index.js";
import { generateAllStatusCodes, getContentKind, resolvePointerParts, stringifyType, takeStatusCodes, toReference } from "../../utils/index.js";
import { PackageConfig } from "../index.js";

export function* generateResponseTypeDeclarations(
    factory: ts.NodeFactory,
    document: OpenAPIV3.Document,
    config: PackageConfig,
    nameIndex: Record<string, string>,
    direction: "incoming" | "outgoing",
) {
    for (const { operationObject, path, method } of selectAllOperations(document)) {
        yield createFromOperation(
            operationObject,
            path,
            method,
        );
    }

    function createFromOperation(
        operationObject: OpenAPIV3.OperationObject,
        path: string,
        method: OpenAPIV3.HttpMethods,
    ) {
        assert(operationObject.operationId);

        const typeName = stringifyType([
            operationObject.operationId,
            direction,
            "response",
        ]);

        const pointerParts = ["paths", path, method];

        return factory.createTypeAliasDeclaration(
            undefined,
            [
                factory.createToken(ts.SyntaxKind.ExportKeyword),
            ],
            typeName,
            undefined,
            factory.createUnionTypeNode([...emitResponseTypes(
                operationObject,
                pointerParts,
            )]),
        );
    }

    function* emitResponseTypes(
        operationObject: OpenAPIV3.OperationObject,
        pointerParts: string[],
    ) {
        assert(operationObject.operationId);

        const statusSet = new Set(generateAllStatusCodes());

        for (
            const { statusKind, responseObject, responsePointerParts } of
            selectReponsesFromOperation(document, operationObject, pointerParts)
        ) {
            const statusses = [...takeStatusCodes(statusSet, statusKind)];

            const responseReference = toReference(responsePointerParts);
            const responseTypeName = nameIndex[responseReference];

            const parametersTypeName = stringifyType([
                responseTypeName,
                "response",
                "parameters",
            ]);

            const contentTypeEntries = [...selectContentTypesFromContentContainer(
                config.responseTypes,
                responseObject,
                responsePointerParts,
            )];

            if (contentTypeEntries.length === 0) {
                if (direction === "outgoing") {
                    yield createTypeNode(
                        [],
                        statusses,
                        null,
                        null,
                        parametersTypeName,
                        true,
                    );
                }

                yield createTypeNode(
                    [],
                    statusses,
                    null,
                    null,
                    parametersTypeName,
                    false,
                );
            }
            else {
                if (direction === "outgoing") {
                    const [defaultContentTypeEntry] = contentTypeEntries;
                    const {
                        contentType: defaultContentType,
                        mediaTypeObject: defaultMediaTypeObject,
                    } = defaultContentTypeEntry;
                    yield createTypeNode(
                        [...responsePointerParts, "content", defaultContentType],
                        statusses,
                        defaultContentType,
                        defaultMediaTypeObject,
                        parametersTypeName,
                        true,
                    );
                }
            }

            for (const { contentType, mediaTypeObject } of contentTypeEntries) {
                yield createTypeNode(
                    [...responsePointerParts, "content", contentType],
                    statusses,
                    contentType,
                    mediaTypeObject,
                    parametersTypeName,
                    false,
                );
            }
        }

    }

    function createTypeNode(
        pointerParts: string[],
        statusses: Array<number>,
        contentType: string | null,
        mediaTypeObject: OpenAPIV3.MediaTypeObject | null,
        parametersTypeName: string,
        isDefault: boolean,
    ) {
        const statusTypeNode = factory.createUnionTypeNode(
            statusses.map(status => factory.createLiteralTypeNode(
                factory.createNumericLiteral(status),
            )),
        );

        const contentKind = getContentKind(contentType);

        const parametersTypeNode = factory.createTypeReferenceNode(
            factory.createQualifiedName(
                factory.createIdentifier("shared"),
                parametersTypeName,
            ),
        );

        const contentTypeTypeNode = factory.createLiteralTypeNode(
            factory.createStringLiteral(contentType ?? ""),
        );

        const schemaPointerParts = resolvePointerParts(
            [...pointerParts, "schema"],
            mediaTypeObject?.schema,
        );

        const modelReference = schemaPointerParts && toReference(schemaPointerParts);
        const modelName = modelReference && nameIndex[modelReference];

        const bodyModelTypeNode = modelName && factory.createTypeReferenceNode(
            factory.createQualifiedName(
                factory.createIdentifier("shared"),
                modelName,
            ),
        );

        const typeNameParts = [direction, contentKind, "response"];
        if (isDefault) typeNameParts.push("default");
        const typeName = stringifyType(typeNameParts);

        switch (contentKind) {
            case "empty":
                return factory.createTypeReferenceNode(
                    factory.createQualifiedName(
                        factory.createIdentifier("lib"),
                        typeName,
                    ),
                    [
                        statusTypeNode,
                        parametersTypeNode,
                    ],
                );

            case "stream":
            case "text":
                return factory.createTypeReferenceNode(
                    factory.createQualifiedName(
                        factory.createIdentifier("lib"),
                        typeName,
                    ),
                    isDefault ?
                        [
                            statusTypeNode,
                            parametersTypeNode,
                        ] :
                        [
                            statusTypeNode,
                            parametersTypeNode,
                            contentTypeTypeNode,
                        ],
                );

            case "json":
            case "form":
                assert(bodyModelTypeNode);

                return factory.createTypeReferenceNode(
                    factory.createQualifiedName(
                        factory.createIdentifier("lib"),
                        typeName,
                    ),
                    isDefault ?
                        [
                            statusTypeNode,
                            parametersTypeNode,
                            bodyModelTypeNode,
                        ] :
                        [
                            statusTypeNode,
                            parametersTypeNode,
                            contentTypeTypeNode,
                            bodyModelTypeNode,
                        ],
                );

            default: assert.fail();
        }

    }

}
