import assert from "assert";
import { OpenAPIV3 } from "openapi-types";
import ts from "typescript";
import { selectModel, selectPropertiesFromSchema, selectSchemaType } from "../../selectors/index.js";
import { resolvePointerParts, toReference } from "../../utils/index.js";
import { PackageConfig } from "../index.js";

export function* generateModelTypeDeclarations(
    factory: ts.NodeFactory,
    document: OpenAPIV3.Document,
    config: PackageConfig,
    nameIndex: Record<string, string>,
) {
    for (
        const { schemaObject, pointerParts } of
        selectModel(document, config)
    ) {
        yield* emitFromSchema(
            pointerParts,
            schemaObject,
        );
    }

    function* emitFromSchema(
        basePointerParts: Array<string>,
        baseObject: OpenAPIV3.SchemaObject,
    ) {
        const baseReference = toReference(basePointerParts);
        const baseName = nameIndex[baseReference];

        if (baseObject.oneOf) {
            yield factory.createTypeAliasDeclaration(
                undefined,
                [
                    factory.createToken(ts.SyntaxKind.ExportKeyword),
                ],
                baseName,
                undefined,
                factory.createUnionTypeNode(
                    Object.entries(baseObject.oneOf).map(([sub, maybeSubSchemaObject]) => {
                        const subPointerParts = resolvePointerParts(
                            [...basePointerParts, "oneOf", sub],
                            maybeSubSchemaObject,
                        );
                        assert(subPointerParts);
                        const subReference = toReference(subPointerParts);
                        const subTypeName = nameIndex[subReference];

                        return factory.createTypeReferenceNode(subTypeName);
                    }),
                ),
            );
            return;
        }

        if (baseObject.allOf) {
            yield factory.createTypeAliasDeclaration(
                undefined,
                [
                    factory.createToken(ts.SyntaxKind.ExportKeyword),
                ],
                baseName,
                undefined,
                factory.createIntersectionTypeNode(
                    Object.entries(baseObject.allOf).map(([sub, maybeSubSchemaObject]) => {
                        const subPointerParts = resolvePointerParts(
                            [...basePointerParts, "allOf", sub],
                            maybeSubSchemaObject,
                        );
                        assert(subPointerParts);
                        const subReference = toReference(subPointerParts);
                        const subTypeName = nameIndex[subReference];

                        return factory.createTypeReferenceNode(subTypeName);
                    }),
                ),
            );
            return;
        }

        switch (selectSchemaType(baseObject)) {
            case "object": {
                if (baseObject.properties) {
                    const requiredSet = new Set(baseObject.required ?? []);
                    const propertySignatures = new Array<ts.PropertySignature>();

                    for (
                        const { property, propertySchemaPointerParts } of
                        selectPropertiesFromSchema(document, baseObject, basePointerParts)
                    ) {

                        const propertyReference = toReference(propertySchemaPointerParts);
                        const propertyTypeName = nameIndex[propertyReference];

                        const propertySignature = factory.createPropertySignature(
                            undefined,
                            factory.createStringLiteral(property),
                            requiredSet.has(property) ?
                                undefined :
                                factory.createToken(ts.SyntaxKind.QuestionToken),
                            factory.createTypeReferenceNode(
                                propertyTypeName,
                            ),
                        );
                        propertySignatures.push(propertySignature);
                    }

                    yield factory.createInterfaceDeclaration(
                        undefined,
                        [
                            factory.createToken(ts.SyntaxKind.ExportKeyword),
                        ],
                        baseName,
                        undefined,
                        undefined,
                        propertySignatures,
                    );
                    break;
                }

                if (typeof baseObject.additionalProperties === "object") {
                    const subPointerParts = resolvePointerParts(
                        [...basePointerParts, "additionalProperties"],
                        baseObject.additionalProperties,
                    );
                    assert(subPointerParts);
                    const subReference = toReference(subPointerParts);
                    const subTypeName = nameIndex[subReference];

                    yield factory.createTypeAliasDeclaration(
                        undefined,
                        [
                            factory.createToken(ts.SyntaxKind.ExportKeyword),
                        ],
                        baseName,
                        undefined,
                        factory.createTypeReferenceNode(
                            "Record",
                            [
                                factory.createKeywordTypeNode(ts.SyntaxKind.StringKeyword),
                                factory.createTypeReferenceNode(subTypeName),
                            ],
                        ),
                    );
                    break;
                }

                if (baseObject.additionalProperties ?? true) {
                    yield factory.createTypeAliasDeclaration(
                        undefined,
                        [
                            factory.createToken(ts.SyntaxKind.ExportKeyword),
                        ],
                        baseName,
                        undefined,
                        factory.createKeywordTypeNode(ts.SyntaxKind.AnyKeyword),
                    );
                    break;
                }
                else {
                    yield factory.createTypeAliasDeclaration(
                        undefined,
                        [
                            factory.createToken(ts.SyntaxKind.ExportKeyword),
                        ],
                        baseName,
                        undefined,
                        factory.createKeywordTypeNode(ts.SyntaxKind.ObjectKeyword),
                    );
                    break;
                }
            }

            case "array": {
                assert("items" in baseObject);

                const subPath = resolvePointerParts(
                    [...basePointerParts, "items"],
                    baseObject.items,
                );
                assert(subPath);
                const subReference = toReference(subPath);
                const subName = nameIndex[subReference];

                yield factory.createTypeAliasDeclaration(
                    undefined,
                    [
                        factory.createToken(ts.SyntaxKind.ExportKeyword),
                    ],
                    baseName,
                    undefined,
                    factory.createTypeReferenceNode(
                        "Array",
                        [
                            factory.createTypeReferenceNode(subName),
                        ],
                    ),
                );
                break;
            }

            case "boolean":
                yield factory.createTypeAliasDeclaration(
                    undefined,
                    [
                        factory.createToken(ts.SyntaxKind.ExportKeyword),
                    ],
                    baseName,
                    undefined,
                    factory.createKeywordTypeNode(
                        ts.SyntaxKind.BooleanKeyword,
                    ),
                );
                break;

            case "number":
            case "integer":
                if (baseObject.enum) {
                    yield factory.createTypeAliasDeclaration(
                        undefined,
                        [
                            factory.createToken(ts.SyntaxKind.ExportKeyword),
                        ],
                        baseName,
                        undefined,
                        factory.createUnionTypeNode(baseObject.enum.map(
                            value => factory.createLiteralTypeNode(
                                factory.createNumericLiteral(value),
                            ),
                        )),
                    );
                }
                else {
                    yield factory.createTypeAliasDeclaration(
                        undefined,
                        [
                            factory.createToken(ts.SyntaxKind.ExportKeyword),
                        ],
                        baseName,
                        undefined,
                        factory.createKeywordTypeNode(
                            ts.SyntaxKind.NumberKeyword,
                        ),
                    );
                }
                break;

            case "string":
                if (baseObject.enum) {
                    yield factory.createTypeAliasDeclaration(
                        undefined,
                        [
                            factory.createToken(ts.SyntaxKind.ExportKeyword),
                        ],
                        baseName,
                        undefined,
                        factory.createUnionTypeNode(baseObject.enum.map(
                            value => factory.createLiteralTypeNode(
                                factory.createStringLiteral(value),
                            ),
                        )),
                    );
                }
                else {
                    yield factory.createTypeAliasDeclaration(
                        undefined,
                        [
                            factory.createToken(ts.SyntaxKind.ExportKeyword),
                        ],
                        baseName,
                        undefined,
                        factory.createKeywordTypeNode(
                            ts.SyntaxKind.StringKeyword,
                        ),
                    );
                }
                break;

            default:
                yield factory.createTypeAliasDeclaration(
                    undefined,
                    [
                        factory.createToken(ts.SyntaxKind.ExportKeyword),
                    ],
                    baseName,
                    undefined,
                    factory.createKeywordTypeNode(
                        ts.SyntaxKind.UnknownKeyword,
                    ),
                );
        }

    }
}

