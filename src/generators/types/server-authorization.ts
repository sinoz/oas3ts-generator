import { OpenAPIV3 } from "openapi-types";
import ts from "typescript";
import { selectAllSecuritySchemes } from "../../selectors/index.js";
import { stringifyIdentifier } from "../../utils/index.js";

export function createServerAuthorizationType(
    factory: ts.NodeFactory,
    document: OpenAPIV3.Document,
) {
    const securitySchemes = [...selectAllSecuritySchemes(document)].
        map(({ securityScheme }) => securityScheme);

    const authorizationType = factory.createTypeReferenceNode(
        factory.createIdentifier("Record"),
        [
            securitySchemes.length === 0 ?
                factory.createKeywordTypeNode(ts.SyntaxKind.NeverKeyword) :
                factory.createUnionTypeNode(
                    securitySchemes.map(scheme => factory.createLiteralTypeNode(
                        factory.createStringLiteral(stringifyIdentifier([scheme])),
                    )),
                ),
            factory.createKeywordTypeNode(ts.SyntaxKind.UnknownKeyword),
        ],
    );

    return factory.createTypeAliasDeclaration(
        undefined,
        [
            factory.createToken(ts.SyntaxKind.ExportKeyword),
        ],
        "ServerAuthorization",
        undefined,
        authorizationType,
    );
}
