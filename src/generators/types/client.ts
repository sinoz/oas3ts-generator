import { OpenAPIV3 } from "openapi-types";
import ts from "typescript";
import { selectAllPaths, selectAllSecuritySchemes } from "../../selectors/index.js";
import { stringifyIdentifier, toReference } from "../../utils/index.js";
import { generateClientOperationDeclarations } from "../members/index.js";
import { PackageConfig } from "../package.js";

export function createClientClass(
    factory: ts.NodeFactory,
    document: OpenAPIV3.Document,
    config: PackageConfig,
    nameIndex: Record<string, string>,
) {
    const classDeclaration = createClassDeclaration();

    return classDeclaration;

    function createClassDeclaration() {
        const constructorStatements = [...emitConstructorStatements()];

        const constructorDeclaration = factory.createConstructorDeclaration(
            undefined,
            undefined,
            [
                factory.createParameterDeclaration(
                    undefined,
                    undefined,
                    undefined,
                    "options",
                    undefined,
                    factory.createTypeReferenceNode(
                        factory.createQualifiedName(
                            factory.createIdentifier("lib"),
                            "ClientOptions",
                        ),
                    ),
                ),
                factory.createParameterDeclaration(
                    [],
                    [
                        factory.createModifier(ts.SyntaxKind.PrivateKeyword),
                        factory.createModifier(ts.SyntaxKind.ReadonlyKeyword),
                    ],
                    undefined,
                    "credentials",
                    undefined,
                    factory.createTypeReferenceNode("ClientCredentials"),
                    factory.createObjectLiteralExpression(),
                ),
            ],
            factory.createBlock(constructorStatements, true),
        );

        const classElements = [
            constructorDeclaration,
            ...generateClientOperationDeclarations(factory, document, config, nameIndex),
        ];

        const classDeclaration = factory.createClassDeclaration(
            undefined,
            [
                factory.createToken(ts.SyntaxKind.ExportKeyword),
            ],
            "Client",
            undefined,
            [
                factory.createHeritageClause(
                    ts.SyntaxKind.ExtendsKeyword,
                    [
                        factory.createExpressionWithTypeArguments(
                            factory.createPropertyAccessExpression(
                                factory.createIdentifier("lib"),
                                "ClientBase",
                            ),
                            undefined,
                        ),
                    ],
                ),
            ],
            classElements,
        );

        return classDeclaration;
    }

    function* emitConstructorStatements() {
        yield factory.createExpressionStatement(factory.createCallExpression(
            factory.createSuper(),
            [],
            [
                factory.createIdentifier("options"),
            ],
        ));

        for (const { path } of selectAllPaths(document)) {
            yield factory.createExpressionStatement(factory.createCallExpression(
                factory.createPropertyAccessExpression(
                    factory.createPropertyAccessExpression(
                        factory.createThis(),
                        factory.createIdentifier("router"),
                    ),
                    factory.createIdentifier("insertRoute"),
                ),
                undefined,
                [
                    factory.createStringLiteral(path),
                    factory.createStringLiteral(path),
                ],
            ));
        }
    }

    function* emityCredentialsTypeMembers() {
        for (const {
            securitySchemeObject,
            pointerParts,
            securityScheme,
        } of selectAllSecuritySchemes(document)) {
            const securitySchemeTypeReference = toReference(pointerParts);
            const securitySchemeTypeName = nameIndex[securitySchemeTypeReference];
            const name = stringifyIdentifier([securityScheme]);

            yield factory.createPropertySignature(
                undefined,
                factory.createIdentifier(name),
                factory.createToken(ts.SyntaxKind.QuestionToken),
                factory.createTypeReferenceNode(
                    factory.createQualifiedName(
                        factory.createIdentifier("shared"),
                        securitySchemeTypeName,
                    ),
                ),
            );

        }
    }

}
