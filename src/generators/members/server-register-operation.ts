import assert from "assert";
import { OpenAPIV3 } from "openapi-types";
import ts from "typescript";
import { selectAllOperations } from "../../selectors/index.js";
import { addMeta, stringifyIdentifier, stringifyType } from "../../utils/index.js";

export function* generateServerRegisterOperationDeclarations(
    factory: ts.NodeFactory,
    document: OpenAPIV3.Document,
) {
    for (
        const { operationObject } of
        selectAllOperations(document)
    ) {
        yield createMethodDeclaration(operationObject);
    }

    function createMethodDeclaration(
        operationObject: OpenAPIV3.OperationObject,
    ) {
        assert(operationObject.operationId);

        const methodName = stringifyIdentifier(["register", operationObject.operationId, "operation"]);
        const handlerTypeName = stringifyType([operationObject.operationId, "operation", "handler"]);

        const statements = [
            ...emitStatements(operationObject),
        ];

        const methodDeclaration = factory.createMethodDeclaration(
            undefined,
            [
                factory.createToken(ts.SyntaxKind.PublicKeyword),
            ],
            undefined,
            methodName,
            undefined,
            undefined,
            [
                factory.createParameterDeclaration(
                    undefined,
                    undefined,
                    undefined,
                    "handler",
                    undefined,
                    factory.createTypeReferenceNode(
                        handlerTypeName,
                        [
                            factory.createTypeReferenceNode("Authorization"),
                        ],
                    ),
                ),
            ],
            undefined,
            factory.createBlock(
                statements,
                true,
            ),
        );

        addMeta(methodDeclaration, {
            deprecated: operationObject.deprecated,
            description: operationObject.description,
        });

        return methodDeclaration;
    }

    function* emitStatements(
        operationObject: OpenAPIV3.OperationObject,
    ) {
        assert(operationObject.operationId);

        const handlerPropertyName = stringifyIdentifier([operationObject.operationId, "operation", "handler"]);

        yield factory.createExpressionStatement(
            factory.createBinaryExpression(
                factory.createPropertyAccessExpression(
                    factory.createThis(),
                    handlerPropertyName,
                ),
                factory.createToken(ts.SyntaxKind.EqualsToken),
                factory.createIdentifier("handler"),
            ),
        );
    }

}
