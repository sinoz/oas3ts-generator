import { OpenAPIV3 } from "openapi-types";
import ts from "typescript";
import { selectAllSecuritySchemes } from "../../selectors/index.js";
import { stringifyIdentifier, stringifyType } from "../../utils/index.js";

export function* generateServerAuthorizationHandlerDeclarations(
    factory: ts.NodeFactory,
    document: OpenAPIV3.Document,
) {
    for (
        const { securitySchemeObject, securityScheme } of
        selectAllSecuritySchemes(document)
    ) {
        yield createPropertyDeclaration(securityScheme);
    }

    function createPropertyDeclaration(
        securityScheme: string,
    ) {
        const propertyName = stringifyIdentifier([
            securityScheme,
            "authorization",
            "handler",
        ]);
        const handlerTypeName = stringifyType([
            securityScheme,
            "authorization",
            "handler",
        ]);

        const propertyDeclaration = factory.createPropertyDeclaration(
            undefined,
            [
                factory.createToken(ts.SyntaxKind.ProtectedKeyword),
            ],
            propertyName,
            factory.createToken(
                ts.SyntaxKind.QuestionToken,
            ),
            factory.createTypeReferenceNode(
                handlerTypeName,
                [
                    factory.createTypeReferenceNode("Authorization"),
                ],
            ),
            undefined,
        );

        return propertyDeclaration;
    }

}
