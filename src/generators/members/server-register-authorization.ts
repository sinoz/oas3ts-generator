import { OpenAPIV3 } from "openapi-types";
import ts from "typescript";
import { selectAllSecuritySchemes } from "../../selectors/index.js";
import { stringifyIdentifier, stringifyType } from "../../utils/index.js";

export function* generateServerRegisterAuthorizationDeclarations(
    factory: ts.NodeFactory,
    document: OpenAPIV3.Document,
) {
    for (
        const { securitySchemeObject, securityScheme } of
        selectAllSecuritySchemes(document)
    ) {
        yield createMethodDeclaration(securityScheme);
    }

    function createMethodDeclaration(
        securityScheme: string,
    ) {
        const methodName = stringifyIdentifier(["register", securityScheme, "authorization"]);
        const handlerTypeName = stringifyType([securityScheme, "authorization", "handler"]);

        const statements = [
            ...emitStatements(securityScheme),
        ];

        const methodDeclaration = factory.createMethodDeclaration(
            undefined,
            [
                factory.createToken(ts.SyntaxKind.PublicKeyword),
            ],
            undefined,
            methodName,
            undefined,
            undefined,
            [
                factory.createParameterDeclaration(
                    undefined,
                    undefined,
                    undefined,
                    "handler",
                    undefined,
                    factory.createTypeReferenceNode(
                        handlerTypeName,
                        [
                            factory.createTypeReferenceNode("Authorization"),
                        ],
                    ),
                ),
            ],
            undefined,
            factory.createBlock(
                statements,
                true,
            ),
        );

        return methodDeclaration;
    }

    function* emitStatements(
        securityScheme: string,
    ) {
        const handlerPropertyName = stringifyIdentifier([securityScheme, "authorization", "handler"]);

        yield factory.createExpressionStatement(
            factory.createBinaryExpression(
                factory.createPropertyAccessExpression(
                    factory.createThis(),
                    handlerPropertyName,
                ),
                factory.createToken(ts.SyntaxKind.EqualsToken),
                factory.createIdentifier("handler"),
            ),
        );
    }

}
