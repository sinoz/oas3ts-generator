import assert from "assert";
import { OpenAPIV3 } from "openapi-types";
import ts from "typescript";
import { selectAllOperations, selectContentTypesFromContentContainer, selectHeadersFromResponse, selectParametersFromPathAndMethod, selectReponsesFromOperation, selectSecuritySchemesForOperation } from "../../selectors/index.js";
import { generateAllStatusCodes, resolveObject, resolvePointerParts, stringifyIdentifier, stringifyType, takeStatusCodes, toReference } from "../../utils/index.js";
import { PackageConfig } from "../package.js";

export function* generateServerOperationDeclarations(
    factory: ts.NodeFactory,
    document: OpenAPIV3.Document,
    config: PackageConfig,
    nameIndex: Record<string, string>,
) {
    for (
        const { operationObject, method, path } of
        selectAllOperations(document)
    ) {
        yield createMethodDeclaration(operationObject, path, method);
    }

    function createMethodDeclaration(
        operationObject: OpenAPIV3.OperationObject,
        path: string,
        method: OpenAPIV3.HttpMethods,
    ) {
        assert(operationObject.operationId);

        const methodName = stringifyIdentifier([operationObject.operationId]);

        const statements = [
            ...emitStatements(operationObject, path, method),
        ];

        const methodDeclaration = factory.createMethodDeclaration(
            undefined,
            [
                factory.createToken(ts.SyntaxKind.ProtectedKeyword),
                factory.createToken(ts.SyntaxKind.AsyncKeyword),
            ],
            undefined,
            methodName,
            undefined,
            undefined,
            [
                factory.createParameterDeclaration(
                    undefined,
                    undefined,
                    undefined,
                    "route",
                    undefined,
                    factory.createTypeReferenceNode("Route"),
                ),
                factory.createParameterDeclaration(
                    undefined,
                    undefined,
                    undefined,
                    "incomingRequest",
                    undefined,
                    factory.createTypeReferenceNode(
                        factory.createQualifiedName(
                            factory.createIdentifier("lib"),
                            "ServerIncomingRequest",
                        ),
                    ),
                ),
            ],
            undefined,
            factory.createBlock(statements, true),
        );

        return methodDeclaration;
    }

    function* emitStatements(
        operationObject: OpenAPIV3.OperationObject,
        path: string,
        method: OpenAPIV3.HttpMethods,
    ) {
        yield* emitSetupStatements(operationObject);
        yield* emitAuthorizationStatements(operationObject);

        yield* emitRequestParametersStatements(operationObject, path, method);

        yield* emitRequestAcceptTypesStatements(operationObject);

        yield* emitRequestStatements(operationObject, method, path);
        yield* emitCallHandlerStatements(operationObject);
        yield* emitResponseStatements(operationObject, path, method);

        yield factory.createReturnStatement(
            factory.createIdentifier("outgoingResponse"),
        );
    }

    function* emitSetupStatements(
        operationObject: OpenAPIV3.OperationObject,
    ) {
        assert(operationObject.operationId);

        const operationHandlerName = stringifyIdentifier([operationObject.operationId, "operation", "handler"]);

        yield factory.createVariableStatement(
            undefined,
            factory.createVariableDeclarationList([
                factory.createVariableDeclaration(
                    factory.createIdentifier("handler"),
                    undefined,
                    undefined,
                    factory.createPropertyAccessExpression(
                        factory.createThis(),
                        operationHandlerName,
                    ),
                ),
            ], ts.NodeFlags.Const),
        );

        yield factory.createIfStatement(
            factory.createBinaryExpression(
                factory.createIdentifier("handler"),
                factory.createToken(ts.SyntaxKind.EqualsEqualsToken),
                factory.createNull(),
            ),
            factory.createBlock([
                factory.createThrowStatement(factory.createNewExpression(
                    factory.createPropertyAccessExpression(
                        factory.createIdentifier("lib"),
                        factory.createIdentifier("OperationNotRegisteredError"),
                    ),
                    undefined,
                    [
                        factory.createStringLiteral(operationObject.operationId),
                    ],
                )),
            ], true),
        );

        yield factory.createVariableStatement(
            undefined,
            factory.createVariableDeclarationList([
                factory.createVariableDeclaration(
                    factory.createIdentifier("routeParameters"),
                    undefined,
                    undefined,
                    factory.createObjectLiteralExpression(
                        [factory.createSpreadAssignment(factory.createPropertyAccessExpression(
                            factory.createIdentifier("route"),
                            factory.createIdentifier("parameters"),
                        ))],
                        false,
                    ),
                ),
            ], ts.NodeFlags.Const),
        );
        yield factory.createVariableStatement(
            undefined,
            factory.createVariableDeclarationList([
                factory.createVariableDeclaration(
                    factory.createIdentifier("requestQuery"),
                    undefined,
                    undefined,
                    factory.createCallExpression(
                        factory.createPropertyAccessExpression(
                            factory.createIdentifier("lib"),
                            factory.createIdentifier("parseParameters"),
                        ),
                        undefined,
                        [factory.createPropertyAccessExpression(
                            factory.createIdentifier("incomingRequest"),
                            factory.createIdentifier("query"),
                        )],
                    ),
                ),
            ], ts.NodeFlags.Const),
        );
        yield factory.createVariableStatement(
            undefined,
            factory.createVariableDeclarationList([
                factory.createVariableDeclaration(
                    factory.createIdentifier("requestHeaders"),
                    undefined,
                    undefined,
                    factory.createObjectLiteralExpression(
                        [factory.createSpreadAssignment(factory.createPropertyAccessExpression(
                            factory.createIdentifier("incomingRequest"),
                            factory.createIdentifier("headers"),
                        ))],
                        false,
                    ),
                ),
            ], ts.NodeFlags.Const),
        );

    }

    function* emitAuthorizationStatements(
        operationObject: OpenAPIV3.OperationObject,
    ) {
        assert(operationObject.operationId);

        const authorizationMethodName = stringifyIdentifier(["authorize", operationObject.operationId]);

        yield factory.createVariableStatement(
            undefined,
            factory.createVariableDeclarationList([
                factory.createVariableDeclaration(
                    factory.createIdentifier("credentials"),
                    undefined,
                    undefined,
                    factory.createObjectLiteralExpression([
                        ...emitCredentialsAssignments(
                            operationObject,
                        ),
                    ], true),
                ),
            ], ts.NodeFlags.Const),
        );

        yield factory.createVariableStatement(
            undefined,
            factory.createVariableDeclarationList([
                factory.createVariableDeclaration(
                    factory.createIdentifier("authorization"),
                    undefined,
                    undefined,
                    factory.createAwaitExpression(factory.createCallExpression(
                        factory.createPropertyAccessExpression(
                            factory.createThis(),
                            authorizationMethodName,
                        ),
                        undefined,
                        [
                            factory.createIdentifier("credentials"),
                        ],
                    )),
                ),
            ], ts.NodeFlags.Const),
        );

        yield factory.createIfStatement(
            factory.createBinaryExpression(
                factory.createIdentifier("authorization"),
                factory.createToken(ts.SyntaxKind.EqualsEqualsToken),
                factory.createNull(),
            ),
            factory.createBlock([
                factory.createThrowStatement(factory.createNewExpression(
                    factory.createPropertyAccessExpression(
                        factory.createIdentifier("lib"),
                        factory.createIdentifier("AuthorizationFailedError"),
                    ),
                    undefined,
                    [
                        factory.createStringLiteral(operationObject.operationId),
                    ],
                )),
            ], true),
        );
    }

    function* emitRequestParametersStatements(
        operationObject: OpenAPIV3.OperationObject,
        path: string,
        method: OpenAPIV3.HttpMethods,
    ) {
        assert(operationObject.operationId);

        const parameterTypeName = stringifyType([
            operationObject.operationId,
            "request",
            "parameters",
        ]);
        const validateRequestParametersFn = stringifyIdentifier([
            "validate",
            parameterTypeName,
        ]);

        yield factory.createVariableStatement(
            undefined,
            factory.createVariableDeclarationList([
                factory.createVariableDeclaration(
                    factory.createIdentifier("requestParameters"),
                    undefined,
                    undefined,
                    undefined,
                ),
            ], ts.NodeFlags.Let),
        );

        yield factory.createTryStatement(
            factory.createBlock([factory.createExpressionStatement(
                factory.createBinaryExpression(
                    factory.createIdentifier("requestParameters"),
                    factory.createToken(ts.SyntaxKind.EqualsToken),
                    factory.createAsExpression(
                        factory.createObjectLiteralExpression([
                            ...emitRequestParametersAssignments(
                                operationObject,
                                path,
                                method,
                            ),
                        ], true),
                        factory.createTypeReferenceNode(
                            factory.createQualifiedName(
                                factory.createIdentifier("shared"),
                                factory.createIdentifier(parameterTypeName),
                            ),
                        ),
                    ),
                ),
            )], true),
            factory.createCatchClause(undefined, factory.createBlock([
                factory.createThrowStatement(factory.createNewExpression(
                    factory.createPropertyAccessExpression(
                        factory.createIdentifier("lib"),
                        factory.createIdentifier("IncomingRequestParameterExtractionError"),
                    ),
                    undefined,
                    [
                    ],
                )),
            ], true)),
            undefined,
        );

        yield factory.createIfStatement(
            factory.createPropertyAccessExpression(
                factory.createThis(),
                factory.createIdentifier("validateIncomingParameters"),
            ),
            factory.createBlock([
                factory.createVariableStatement(
                    undefined,
                    factory.createVariableDeclarationList([factory.createVariableDeclaration(
                        factory.createIdentifier("invalidPaths"),
                        undefined,
                        undefined,
                        factory.createArrayLiteralExpression([
                            factory.createSpreadElement(factory.createCallExpression(
                                factory.createPropertyAccessExpression(
                                    factory.createIdentifier("shared"),
                                    factory.createIdentifier(validateRequestParametersFn),
                                ),
                                undefined,
                                [factory.createIdentifier("requestParameters")],
                            )),
                        ], false),
                    )], ts.NodeFlags.Const),
                ),
                factory.createIfStatement(
                    factory.createBinaryExpression(
                        factory.createPropertyAccessExpression(
                            factory.createIdentifier("invalidPaths"),
                            factory.createIdentifier("length"),
                        ),
                        factory.createToken(ts.SyntaxKind.GreaterThanToken),
                        factory.createNumericLiteral(0),
                    ),
                    factory.createBlock([
                        factory.createThrowStatement(factory.createNewExpression(
                            factory.createPropertyAccessExpression(
                                factory.createIdentifier("lib"),
                                factory.createIdentifier("IncomingRequestParametersValidationError"),
                            ),
                            undefined,
                            [factory.createIdentifier("invalidPaths")],
                        )),
                    ], true),
                ),
            ], true),
        );

    }

    function* emitRequestParametersAssignments(
        operationObject: OpenAPIV3.OperationObject,
        path: string,
        method: OpenAPIV3.HttpMethods,
    ) {
        for (
            const { parameterObject, parameterPointerParts } of
            selectParametersFromPathAndMethod(document, path, method)
        ) {
            const parameterTypeReference = toReference(parameterPointerParts);
            const parameterTypeName = nameIndex[parameterTypeReference];
            const functionName = stringifyIdentifier([
                "extract",
                parameterTypeName,
            ]);
            const parameterName = stringifyIdentifier([
                parameterObject.name,
            ]);

            let parametersSource: ts.Expression;
            switch (parameterObject.in) {
                case "path":
                    parametersSource = factory.createIdentifier("routeParameters");
                    break;

                case "query":
                    parametersSource = factory.createIdentifier("requestQuery");
                    break;

                case "header":
                    parametersSource = factory.createIdentifier("requestHeaders");
                    break;

                default: throw new Error("cannot happen");
            }

            const parameterValue = factory.createCallExpression(
                factory.createPropertyAccessExpression(
                    factory.createIdentifier("internal"),
                    factory.createIdentifier(functionName),
                ),
                undefined,
                [
                    parametersSource,
                ],
            );

            yield factory.createPropertyAssignment(
                parameterName, parameterValue,
            );
        }

    }

    function* emitCredentialsAssignments(
        operationObject: OpenAPIV3.OperationObject,
    ) {
        for (
            const { securityScheme, securitySchemeObject, pointerParts } of
            selectSecuritySchemesForOperation(document, operationObject)
        ) {
            const credentialTypeReference = toReference(pointerParts);
            const credentialTypeName = nameIndex[credentialTypeReference];
            const functionName = stringifyIdentifier([
                "extract",
                credentialTypeName,
            ]);
            const credentialName = stringifyIdentifier([
                securityScheme,
            ]);

            let credentialsSource: ts.Expression;
            switch (securitySchemeObject.type) {
                case "apiKey":
                    switch (securitySchemeObject.in) {
                        case "query":
                            credentialsSource = factory.createIdentifier("requestQuery");
                            break;

                        case "header":
                            credentialsSource = factory.createIdentifier("requestHeaders");
                            break;

                        default: throw new Error("cannot happen");
                    }
                    break;

                case "http":
                    credentialsSource = factory.createIdentifier("requestHeaders");
                    break;

                default: throw new Error("cannot happen");
            }

            const credentialValue = factory.createCallExpression(
                factory.createPropertyAccessExpression(
                    factory.createIdentifier("internal"),
                    factory.createIdentifier(functionName),
                ),
                undefined,
                [
                    credentialsSource,
                ],
            );

            yield factory.createPropertyAssignment(
                credentialName, credentialValue,
            );
        }

    }

    function* emitValidateResponseParametersStatements(
        responseObject: OpenAPIV3.ResponseObject,
        responsePointerPath: string[],
    ) {
        const responseReference = toReference(responsePointerPath);
        const typeName = nameIndex[responseReference];
        const validateResponseParametersFn = stringifyIdentifier([
            "validate",
            typeName,
            "response",
            "parameters",
        ]);

        yield factory.createIfStatement(
            factory.createPropertyAccessExpression(
                factory.createThis(),
                factory.createIdentifier("validateOutgoingParameters"),
            ),
            factory.createBlock([
                factory.createVariableStatement(
                    undefined,
                    factory.createVariableDeclarationList([factory.createVariableDeclaration(
                        factory.createIdentifier("invalidPaths"),
                        undefined,
                        undefined,
                        factory.createArrayLiteralExpression([
                            factory.createSpreadElement(factory.createCallExpression(
                                factory.createPropertyAccessExpression(
                                    factory.createIdentifier("shared"),
                                    factory.createIdentifier(validateResponseParametersFn),
                                ),
                                undefined,
                                [factory.createPropertyAccessExpression(
                                    factory.createIdentifier("outgoingResponseModel"),
                                    factory.createIdentifier("parameters"),
                                )],
                            )),
                        ], false),
                    )], ts.NodeFlags.Const),
                ),
                factory.createIfStatement(
                    factory.createBinaryExpression(
                        factory.createPropertyAccessExpression(
                            factory.createIdentifier("invalidPaths"),
                            factory.createIdentifier("length"),
                        ),
                        factory.createToken(ts.SyntaxKind.GreaterThanToken),
                        factory.createNumericLiteral(0),
                    ),
                    factory.createBlock([
                        factory.createThrowStatement(factory.createNewExpression(
                            factory.createPropertyAccessExpression(
                                factory.createIdentifier("lib"),
                                factory.createIdentifier("OutgoingResponseParametersValidationError"),
                            ),
                            undefined,
                            [factory.createIdentifier("invalidPaths")],
                        )),
                    ], true),
                ),
            ], true),
        );
    }

    function* emitInjectResponseParametersStatements(
        responseObject: OpenAPIV3.ResponseObject,
        responsePointerPath: string[],
    ) {
        for (
            const { header, headerPointerParts } of
            selectHeadersFromResponse(document, responseObject, responsePointerPath)
        ) {
            const parameterTypeReference = toReference(headerPointerParts);
            const parameterTypeName = nameIndex[parameterTypeReference];
            const functionName = stringifyIdentifier([
                "inject",
                parameterTypeName,
            ]);
            const parameterName = stringifyIdentifier([
                header,
            ]);

            const parametersSource = factory.createIdentifier("responseHeaders");

            const parameterValue = factory.createPropertyAccessExpression(
                factory.createPropertyAccessExpression(
                    factory.createIdentifier("outgoingResponseModel"),
                    factory.createIdentifier("parameters"),
                ),
                factory.createIdentifier(parameterName),
            );

            yield factory.createExpressionStatement(factory.createCallExpression(
                factory.createPropertyAccessExpression(
                    factory.createIdentifier("internal"),
                    factory.createIdentifier(functionName),
                ),
                undefined,
                [
                    parametersSource,
                    parameterValue,
                ],
            ));
        }

    }

    function* emitRequestAcceptTypesStatements(
        operationObject: OpenAPIV3.OperationObject,
    ) {
        assert(operationObject.operationId);

        const constantName = stringifyIdentifier([
            operationObject.operationId,
            "accept",
            "types",
        ]);

        yield factory.createVariableStatement(
            undefined,
            factory.createVariableDeclarationList([
                factory.createVariableDeclaration(
                    factory.createIdentifier("requestAcceptTypes"),
                    undefined,
                    undefined,
                    factory.createCallExpression(
                        factory.createPropertyAccessExpression(
                            factory.createIdentifier("lib"),
                            factory.createIdentifier("parseAcceptHeader"),
                        ),
                        undefined,
                        [
                            factory.createCallExpression(
                                factory.createPropertyAccessExpression(
                                    factory.createIdentifier("lib"),
                                    factory.createIdentifier("getParameterValues"),
                                ),
                                undefined,
                                [
                                    factory.createPropertyAccessExpression(
                                        factory.createIdentifier("incomingRequest"),
                                        factory.createIdentifier("headers"),
                                    ),
                                    factory.createStringLiteral("accept"),
                                ],
                            ),
                            factory.createPropertyAccessExpression(
                                factory.createIdentifier("shared"),
                                factory.createIdentifier(constantName),
                            ),
                        ],
                    ),
                ),
            ], ts.NodeFlags.Const),
        );
    }

    function* emitCallHandlerStatements(
        operationObject: OpenAPIV3.OperationObject,
    ) {
        yield factory.createVariableStatement(
            undefined,
            factory.createVariableDeclarationList([
                factory.createVariableDeclaration(
                    factory.createIdentifier("outgoingResponseModel"),
                    undefined,
                    undefined,
                    factory.createAwaitExpression(factory.createCallExpression(
                        factory.createIdentifier("handler"),
                        undefined,
                        [
                            factory.createIdentifier("incomingRequestModel"),
                            factory.createIdentifier("authorization"),
                            factory.createIdentifier("requestAcceptTypes"),
                        ],
                    )),
                ),
            ], ts.NodeFlags.Const),
        );
    }

    function* emitRequestStatements(
        operationObject: OpenAPIV3.OperationObject,
        method: OpenAPIV3.HttpMethods,
        path: string,
    ) {
        assert(operationObject.operationId);

        const operationPointerParts = ["paths", path, method];

        const incomingRequestTypeName = stringifyType([
            operationObject.operationId,
            "incoming",
            "request",
        ]);

        const requestBodyPointerParts = resolvePointerParts(
            [...operationPointerParts, "requestBody"],
            operationObject.requestBody,
        );
        const requestBody = resolveObject(
            document,
            operationObject.requestBody,
        );

        if (requestBody) {
            assert(requestBodyPointerParts);

            const caseClauses = [...emitRequestContentTypeCaseClauses(
                requestBodyPointerParts,
                requestBody,
            )];

            yield factory.createVariableStatement(
                undefined,
                factory.createVariableDeclarationList([
                    factory.createVariableDeclaration(
                        factory.createIdentifier("incomingRequestModel"),
                        undefined,
                        factory.createTypeReferenceNode(
                            factory.createIdentifier(incomingRequestTypeName),
                        ),
                        undefined,
                    ),
                ], ts.NodeFlags.Let),
            );

            yield factory.createVariableStatement(
                undefined,
                factory.createVariableDeclarationList(
                    [factory.createVariableDeclaration(
                        factory.createIdentifier("requestContentTypeHeader"),
                        undefined,
                        undefined,
                        factory.createCallExpression(
                            factory.createPropertyAccessExpression(
                                factory.createIdentifier("lib"),
                                factory.createIdentifier("getParameterValue"),
                            ),
                            undefined,
                            [
                                factory.createPropertyAccessExpression(
                                    factory.createIdentifier("incomingRequest"),
                                    factory.createIdentifier("headers"),
                                ),
                                factory.createStringLiteral("content-type"),
                            ],
                        ),
                    )],
                    ts.NodeFlags.Const,
                ),
            );
            yield factory.createIfStatement(
                factory.createBinaryExpression(
                    factory.createIdentifier("requestContentTypeHeader"),
                    factory.createToken(ts.SyntaxKind.EqualsEqualsToken),
                    factory.createNull(),
                ),
                factory.createBlock(
                    [factory.createThrowStatement(factory.createNewExpression(
                        factory.createPropertyAccessExpression(
                            factory.createIdentifier("lib"),
                            factory.createIdentifier("MissingRequestContentTypeError"),
                        ),
                        undefined,
                        [
                        ],
                    ))],
                    true,
                ),
                undefined,
            );
            yield factory.createVariableStatement(
                undefined,
                factory.createVariableDeclarationList(
                    [factory.createVariableDeclaration(
                        factory.createIdentifier("requestContentType"),
                        undefined,
                        undefined,
                        factory.createCallExpression(
                            factory.createPropertyAccessExpression(
                                factory.createIdentifier("lib"),
                                factory.createIdentifier("parseContentTypeHeader"),
                            ),
                            undefined,
                            [factory.createIdentifier("requestContentTypeHeader")],
                        ),
                    )],
                    ts.NodeFlags.Const,
                ),
            );
            yield factory.createIfStatement(
                factory.createBinaryExpression(
                    factory.createIdentifier("requestContentType"),
                    factory.createToken(ts.SyntaxKind.EqualsEqualsToken),
                    factory.createNull(),
                ),
                factory.createBlock([
                    factory.createThrowStatement(factory.createNewExpression(
                        factory.createPropertyAccessExpression(
                            factory.createIdentifier("lib"),
                            factory.createIdentifier("MissingRequestContentTypeError"),
                        ),
                        undefined,
                        [
                        ],
                    )),
                ], true),
            );

            yield factory.createSwitchStatement(
                factory.createIdentifier("requestContentType"),
                factory.createCaseBlock(caseClauses),
            );
        }
        else {
            yield factory.createVariableStatement(
                undefined,
                factory.createVariableDeclarationList([
                    factory.createVariableDeclaration(
                        factory.createIdentifier("incomingRequestModel"),
                        undefined,
                        undefined,
                        createEmptyRequestModel(),
                    ),
                ], ts.NodeFlags.Const),
            );

        }

    }

    function* emitResponseStatements(
        operationObject: OpenAPIV3.OperationObject,
        path: string,
        method: OpenAPIV3.HttpMethods,
    ) {
        const operationPointerParts = ["paths", path, method];

        const caseClauses = [...emitResponseStatusCaseClauses(
            operationPointerParts,
            operationObject,
        )];

        yield factory.createVariableStatement(
            undefined,
            factory.createVariableDeclarationList([
                factory.createVariableDeclaration(
                    factory.createIdentifier("responseHeaders"),
                    undefined,
                    factory.createTypeReferenceNode(
                        factory.createQualifiedName(
                            factory.createIdentifier("lib"),
                            factory.createIdentifier("Parameters"),
                        ),
                    ),
                    factory.createObjectLiteralExpression([]),
                ),
            ], ts.NodeFlags.Const),
        );

        yield factory.createVariableStatement(
            undefined,
            factory.createVariableDeclarationList([
                factory.createVariableDeclaration(
                    factory.createIdentifier("outgoingResponse"),
                    undefined,
                    factory.createTypeReferenceNode(
                        factory.createQualifiedName(
                            factory.createIdentifier("lib"),
                            "ServerOutgoingResponse",
                        ),
                    ),
                    undefined,
                ),
            ], ts.NodeFlags.Let),
        );

        yield factory.createVariableStatement(
            undefined,
            factory.createVariableDeclarationList(
                [factory.createVariableDeclaration(
                    factory.createIdentifier("responseStatus"),
                    undefined,
                    undefined,
                    factory.createPropertyAccessExpression(
                        factory.createIdentifier("outgoingResponseModel"),
                        factory.createIdentifier("status"),
                    ),
                )],
                ts.NodeFlags.Const,
            ),
        );

        yield factory.createSwitchStatement(
            factory.createIdentifier("responseStatus"),
            factory.createCaseBlock(caseClauses),
        );

    }

    function* emitRequestContentTypeCaseClauses(
        requestBodyPointerParts: string[],
        requestBodyObject: OpenAPIV3.RequestBodyObject,
    ) {

        for (
            const { contentType, mediaTypeObject, mediaTypePointerParts } of
            selectContentTypesFromContentContainer(
                config.requestTypes,
                requestBodyObject,
                requestBodyPointerParts,
            )
        ) {
            const valueExpression = createRequestModel(
                mediaTypePointerParts,
                contentType,
            );

            yield factory.createCaseClause(
                factory.createStringLiteral(contentType),
                [
                    factory.createExpressionStatement(factory.createBinaryExpression(
                        factory.createIdentifier("incomingRequestModel"),
                        factory.createToken(ts.SyntaxKind.EqualsToken),
                        valueExpression,
                    )),
                    factory.createBreakStatement(),
                ],
            );
        }

        yield factory.createDefaultClause([
            factory.createThrowStatement(factory.createNewExpression(
                factory.createPropertyAccessExpression(
                    factory.createIdentifier("lib"),
                    factory.createIdentifier("UnexpectedRequestContentType"),
                ),
                undefined,
                [
                    factory.createIdentifier("requestContentType"),
                ],
            )),
        ]);

    }

    function* emitResponseStatusCaseClauses(
        operationPointerParts: string[],
        operationObject: OpenAPIV3.OperationObject,
    ) {
        const statusSet = new Set(generateAllStatusCodes());

        for (
            const { statusKind, responseObject, responsePointerParts } of
            selectReponsesFromOperation(document, operationObject, operationPointerParts)
        ) {
            const statusses = [...takeStatusCodes(statusSet, statusKind)];

            {
                let statusPrev = 0;
                for (const status of statusses) {
                    if (statusPrev !== 0) {
                        yield factory.createCaseClause(
                            factory.createNumericLiteral(statusPrev),
                            [],
                        );
                    }
                    statusPrev = status;
                }

                const responseTypeEntries = [...selectContentTypesFromContentContainer(
                    config.responseTypes,
                    responseObject,
                    responsePointerParts,
                )];

                if (responseTypeEntries.length > 0) {
                    const [defaultReponseTypeEntry] = responseTypeEntries;
                    const {
                        contentType: defaultContentType,
                        mediaTypeObject: defaultMediaTypeObject,
                        mediaTypePointerParts: defaultMediaTypePointerParts,
                    } = defaultReponseTypeEntry;

                    const caseClauses = [...emitResponseContentTypeCaseClauses(
                        responsePointerParts,
                        responseObject,
                    )];

                    yield factory.createCaseClause(
                        factory.createNumericLiteral(statusPrev),
                        [factory.createBlock([
                            ...emitValidateResponseParametersStatements(
                                responseObject,
                                responsePointerParts,
                            ),
                            factory.createTryStatement(
                                factory.createBlock([...emitInjectResponseParametersStatements(
                                    responseObject,
                                    responsePointerParts,
                                )], true),
                                factory.createCatchClause(undefined, factory.createBlock([
                                    factory.createThrowStatement(factory.createNewExpression(
                                        factory.createPropertyAccessExpression(
                                            factory.createIdentifier("lib"),
                                            factory.createIdentifier("OutgoingResponseParameterInjectionError"),
                                        ),
                                        undefined,
                                        [
                                        ],
                                    )),
                                ], true)),
                                undefined,
                            ),

                            factory.createIfStatement(
                                factory.createBinaryExpression(
                                    factory.createStringLiteral("contentType"),
                                    factory.createToken(ts.SyntaxKind.InKeyword),
                                    factory.createIdentifier("outgoingResponseModel"),
                                ),

                                factory.createBlock([
                                    factory.createExpressionStatement(factory.createCallExpression(
                                        factory.createPropertyAccessExpression(
                                            factory.createIdentifier("lib"),
                                            factory.createIdentifier("addParameter"),
                                        ),
                                        undefined,
                                        [
                                            factory.createIdentifier("responseHeaders"),
                                            factory.createStringLiteral("content-type"),
                                            factory.createPropertyAccessExpression(
                                                factory.createIdentifier("outgoingResponseModel"),
                                                factory.createIdentifier("contentType"),
                                            ),
                                        ],
                                    )),

                                    factory.createSwitchStatement(
                                        factory.createPropertyAccessExpression(
                                            factory.createIdentifier("outgoingResponseModel"),
                                            factory.createIdentifier("contentType"),
                                        ),
                                        factory.createCaseBlock(caseClauses),
                                    ),
                                ], true),

                                factory.createBlock([
                                    factory.createExpressionStatement(factory.createCallExpression(
                                        factory.createPropertyAccessExpression(
                                            factory.createIdentifier("lib"),
                                            factory.createIdentifier("addParameter"),
                                        ),
                                        undefined,
                                        [
                                            factory.createIdentifier("responseHeaders"),
                                            factory.createStringLiteral("content-type"),
                                            factory.createStringLiteral(defaultContentType),
                                        ],
                                    )),

                                    factory.createExpressionStatement(
                                        factory.createBinaryExpression(
                                            factory.createIdentifier("outgoingResponse"),
                                            factory.createToken(ts.SyntaxKind.EqualsToken),
                                            createResponseModel(
                                                defaultMediaTypePointerParts,
                                            ),
                                        ),
                                    ),
                                ], true),
                            ),
                            factory.createBreakStatement(),
                        ])],
                    );
                }
                else {
                    yield factory.createCaseClause(
                        factory.createNumericLiteral(statusPrev),
                        [
                            factory.createExpressionStatement(factory.createBinaryExpression(
                                factory.createIdentifier("outgoingResponse"),
                                factory.createToken(ts.SyntaxKind.EqualsToken),
                                createEmptyResponse(),
                            )),
                            ...emitValidateResponseParametersStatements(
                                responseObject,
                                responsePointerParts,
                            ),
                            factory.createTryStatement(
                                factory.createBlock([...emitInjectResponseParametersStatements(
                                    responseObject,
                                    responsePointerParts,
                                )], true),
                                factory.createCatchClause(undefined, factory.createBlock([
                                    factory.createThrowStatement(factory.createNewExpression(
                                        factory.createPropertyAccessExpression(
                                            factory.createIdentifier("lib"),
                                            factory.createIdentifier("OutgoingResponseParameterInjectionError"),
                                        ),
                                        undefined,
                                        [
                                        ],
                                    )),
                                ], true)),
                                undefined,
                            ),
                            factory.createBreakStatement(),
                        ],
                    );
                }

            }

        }

        yield factory.createDefaultClause([
            factory.createThrowStatement(factory.createNewExpression(
                factory.createIdentifier("TypeError"),
                undefined,
                [
                    factory.createStringLiteral("status code not supported"),
                ],
            )),
        ]);
    }

    function* emitResponseContentTypeCaseClauses(
        responsePointerParts: string[],
        responseObject: OpenAPIV3.ResponseObject,
    ) {
        for (
            const { contentType, mediaTypeObject, mediaTypePointerParts } of
            selectContentTypesFromContentContainer(
                config.responseTypes,
                responseObject,
                responsePointerParts,
            )
        ) {

            const valueExpression = createResponseModel(
                mediaTypePointerParts,
            );

            yield factory.createCaseClause(
                factory.createStringLiteral(contentType),
                [
                    factory.createExpressionStatement(factory.createBinaryExpression(
                        factory.createIdentifier("outgoingResponse"),
                        factory.createToken(ts.SyntaxKind.EqualsToken),
                        valueExpression,
                    )),
                    factory.createBreakStatement(),
                ],
            );
        }

        yield factory.createDefaultClause([
            factory.createThrowStatement(factory.createNewExpression(
                factory.createIdentifier("TypeError"),
                undefined,
                [
                    factory.createStringLiteral("invalid content-type"),
                ],
            )),
        ]);
    }

    function createRequestModel(
        mediaTypePointerParts: string[],
        contentType: string,
    ) {
        const mediaTypeReference = toReference(mediaTypePointerParts);
        const mediaTypeName = nameIndex[mediaTypeReference];

        const deserializeFn = stringifyIdentifier([
            "deserialize",
            mediaTypeName,
        ]);

        return factory.createObjectLiteralExpression([
            factory.createSpreadAssignment(
                factory.createObjectLiteralExpression([
                    factory.createPropertyAssignment(
                        factory.createIdentifier("parameters"),
                        factory.createIdentifier("requestParameters"),
                    ),
                    factory.createPropertyAssignment(
                        factory.createIdentifier("contentType"),
                        factory.createStringLiteral(contentType),
                    ),
                ], true),
            ),
            factory.createSpreadAssignment(factory.createCallExpression(
                factory.createPropertyAccessExpression(
                    factory.createIdentifier("internal"),
                    deserializeFn,
                ),
                undefined,
                [
                    factory.createPropertyAccessExpression(
                        factory.createIdentifier("incomingRequest"),
                        factory.createIdentifier("stream"),
                    ),
                    factory.createPropertyAccessExpression(
                        factory.createThis(),
                        factory.createIdentifier("validateIncomingEntities"),
                    ),
                ],

            )),
        ], true);
    }

    function createResponseModel(
        mediaTypePointerParts: string[],
    ) {
        const mediaTypeReference = toReference(mediaTypePointerParts);
        const mediaTypeName = nameIndex[mediaTypeReference];

        const serializeFn = stringifyIdentifier([
            "serialize",
            mediaTypeName,
        ]);

        return factory.createObjectLiteralExpression([
            factory.createPropertyAssignment(
                factory.createIdentifier("headers"),
                factory.createIdentifier("responseHeaders"),
            ),
            factory.createPropertyAssignment(
                factory.createIdentifier("status"),
                factory.createPropertyAccessExpression(
                    factory.createIdentifier("outgoingResponseModel"),
                    "status",
                ),
            ),
            factory.createPropertyAssignment(
                factory.createIdentifier("stream"),
                factory.createCallExpression(
                    factory.createPropertyAccessExpression(
                        factory.createIdentifier("internal"),
                        serializeFn,
                    ),
                    undefined,
                    [
                        factory.createIdentifier("outgoingResponseModel"),
                        factory.createPropertyAccessExpression(
                            factory.createThis(),
                            factory.createIdentifier("validateOutgoingEntities"),
                        ),
                    ],
                ),
            ),
        ], true);
    }

    function createEmptyRequestModel() {
        return factory.createObjectLiteralExpression([
            factory.createPropertyAssignment(
                factory.createIdentifier("parameters"),
                factory.createIdentifier("requestParameters"),
            ),
            factory.createPropertyAssignment(
                factory.createIdentifier("contentType"),
                factory.createNull(),
            ),
        ], true);
    }

    function createStreamRequestModel(
        contentType: string,
    ) {
        return factory.createObjectLiteralExpression([
            factory.createPropertyAssignment(
                factory.createIdentifier("parameters"),
                factory.createIdentifier("requestParameters"),
            ),
            factory.createPropertyAssignment(
                factory.createIdentifier("contentType"),
                factory.createStringLiteral(contentType),
            ),
            factory.createPropertyAssignment(
                factory.createIdentifier("stream"),
                factory.createPropertyAccessExpression(
                    factory.createIdentifier("incomingRequest"),
                    "stream",
                ),
            ),
        ], true);
    }

    function createTextRequestModel(
        contentType: string,
    ) {
        return factory.createObjectLiteralExpression([
            factory.createSpreadAssignment(
                factory.createObjectLiteralExpression([
                    factory.createPropertyAssignment(
                        factory.createIdentifier("parameters"),
                        factory.createIdentifier("requestParameters"),
                    ),
                    factory.createPropertyAssignment(
                        factory.createIdentifier("contentType"),
                        factory.createStringLiteral(contentType),
                    ),
                ], true),
            ),
            factory.createSpreadAssignment(factory.createCallExpression(
                factory.createPropertyAccessExpression(
                    factory.createIdentifier("lib"),
                    "deserializeIncomingTextContainer",
                ),
                undefined,
                [
                    factory.createPropertyAccessExpression(
                        factory.createIdentifier("incomingRequest"),
                        factory.createIdentifier("stream"),
                    ),
                ],

            )),
        ], true);
    }

    function createJsonRequestModel(
        contentType: string,
        modelTypeName: string,
    ) {
        const validateFn = stringifyIdentifier([
            "validate",
            modelTypeName,
        ]);

        return factory.createObjectLiteralExpression([
            factory.createSpreadAssignment(factory.createObjectLiteralExpression([
                factory.createPropertyAssignment(
                    factory.createIdentifier("parameters"),
                    factory.createIdentifier("requestParameters"),
                ),
                factory.createPropertyAssignment(
                    factory.createIdentifier("contentType"),
                    factory.createStringLiteral(contentType),
                ),
            ], true)),
            factory.createSpreadAssignment(factory.createCallExpression(
                factory.createPropertyAccessExpression(
                    factory.createIdentifier("lib"),
                    factory.createIdentifier("deserializeIncomingJsonContainer"),
                ),
                [
                    factory.createTypeReferenceNode(
                        factory.createQualifiedName(
                            factory.createIdentifier("shared"),
                            modelTypeName,
                        ),
                    ),
                ],
                [
                    factory.createPropertyAccessExpression(
                        factory.createIdentifier("incomingRequest"),
                        factory.createIdentifier("stream"),
                    ),
                    // factory.createArrowFunction(
                    //     undefined,
                    //     undefined,
                    //     [factory.createParameterDeclaration(
                    //         undefined,
                    //         undefined,
                    //         undefined,
                    //         factory.createIdentifier("entity"),
                    //     )],
                    //     undefined,
                    //     factory.createToken(ts.SyntaxKind.EqualsGreaterThanToken),
                    //     factory.createBlock([
                    //         factory.createIfStatement(
                    //             factory.createPropertyAccessExpression(
                    //                 factory.createThis(),
                    //                 factory.createIdentifier("validateIncomingEntities"),
                    //             ),
                    //             factory.createBlock([
                    //                 factory.createVariableStatement(
                    //                     undefined,
                    //                     // eslint-disable-next-line max-len
                    //                     factory.createVariableDeclarationList([factory.createVariableDeclaration(
                    //                         factory.createIdentifier("invalidPaths"),
                    //                         undefined,
                    //                         undefined,
                    //                         factory.createArrayLiteralExpression([
                    //                             // eslint-disable-next-line max-len
                    //                             factory.createSpreadElement(factory.createCallExpression(
                    //                                 factory.createPropertyAccessExpression(
                    //                                     factory.createIdentifier("shared"),
                    //                                     factory.createIdentifier(validateFn),
                    //                                 ),
                    //                                 undefined,
                    //                                 [factory.createIdentifier("entity")],
                    //                             )),
                    //                         ], false),
                    //                     )], ts.NodeFlags.Const),
                    //                 ),
                    //                 factory.createIfStatement(
                    //                     factory.createBinaryExpression(
                    //                         factory.createPropertyAccessExpression(
                    //                             factory.createIdentifier("invalidPaths"),
                    //                             factory.createIdentifier("length"),
                    //                         ),
                    //                         factory.createToken(ts.SyntaxKind.GreaterThanToken),
                    //                         factory.createNumericLiteral(0),
                    //                     ),
                    //                     factory.createBlock([
                    //                         // eslint-disable-next-line max-len
                    //                         factory.createThrowStatement(factory.createNewExpression(
                    //                             factory.createPropertyAccessExpression(
                    //                                 factory.createIdentifier("lib"),
                    //                                 factory.createIdentifier("IncomingRequestEntityValidationError"),
                    //                             ),
                    //                             undefined,
                    //                             [factory.createIdentifier("invalidPaths")],
                    //                         )),
                    //                     ], true),
                    //                 ),
                    //             ]),
                    //         ),
                    //     ], true),
                    // ),
                ],
            )),
        ], true);
    }

    function createFormRequestModel(
        contentType: string,
        modelTypeName: string,
    ) {
        const validateFn = stringifyIdentifier([
            "validate",
            modelTypeName,
        ]);

        return factory.createObjectLiteralExpression([
            factory.createSpreadAssignment(factory.createObjectLiteralExpression([
                factory.createPropertyAssignment(
                    factory.createIdentifier("parameters"),
                    factory.createIdentifier("requestParameters"),
                ),
                factory.createPropertyAssignment(
                    factory.createIdentifier("contentType"),
                    factory.createStringLiteral(contentType),
                ),
            ], true)),
            factory.createSpreadAssignment(factory.createCallExpression(
                factory.createPropertyAccessExpression(
                    factory.createIdentifier("lib"),
                    factory.createIdentifier("deserializeIncomingFormContainer"),
                ),
                [
                    factory.createTypeReferenceNode(
                        factory.createQualifiedName(
                            factory.createIdentifier("shared"),
                            modelTypeName,
                        ),
                    ),
                ],
                [
                    factory.createPropertyAccessExpression(
                        factory.createIdentifier("incomingRequest"),
                        factory.createIdentifier("stream"),
                    ),
                    // factory.createArrowFunction(
                    //     undefined,
                    //     undefined,
                    //     [factory.createParameterDeclaration(
                    //         undefined,
                    //         undefined,
                    //         undefined,
                    //         factory.createIdentifier("entity"),
                    //     )],
                    //     undefined,
                    //     factory.createToken(ts.SyntaxKind.EqualsGreaterThanToken),
                    //     factory.createBlock([
                    //         factory.createIfStatement(
                    //             factory.createPropertyAccessExpression(
                    //                 factory.createThis(),
                    //                 factory.createIdentifier("validateIncomingEntities"),
                    //             ),
                    //             factory.createBlock([
                    //                 factory.createVariableStatement(
                    //                     undefined,
                    //                     // eslint-disable-next-line max-len
                    //                     factory.createVariableDeclarationList([factory.createVariableDeclaration(
                    //                         factory.createIdentifier("invalidPaths"),
                    //                         undefined,
                    //                         undefined,
                    //                         factory.createArrayLiteralExpression([
                    //                             // eslint-disable-next-line max-len
                    //                             factory.createSpreadElement(factory.createCallExpression(
                    //                                 factory.createPropertyAccessExpression(
                    //                                     factory.createIdentifier("shared"),
                    //                                     factory.createIdentifier(validateFn),
                    //                                 ),
                    //                                 undefined,
                    //                                 [factory.createIdentifier("entity")],
                    //                             )),
                    //                         ], false),
                    //                     )], ts.NodeFlags.Const),
                    //                 ),
                    //                 factory.createIfStatement(
                    //                     factory.createBinaryExpression(
                    //                         factory.createPropertyAccessExpression(
                    //                             factory.createIdentifier("invalidPaths"),
                    //                             factory.createIdentifier("length"),
                    //                         ),
                    //                         factory.createToken(ts.SyntaxKind.GreaterThanToken),
                    //                         factory.createNumericLiteral(0),
                    //                     ),
                    //                     factory.createBlock([
                    //                         // eslint-disable-next-line max-len
                    //                         factory.createThrowStatement(factory.createNewExpression(
                    //                             factory.createPropertyAccessExpression(
                    //                                 factory.createIdentifier("lib"),
                    //                                 factory.createIdentifier("IncomingRequestEntityValidationError"),
                    //                             ),
                    //                             undefined,
                    //                             [factory.createIdentifier("invalidPaths")],
                    //                         )),
                    //                     ], true),
                    //                 ),
                    //             ]),
                    //         ),
                    //     ], true),
                    // ),
                ],
            )),
        ], true);
    }

    function createEmptyResponse() {
        return factory.createObjectLiteralExpression([
            factory.createPropertyAssignment(
                factory.createIdentifier("headers"),
                factory.createIdentifier("responseHeaders"),
            ),
            factory.createPropertyAssignment(
                factory.createIdentifier("status"),
                factory.createPropertyAccessExpression(
                    factory.createIdentifier("outgoingResponseModel"),
                    "status",
                ),
            ),
        ], true);
    }

    function createStreamResponse() {
        return factory.createObjectLiteralExpression([
            factory.createPropertyAssignment(
                factory.createIdentifier("headers"),
                factory.createIdentifier("responseHeaders"),
            ),
            factory.createPropertyAssignment(
                factory.createIdentifier("status"),
                factory.createPropertyAccessExpression(
                    factory.createIdentifier("outgoingResponseModel"),
                    "status",
                ),
            ),
            factory.createPropertyAssignment(
                factory.createIdentifier("stream"),
                factory.createPropertyAccessExpression(
                    factory.createIdentifier("outgoingResponseModel"),
                    "stream",
                ),
            ),
        ], true);
    }

    function createTextResponse() {
        return factory.createObjectLiteralExpression([
            factory.createPropertyAssignment(
                factory.createIdentifier("headers"),
                factory.createIdentifier("responseHeaders"),
            ),
            factory.createPropertyAssignment(
                factory.createIdentifier("status"),
                factory.createPropertyAccessExpression(
                    factory.createIdentifier("outgoingResponseModel"),
                    "status",
                ),
            ),
            factory.createPropertyAssignment(
                factory.createIdentifier("stream"),
                factory.createCallExpression(
                    factory.createPropertyAccessExpression(
                        factory.createIdentifier("lib"),
                        factory.createIdentifier("serializeOutgoingTextContainer"),
                    ),
                    undefined,
                    [
                        factory.createIdentifier("outgoingResponseModel"),
                    ],
                ),
            ),
        ], true);
    }

    function createJsonResponse(
        modelTypeName: string,
    ) {
        const validateFn = stringifyIdentifier([
            "validate",
            modelTypeName,
        ]);

        return factory.createObjectLiteralExpression([
            factory.createPropertyAssignment(
                factory.createIdentifier("headers"),
                factory.createIdentifier("responseHeaders"),
            ),
            factory.createPropertyAssignment(
                factory.createIdentifier("status"),
                factory.createPropertyAccessExpression(
                    factory.createIdentifier("outgoingResponseModel"),
                    "status",
                ),
            ),
            factory.createPropertyAssignment(
                factory.createIdentifier("stream"),
                factory.createCallExpression(
                    factory.createPropertyAccessExpression(
                        factory.createIdentifier("lib"),
                        factory.createIdentifier("serializeOutgoingJsonContainer"),
                    ),
                    undefined,
                    [
                        factory.createIdentifier("outgoingResponseModel"),
                        // factory.createArrowFunction(
                        //     undefined,
                        //     undefined,
                        //     [factory.createParameterDeclaration(
                        //         undefined,
                        //         undefined,
                        //         undefined,
                        //         factory.createIdentifier("entity"),
                        //     )],
                        //     undefined,
                        //     factory.createToken(ts.SyntaxKind.EqualsGreaterThanToken),
                        //     factory.createBlock([
                        //         factory.createIfStatement(
                        //             factory.createPropertyAccessExpression(
                        //                 factory.createThis(),
                        //                 factory.createIdentifier("validateOutgoingEntities"),
                        //             ),
                        //             factory.createBlock([
                        //                 factory.createVariableStatement(
                        //                     undefined,
                        //                     // eslint-disable-next-line max-len
                        //                     factory.createVariableDeclarationList([factory.createVariableDeclaration(
                        //                         factory.createIdentifier("invalidPaths"),
                        //                         undefined,
                        //                         undefined,
                        //                         factory.createArrayLiteralExpression([
                        //                             // eslint-disable-next-line max-len
                        //                             factory.createSpreadElement(factory.createCallExpression(
                        //                                 factory.createPropertyAccessExpression(
                        //                                     factory.createIdentifier("shared"),
                        //                                     factory.createIdentifier(validateFn),
                        //                                 ),
                        //                                 undefined,
                        //                                 [factory.createIdentifier("entity")],
                        //                             )),
                        //                         ], false),
                        //                     )], ts.NodeFlags.Const),
                        //                 ),
                        //                 factory.createIfStatement(
                        //                     factory.createBinaryExpression(
                        //                         factory.createPropertyAccessExpression(
                        //                             factory.createIdentifier("invalidPaths"),
                        //                             factory.createIdentifier("length"),
                        //                         ),
                        //                         factory.createToken(ts.SyntaxKind.GreaterThanToken),
                        //                         factory.createNumericLiteral(0),
                        //                     ),
                        //                     factory.createBlock([
                        //                         // eslint-disable-next-line max-len
                        //                         factory.createThrowStatement(factory.createNewExpression(
                        //                             factory.createPropertyAccessExpression(
                        //                                 factory.createIdentifier("lib"),
                        //                                 factory.createIdentifier("OutgoingResponseEntityValidationError"),
                        //                             ),
                        //                             undefined,
                        //                             [factory.createIdentifier("invalidPaths")],
                        //                         )),
                        //                     ], true),
                        //                 ),
                        //             ]),
                        //         ),
                        //     ], true),
                        // ),
                    ],
                ),
            ),
        ], true);

    }

    function createFormResponse(
        modelTypeName: string,
    ) {
        const validateFn = stringifyIdentifier([
            "validate",
            modelTypeName,
        ]);

        return factory.createObjectLiteralExpression([
            factory.createPropertyAssignment(
                factory.createIdentifier("headers"),
                factory.createIdentifier("responseHeaders"),
            ),
            factory.createPropertyAssignment(
                factory.createIdentifier("status"),
                factory.createPropertyAccessExpression(
                    factory.createIdentifier("outgoingResponseModel"),
                    "status",
                ),
            ),
            factory.createPropertyAssignment(
                factory.createIdentifier("stream"),
                factory.createCallExpression(
                    factory.createPropertyAccessExpression(
                        factory.createIdentifier("lib"),
                        factory.createIdentifier("serializeOutgoingFormContainer"),
                    ),
                    undefined,
                    [
                        factory.createIdentifier("outgoingResponseModel"),
                        // factory.createArrowFunction(
                        //     undefined,
                        //     undefined,
                        //     [factory.createParameterDeclaration(
                        //         undefined,
                        //         undefined,
                        //         undefined,
                        //         factory.createIdentifier("entity"),
                        //     )],
                        //     undefined,
                        //     factory.createToken(ts.SyntaxKind.EqualsGreaterThanToken),
                        //     factory.createBlock([
                        //         factory.createIfStatement(
                        //             factory.createPropertyAccessExpression(
                        //                 factory.createThis(),
                        //                 factory.createIdentifier("validateOutgoingEntities"),
                        //             ),
                        //             factory.createBlock([
                        //                 factory.createVariableStatement(
                        //                     undefined,
                        //                     // eslint-disable-next-line max-len
                        //                     factory.createVariableDeclarationList([factory.createVariableDeclaration(
                        //                         factory.createIdentifier("invalidPaths"),
                        //                         undefined,
                        //                         undefined,
                        //                         factory.createArrayLiteralExpression([
                        //                             // eslint-disable-next-line max-len
                        //                             factory.createSpreadElement(factory.createCallExpression(
                        //                                 factory.createPropertyAccessExpression(
                        //                                     factory.createIdentifier("shared"),
                        //                                     factory.createIdentifier(validateFn),
                        //                                 ),
                        //                                 undefined,
                        //                                 [factory.createIdentifier("entity")],
                        //                             )),
                        //                         ], false),
                        //                     )], ts.NodeFlags.Const),
                        //                 ),
                        //                 factory.createIfStatement(
                        //                     factory.createBinaryExpression(
                        //                         factory.createPropertyAccessExpression(
                        //                             factory.createIdentifier("invalidPaths"),
                        //                             factory.createIdentifier("length"),
                        //                         ),
                        //                         factory.createToken(ts.SyntaxKind.GreaterThanToken),
                        //                         factory.createNumericLiteral(0),
                        //                     ),
                        //                     factory.createBlock([
                        //                         // eslint-disable-next-line max-len
                        //                         factory.createThrowStatement(factory.createNewExpression(
                        //                             factory.createPropertyAccessExpression(
                        //                                 factory.createIdentifier("lib"),
                        //                                 factory.createIdentifier("OutgoingResponseEntityValidationError"),
                        //                             ),
                        //                             undefined,
                        //                             [factory.createIdentifier("invalidPaths")],
                        //                         )),
                        //                     ], true),
                        //                 ),
                        //             ]),
                        //         ),
                        //     ], true),
                        // ),
                    ],
                ),
            ),
        ], true);

    }

}
