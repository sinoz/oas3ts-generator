import * as lib from "@oas3/oas3ts-lib";
import { OpenAPIV3 } from "openapi-types";
import ts from "typescript";
import { selectAllOperationRequestHeaders, selectAllOperationResponseHeaders, selectAllOperations } from "../../selectors/index.js";
import { generateLiteral } from "../../utils/index.js";
import { PackageConfig } from "../package.js";

export function createMetadataConstantDeclaration(
    factory: ts.NodeFactory,
    document: OpenAPIV3.Document,
    config: PackageConfig,
) {
    const metadata: lib.Metadata = {
        operationsMap: createOperationsMap(),
        availableRequestTypes: config.requestTypes,
        availableResponseTypes: config.responseTypes,
        requestHeadersMap: createRequestHeadersMap(),
        responseHeadersMap: createResponseHeadersMap(),
    };

    return factory.createVariableStatement(
        [
            factory.createToken(ts.SyntaxKind.ExportKeyword),
        ],
        factory.createVariableDeclarationList([
            factory.createVariableDeclaration(
                factory.createIdentifier("metadata"),
                undefined,
                factory.createTypeReferenceNode(
                    factory.createQualifiedName(
                        factory.createIdentifier("lib"),
                        "Metadata",
                    ),
                ),
                generateLiteral(factory, metadata),
            ),
        ], ts.NodeFlags.Const),
    );

    function createOperationsMap() {
        const result = {} as Record<string, Record<string, string>>;
        for (const { path, method, operationObject } of selectAllOperations(document)) {
            if (!operationObject.operationId) continue;
            result[path] ??= {};
            result[path][method.toUpperCase()] = operationObject.operationId;
        }
        return result;
    }

    function createRequestHeadersMap() {
        const result = {} as Record<string, string[]>;
        for (
            const { operationObject, headers } of
            selectAllOperationRequestHeaders(document, config.requestTypes, config.responseTypes)
        ) {
            if (!operationObject.operationId) continue;
            result[operationObject.operationId] = headers;
        }
        return result;
    }

    function createResponseHeadersMap() {
        const result = {} as Record<string, string[]>;
        for (
            const { operationObject, headers } of
            selectAllOperationResponseHeaders(document, config.responseTypes)
        ) {
            if (!operationObject.operationId) continue;
            result[operationObject.operationId] = headers;
        }
        return result;
    }

}

