import assert from "assert";
import { OpenAPIV3 } from "openapi-types";
import { isReferenceObject, resolveObject, resolvePointerParts } from "../utils/index.js";

export function selectAllResponses(
    document: OpenAPIV3.Document,
) {
    return selectFromDocument(document, [], []);

    function* selectFromDocument(
        document: OpenAPIV3.Document,
        pointerParts: Array<string>,
        nameParts: Array<string>,
    ) {
        for (
            const [path, pathObject] of
            Object.entries(document.paths)
        ) {
            yield* selectFromPath(
                pathObject,
                [...pointerParts, "paths", path],
                [...nameParts],
            );
        }

        for (
            const [response, responseObject] of
            Object.entries(document.components?.responses ?? {})
        ) {
            yield* selectFromResponse(
                responseObject,
                [...pointerParts, "components", "responses", response],
                [...nameParts, response],
            );
        }

    }

    function* selectFromPath(
        pathObject: OpenAPIV3.ReferenceObject | OpenAPIV3.PathItemObject | undefined,
        pointerParts: Array<string>,
        nameParts: Array<string>,
    ) {
        if (!pathObject) return;
        if (isReferenceObject(pathObject)) return;

        for (const method of Object.values(OpenAPIV3.HttpMethods)) {
            const operationObject = pathObject[method];

            yield* selectFromOperation(
                operationObject,
                [...pointerParts, method],
                [...nameParts],
            );
        }
    }

    function* selectFromOperation(
        operationObject: OpenAPIV3.OperationObject | undefined,
        pointerParts: Array<string>,
        nameParts: Array<string>,
    ) {
        if (!operationObject) return;

        assert(operationObject.operationId);

        for (
            const [response, responseObject] of
            Object.entries(operationObject.responses ?? {})
        ) {
            yield* selectFromResponse(
                responseObject,
                [...pointerParts, "responses", response],
                [...nameParts, operationObject.operationId, response],
            );
        }

    }

    function* selectFromResponse(
        responseObject: OpenAPIV3.ReferenceObject | OpenAPIV3.ResponseObject,
        responsePointerParts: Array<string>,
        nameParts: Array<string>,
    ) {
        if (isReferenceObject(responseObject)) return;

        yield {
            responseObject,
            responsePointerParts,
            pointerParts: responsePointerParts,
            nameParts: [...nameParts, "response"],
        };
    }

}

export function* selectReponsesFromOperation(
    document: OpenAPIV3.Document,
    operationObject: OpenAPIV3.OperationObject,
    operationPointerParts: string[],
) {
    const responseEntries = Object.entries(operationObject.responses);
    responseEntries.sort(([a], [b]) => {
        if (a > b) return +1;
        if (a < b) return -1;
        return 0;
    });

    for (const [statusKind, maybeResponseObject] of responseEntries) {
        const responseObject = resolveObject(
            document,
            maybeResponseObject,
        );
        assert(responseObject);

        const responsePointerParts = resolvePointerParts(
            [...operationPointerParts, "responses", statusKind],
            maybeResponseObject,
        );
        assert(responsePointerParts);

        yield {
            statusKind,
            responseObject,
            responsePointerParts,
        };
    }

}
