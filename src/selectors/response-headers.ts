import { OpenAPIV3 } from "openapi-types";
import { selectResponseContentTypes } from "./content-types.js";
import { selectAllOperations } from "./operation.js";
import { selectHeadersFromResponse } from "./response-parameter.js";
import { selectReponsesFromOperation } from "./response.js";

export function* selectAllOperationResponseHeaders(
    document: OpenAPIV3.Document,
    availableResponseTypes: string[],
) {
    for (
        const { operationObject, pathObject, path, method }
        of selectAllOperations(document)
    ) {
        const headers = new Set<string>();

        const responseContentTypes = new Set(
            selectResponseContentTypes(document, availableResponseTypes, operationObject),
        );

        if (responseContentTypes.size > 0) {
            headers.add("content-type");
        }

        for (
            const { responseObject, responsePointerParts } of
            selectReponsesFromOperation(document, operationObject, ["paths", path, method])
        ) {
            for (
                const { header } of
                selectHeadersFromResponse(document, responseObject, responsePointerParts)
            ) {
                headers.add(header);
            }
        }

        yield {
            operationObject,
            pathObject,
            path,
            method,
            headers: [...headers],
        };
    }
}
