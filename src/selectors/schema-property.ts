import assert from "assert";
import { OpenAPIV3 } from "openapi-types";
import { resolveObject, resolvePointerParts } from "../utils/index.js";

export function* selectPropertiesFromSchema(
    document: OpenAPIV3.Document,
    schemaObject: OpenAPIV3.SchemaObject,
    schemaPointerParts: string[],
) {
    for (
        const [property, maybePropertySchemaObject] of
        Object.entries(schemaObject.properties ?? {})
    ) {
        const propertySchemaObject = resolveObject(
            document,
            maybePropertySchemaObject,
        );
        assert(propertySchemaObject);

        const propertySchemaPointerParts = resolvePointerParts(
            [...schemaPointerParts, "properties", property],
            maybePropertySchemaObject,
        );
        assert(propertySchemaPointerParts);

        yield {
            property,
            propertySchemaObject,
            propertySchemaPointerParts,
        };
    }
}
