import { OpenAPIV3 } from "openapi-types";

export type ParameterIn =
    "path" |
    "query" |
    "header" |
    "cookie";

export type ParameterStyle =
    "simple" |
    "label" |
    "matrix" |
    "form" |
    "spaceDelimited" |
    "pipeDelimited" |
    "deepObject";

export function selectParameterStyle(
    parameterIn: ParameterIn,
    parameterObject: OpenAPIV3.ParameterBaseObject,
) {
    if (parameterObject.style != null)
        return parameterObject.style as ParameterStyle;

    switch (parameterIn) {
        case "path":
        case "header":
            return "simple";

        case "query":
        case "cookie":
            return "form";
    }
}

export function selectParameterExplode(
    parameterIn: "path" | "query" | "header" | "cookie",
    parameterObject: OpenAPIV3.ParameterBaseObject,
) {
    if (parameterObject.explode != null)
        return parameterObject.explode;

    switch (parameterIn) {
        case "path":
        case "header":
            return false;

        case "query":
        case "cookie":
            return true;
    }
}

export function selectParameterProperties(
    parameterIn: "path" | "query" | "header" | "cookie",
    parameterObject: OpenAPIV3.ParameterBaseObject,
) {
    const parameterStyle = selectParameterStyle(
        parameterIn,
        parameterObject,
    );
    const parameterExplode = selectParameterExplode(
        parameterIn,
        parameterObject,
    );

    let prefix: string | null;
    let assignment: string | null;
    let glue: string | null;
    let deepObject: boolean;

    switch (parameterStyle) {
        case "simple":
            deepObject = false;
            prefix = null;
            if (parameterExplode) {
                assignment = "=";
            }
            else {
                assignment = ",";
            }
            glue = ",";
            break;

        case "label":
            deepObject = false;
            prefix = ".";
            if (parameterExplode) {
                assignment = "=";
                glue = ".";
            }
            else {
                assignment = ",";
                glue = ",";
            }
            break;

        case "form":
            deepObject = false;
            prefix = null;
            assignment = null;
            if (parameterExplode) {
                assignment = null;
                glue = null;
            }
            else {
                assignment = ",";
                glue = ",";
            }
            break;

        case "deepObject":
            deepObject = true;
            prefix = null;
            assignment = null;
            glue = null;
            break;

        case "matrix":
        case "spaceDelimited":
        case "pipeDelimited":
            throw new Error("parameter style not supported");
    }
    return { deepObject, prefix, assignment, glue };
}
