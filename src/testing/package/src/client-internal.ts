/* eslint-disable */

import * as lib from "@oas3/oas3ts-lib";
import * as shared from "./shared.js";
export function injectBasicCredential(parameters: lib.Parameters, value: undefined | shared.BasicCredential) {
    if (value == null)
        return;
    const parameterValue = lib.stringifyBasicAuthorizationHeader(value);
    lib.addParameter(parameters, "authorization", parameterValue);
}
export function injectApiTokenCredential(parameters: lib.Parameters, value: undefined | shared.ApiTokenCredential) {
    if (value == null)
        return;
    const parameterValue = lib.stringifyAuthorizationHeader("bearer", value);
    lib.addParameter(parameters, "authorization", parameterValue);
}
export function injectApiKeyCredential(parameters: lib.Parameters, value: undefined | shared.ApiKeyCredential) {
    if (value == null)
        return;
    lib.addParameter(parameters, "Authorization".toLowerCase(), value);
}
export function injectAccessTokenCredential(parameters: lib.Parameters, value: undefined | shared.AccessTokenCredential) {
    if (value == null)
        return;
    lib.addParameter(parameters, "access_token", encodeURIComponent(value));
}
export function injectSessionMetricsOrganizationRequestParameter(parameters: lib.Parameters, value: undefined | shared.SessionMetricsOrganizationParameterSchema) {
    if (value == null)
        return;
    lib.addParameter(parameters, "organization", encodeURIComponent(value));
}
export function injectSimpleStringParameterVAlueRequestParameter(parameters: lib.Parameters, value: undefined | shared.SimpleStringParameterVAlueParameterSchema) {
    if (value == null)
        return;
    lib.addParameter(parameters, "V-Alue".toLowerCase(), value);
}
export function injectSimpleNumberParameterVAlueRequestParameter(parameters: lib.Parameters, value: undefined | shared.SimpleNumberParameterVAlueParameterSchema) {
    if (value == null)
        return;
    lib.addParameter(parameters, "V-Alue".toLowerCase(), value.toString());
}
export function injectSimpleArrayParameterVAlueRequestParameter(parameters: lib.Parameters, value: undefined | shared.SimpleArrayParameterVAlueParameterSchema) {
    if (value == null)
        return;
    lib.addParameter(parameters, "V-Alue".toLowerCase(), value.map(elementValue => elementValue).join(","));
}
export function injectSimpleExplodeArrayParameterVAlueRequestParameter(parameters: lib.Parameters, value: undefined | shared.SimpleExplodeArrayParameterVAlueParameterSchema) {
    if (value == null)
        return;
    lib.addParameter(parameters, "V-Alue".toLowerCase(), value.map(elementValue => elementValue).join(","));
}
export function injectSimpleObjectParameterVAlueRequestParameter(parameters: lib.Parameters, value: undefined | shared.SimpleObjectParameterVAlueParameterSchema) {
    if (value == null)
        return;
    const parameterEntries = new Array<[
        string,
        string
    ]>();
    {
        const propertyValue = value["str"];
        if (propertyValue != null) {
            parameterEntries.push(["str", propertyValue]);
        }
    }
    {
        const propertyValue = value["num"];
        if (propertyValue != null) {
            parameterEntries.push(["num", propertyValue.toString()]);
        }
    }
    const parameterValue = lib.stringifyParameterEntries(parameterEntries, "", ",", ",");
    lib.addParameter(parameters, "V-Alue".toLowerCase(), parameterValue);
}
export function injectSimpleExplodeObjectParameterVAlueRequestParameter(parameters: lib.Parameters, value: undefined | shared.SimpleExplodeObjectParameterVAlueParameterSchema) {
    if (value == null)
        return;
    const parameterEntries = new Array<[
        string,
        string
    ]>();
    {
        const propertyValue = value["str"];
        if (propertyValue != null) {
            parameterEntries.push(["str", propertyValue]);
        }
    }
    {
        const propertyValue = value["num"];
        if (propertyValue != null) {
            parameterEntries.push(["num", propertyValue.toString()]);
        }
    }
    const parameterValue = lib.stringifyParameterEntries(parameterEntries, "", ",", "=");
    lib.addParameter(parameters, "V-Alue".toLowerCase(), parameterValue);
}
export function injectSimpleDictionaryParameterVAlueRequestParameter(parameters: lib.Parameters, value: undefined | shared.SimpleDictionaryParameterVAlueParameterSchema) {
    if (value == null)
        return;
    const parameterEntries = new Array<[
        string,
        string
    ]>();
    for (const [propertyName, propertyValue] of Object.entries(value)) {
        if (propertyValue != null) {
            parameterEntries.push([propertyName, propertyValue]);
        }
    }
    const parameterValue = lib.stringifyParameterEntries(parameterEntries, "", ",", ",");
    lib.addParameter(parameters, "V-Alue".toLowerCase(), parameterValue);
}
export function injectSimpleExplodeDictionaryParameterVAlueRequestParameter(parameters: lib.Parameters, value: undefined | shared.SimpleExplodeDictionaryParameterVAlueParameterSchema) {
    if (value == null)
        return;
    const parameterEntries = new Array<[
        string,
        string
    ]>();
    for (const [propertyName, propertyValue] of Object.entries(value)) {
        if (propertyValue != null) {
            parameterEntries.push([propertyName, propertyValue]);
        }
    }
    const parameterValue = lib.stringifyParameterEntries(parameterEntries, "", ",", "=");
    lib.addParameter(parameters, "V-Alue".toLowerCase(), parameterValue);
}
export function injectLabelStringParameterVAlueRequestParameter(parameters: lib.Parameters, value: undefined | shared.LabelStringParameterVAlueParameterSchema) {
    if (value == null)
        return;
    lib.addParameter(parameters, "V-Alue", "." + encodeURIComponent(value));
}
export function injectLabelNumberParameterVAlueRequestParameter(parameters: lib.Parameters, value: undefined | shared.LabelNumberParameterVAlueParameterSchema) {
    if (value == null)
        return;
    lib.addParameter(parameters, "V-Alue", "." + value.toString());
}
export function injectLabelArrayParameterVAlueRequestParameter(parameters: lib.Parameters, value: undefined | shared.LabelArrayParameterVAlueParameterSchema) {
    if (value == null)
        return;
    lib.addParameter(parameters, "V-Alue", "." + value.map(elementValue => encodeURIComponent(elementValue)).join(","));
}
export function injectLabelExplodeArrayParameterVAlueRequestParameter(parameters: lib.Parameters, value: undefined | shared.LabelExplodeArrayParameterVAlueParameterSchema) {
    if (value == null)
        return;
    lib.addParameter(parameters, "V-Alue", "." + value.map(elementValue => encodeURIComponent(elementValue)).join("."));
}
export function injectLabelObjectParameterVAlueRequestParameter(parameters: lib.Parameters, value: undefined | shared.LabelObjectParameterVAlueParameterSchema) {
    if (value == null)
        return;
    const parameterEntries = new Array<[
        string,
        string
    ]>();
    {
        const propertyValue = value["str"];
        if (propertyValue != null) {
            parameterEntries.push(["str", encodeURIComponent(propertyValue)]);
        }
    }
    {
        const propertyValue = value["num"];
        if (propertyValue != null) {
            parameterEntries.push(["num", propertyValue.toString()]);
        }
    }
    const parameterValue = lib.stringifyParameterEntries(parameterEntries, ".", ",", ",");
    lib.addParameter(parameters, "V-Alue", parameterValue);
}
export function injectLabelExplodeObjectParameterVAlueRequestParameter(parameters: lib.Parameters, value: undefined | shared.LabelExplodeObjectParameterVAlueParameterSchema) {
    if (value == null)
        return;
    const parameterEntries = new Array<[
        string,
        string
    ]>();
    {
        const propertyValue = value["str"];
        if (propertyValue != null) {
            parameterEntries.push(["str", encodeURIComponent(propertyValue)]);
        }
    }
    {
        const propertyValue = value["num"];
        if (propertyValue != null) {
            parameterEntries.push(["num", propertyValue.toString()]);
        }
    }
    const parameterValue = lib.stringifyParameterEntries(parameterEntries, ".", ".", "=");
    lib.addParameter(parameters, "V-Alue", parameterValue);
}
export function injectLabelDictionaryParameterVAlueRequestParameter(parameters: lib.Parameters, value: undefined | shared.LabelDictionaryParameterVAlueParameterSchema) {
    if (value == null)
        return;
    const parameterEntries = new Array<[
        string,
        string
    ]>();
    for (const [propertyName, propertyValue] of Object.entries(value)) {
        if (propertyValue != null) {
            parameterEntries.push([propertyName, encodeURIComponent(propertyValue)]);
        }
    }
    const parameterValue = lib.stringifyParameterEntries(parameterEntries, ".", ",", ",");
    lib.addParameter(parameters, "V-Alue", parameterValue);
}
export function injectLabelExplodeDictionaryParameterVAlueRequestParameter(parameters: lib.Parameters, value: undefined | shared.LabelExplodeDictionaryParameterVAlueParameterSchema) {
    if (value == null)
        return;
    const parameterEntries = new Array<[
        string,
        string
    ]>();
    for (const [propertyName, propertyValue] of Object.entries(value)) {
        if (propertyValue != null) {
            parameterEntries.push([propertyName, encodeURIComponent(propertyValue)]);
        }
    }
    const parameterValue = lib.stringifyParameterEntries(parameterEntries, ".", ".", "=");
    lib.addParameter(parameters, "V-Alue", parameterValue);
}
export function injectFormStringParameterVAlueRequestParameter(parameters: lib.Parameters, value: undefined | shared.FormStringParameterVAlueParameterSchema) {
    if (value == null)
        return;
    lib.addParameter(parameters, "V-Alue", encodeURIComponent(value));
}
export function injectFormNumberParameterVAlueRequestParameter(parameters: lib.Parameters, value: undefined | shared.FormNumberParameterVAlueParameterSchema) {
    if (value == null)
        return;
    lib.addParameter(parameters, "V-Alue", value.toString());
}
export function injectFormArrayParameterVAlueRequestParameter(parameters: lib.Parameters, value: undefined | shared.FormArrayParameterVAlueParameterSchema) {
    if (value == null)
        return;
    lib.addParameter(parameters, "V-Alue", value.map(elementValue => encodeURIComponent(elementValue)).join(","));
}
export function injectFormExplodeArrayParameterVAlueRequestParameter(parameters: lib.Parameters, value: undefined | shared.FormExplodeArrayParameterVAlueParameterSchema) {
    if (value == null)
        return;
    for (const valueElement of value) {
        lib.addParameter(parameters, "V-Alue", encodeURIComponent(valueElement));
    }
}
export function injectFormObjectParameterVAlueRequestParameter(parameters: lib.Parameters, value: undefined | shared.FormObjectParameterVAlueParameterSchema) {
    if (value == null)
        return;
    const parameterEntries = new Array<[
        string,
        string
    ]>();
    {
        const propertyValue = value["str"];
        if (propertyValue != null) {
            parameterEntries.push(["str", encodeURIComponent(propertyValue)]);
        }
    }
    {
        const propertyValue = value["num"];
        if (propertyValue != null) {
            parameterEntries.push(["num", propertyValue.toString()]);
        }
    }
    const parameterValue = lib.stringifyParameterEntries(parameterEntries, "", ",", ",");
    lib.addParameter(parameters, "V-Alue", parameterValue);
}
export function injectFormExplodeObjectParameterVAlueRequestParameter(parameters: lib.Parameters, value: undefined | shared.FormExplodeObjectParameterVAlueParameterSchema) {
    if (value == null)
        return;
    {
        const propertyValue = value["str"];
        if (propertyValue != null) {
            lib.addParameter(parameters, "str", encodeURIComponent(propertyValue));
        }
    }
    {
        const propertyValue = value["num"];
        if (propertyValue != null) {
            lib.addParameter(parameters, "num", propertyValue.toString());
        }
    }
}
export function injectFormDictionaryParameterVAlueRequestParameter(parameters: lib.Parameters, value: undefined | shared.FormDictionaryParameterVAlueParameterSchema) {
    if (value == null)
        return;
    const parameterEntries = new Array<[
        string,
        string
    ]>();
    for (const [propertyName, propertyValue] of Object.entries(value)) {
        if (propertyValue != null) {
            parameterEntries.push([propertyName, encodeURIComponent(propertyValue)]);
        }
    }
    const parameterValue = lib.stringifyParameterEntries(parameterEntries, "", ",", ",");
    lib.addParameter(parameters, "V-Alue", parameterValue);
}
export function injectFormExplodeDictionaryParameterVAlueRequestParameter(parameters: lib.Parameters, value: undefined | shared.FormExplodeDictionaryParameterVAlueParameterSchema) {
    if (value == null)
        return;
    for (const [propertyName, propertyValue] of Object.entries(value)) {
        if (propertyValue != null) {
            lib.addParameter(parameters, propertyName, encodeURIComponent(propertyValue));
        }
    }
}
export function injectDeepObjectObjectParameterVAlueRequestParameter(parameters: lib.Parameters, value: undefined | shared.DeepObjectObjectParameterVAlueParameterSchema) {
    if (value == null)
        return;
    {
        const propertyValue = value["str"];
        if (propertyValue != null) {
            const parameterName = lib.stringifyDeepObjectPropertyName("V-Alue", "str");
            lib.addParameter(parameters, parameterName, encodeURIComponent(propertyValue));
        }
    }
    {
        const propertyValue = value["num"];
        if (propertyValue != null) {
            const parameterName = lib.stringifyDeepObjectPropertyName("V-Alue", "num");
            lib.addParameter(parameters, parameterName, propertyValue.toString());
        }
    }
}
export function injectDeepObjectDictionaryParameterVAlueRequestParameter(parameters: lib.Parameters, value: undefined | shared.DeepObjectDictionaryParameterVAlueParameterSchema) {
    if (value == null)
        return;
    for (const [propertyName, propertyValue] of Object.entries(value)) {
        if (propertyValue != null) {
            const parameterName = lib.stringifyDeepObjectPropertyName("V-Alue", propertyName);
            lib.addParameter(parameters, parameterName, encodeURIComponent(propertyValue));
        }
    }
}
export function serializeEchoApplicationJson(container: lib.OutgoingJsonContainer<shared.EchoMessageSchema>, validate: boolean): (signal?: AbortSignal) => AsyncIterable<Uint8Array> {
    if ("entity" in container) {
        return () => lib.serializeJsonEntity((async function () {
            try {
                const entity = await container.entity();
                if (validate) {
                    const invalidPaths = [...shared.validateEchoMessageSchema(entity)];
                    if (invalidPaths.length > 0) {
                        throw new lib.OutgoingRequestEntityValidationError(invalidPaths);
                    }
                }
                return entity;
            }
            catch (error) {
                if (error instanceof lib.SerializationError) {
                    throw new lib.OutgoingRequestEntitySerializationError();
                }
                throw error;
            }
        })());
    }
    if ("entities" in container) {
        return (signal?: AbortSignal) => lib.serializeJsonEntities((async function* () {
            try {
                for await (const entity of container.entities(signal)) {
                    if (validate) {
                        const invalidPaths = [...shared.validateEchoMessageSchema(entity)];
                        if (invalidPaths.length > 0) {
                            throw new lib.OutgoingRequestEntityValidationError(invalidPaths);
                        }
                    }
                    yield entity;
                }
            }
            catch (error) {
                if (error instanceof lib.SerializationError) {
                    throw new lib.OutgoingRequestEntitySerializationError();
                }
                throw error;
            }
        })());
    }
    if ("stream" in container) {
        return container.stream;
    }
    throw new TypeError("bad container");
}
export function serializeEchoApplicationXWwwFormUrlencoded(container: lib.OutgoingFormContainer<shared.EchoMessageSchema>, validate: boolean): (signal?: AbortSignal) => AsyncIterable<Uint8Array> {
    if ("entity" in container) {
        return () => lib.serializeFormEntity((async function () {
            try {
                const entity = await container.entity();
                if (validate) {
                    const invalidPaths = [...shared.validateEchoMessageSchema(entity)];
                    if (invalidPaths.length > 0) {
                        throw new lib.OutgoingRequestEntityValidationError(invalidPaths);
                    }
                }
                return entity;
            }
            catch (error) {
                if (error instanceof lib.SerializationError) {
                    throw new lib.OutgoingRequestEntitySerializationError();
                }
                throw error;
            }
        })());
    }
    if ("stream" in container) {
        return container.stream;
    }
    throw new TypeError("bad container");
}
export function serializeEchoTextPlain(container: lib.OutgoingTextContainer, validate: boolean): (signal?: AbortSignal) => AsyncIterable<Uint8Array> {
    if ("value" in container) {
        return () => lib.serializeTextValue((async function () {
            try {
                const value = await container.value();
                return value;
            }
            catch (error) {
                if (error instanceof lib.SerializationError) {
                    throw new lib.OutgoingRequestEntitySerializationError();
                }
                throw error;
            }
        })());
    }
    if ("lines" in container) {
        return (signal?: AbortSignal) => lib.serializeTextLines((async function* () {
            try {
                const lines = container.lines(signal);
                yield* lines;
            }
            catch (error) {
                if (error instanceof lib.SerializationError) {
                    throw new lib.OutgoingRequestEntitySerializationError();
                }
                throw error;
            }
        })());
    }
    if ("stream" in container) {
        return container.stream;
    }
    throw new TypeError("bad container");
}
export function serializeEchoApplicationOctetStream(container: lib.OutgoingStreamContainer, validate: boolean): (signal?: AbortSignal) => AsyncIterable<Uint8Array> {
    return container.stream;
}
export function deserializeSessionMetrics200ResponseApplicationJson(stream: (signal?: AbortSignal) => AsyncIterable<Uint8Array>, validate: boolean): lib.IncomingJsonContainer<shared.SessionMetrics200ApplicationJsonResponseSchema> {
    return {
        stream,
        async entity() {
            try {
                const entity = await lib.deserializeJsonEntity<shared.SessionMetrics200ApplicationJsonResponseSchema>(stream);
                if (validate) {
                    const invalidPaths = [...shared.validateSessionMetrics200ApplicationJsonResponseSchema(entity)];
                    if (invalidPaths.length > 0) {
                        throw new lib.IncomingResponseEntityValidationError(invalidPaths);
                    }
                }
                return entity;
            }
            catch (error) {
                if (error instanceof lib.DeserializationError) {
                    throw new lib.IncomingResponseEntityDeserializationError();
                }
                throw error;
            }
        },
        async *entities(signal?: AbortSignal) {
            try {
                for await (const entity of lib.deserializeJsonEntities<shared.SessionMetrics200ApplicationJsonResponseSchema>(stream, signal)) {
                    if (validate) {
                        const invalidPaths = [...shared.validateSessionMetrics200ApplicationJsonResponseSchema(entity)];
                        if (invalidPaths.length > 0) {
                            throw new lib.IncomingResponseEntityValidationError(invalidPaths);
                        }
                    }
                    yield entity;
                }
            }
            catch (error) {
                if (error instanceof lib.DeserializationError) {
                    throw new lib.IncomingResponseEntityDeserializationError();
                }
                throw error;
            }
        }
    };
}
export function deserializeMetrics200ResponseTextPlain(stream: (signal?: AbortSignal) => AsyncIterable<Uint8Array>, validate: boolean): lib.IncomingTextContainer {
    return {
        stream,
        async value() {
            try {
                const value = await lib.deserializeTextValue(stream);
                return value;
            }
            catch (error) {
                if (error instanceof lib.DeserializationError) {
                    throw new lib.IncomingResponseEntityDeserializationError();
                }
                throw error;
            }
        },
        async *lines(signal?: AbortSignal) {
            try {
                const lines = lib.deserializeTextLines(stream, signal);
                yield* lines;
            }
            catch (error) {
                if (error instanceof lib.DeserializationError) {
                    throw new lib.IncomingResponseEntityDeserializationError();
                }
                throw error;
            }
        }
    };
}
export function deserializeEcho200ResponseApplicationJson(stream: (signal?: AbortSignal) => AsyncIterable<Uint8Array>, validate: boolean): lib.IncomingJsonContainer<shared.EchoMessageSchema> {
    return {
        stream,
        async entity() {
            try {
                const entity = await lib.deserializeJsonEntity<shared.EchoMessageSchema>(stream);
                if (validate) {
                    const invalidPaths = [...shared.validateEchoMessageSchema(entity)];
                    if (invalidPaths.length > 0) {
                        throw new lib.IncomingResponseEntityValidationError(invalidPaths);
                    }
                }
                return entity;
            }
            catch (error) {
                if (error instanceof lib.DeserializationError) {
                    throw new lib.IncomingResponseEntityDeserializationError();
                }
                throw error;
            }
        },
        async *entities(signal?: AbortSignal) {
            try {
                for await (const entity of lib.deserializeJsonEntities<shared.EchoMessageSchema>(stream, signal)) {
                    if (validate) {
                        const invalidPaths = [...shared.validateEchoMessageSchema(entity)];
                        if (invalidPaths.length > 0) {
                            throw new lib.IncomingResponseEntityValidationError(invalidPaths);
                        }
                    }
                    yield entity;
                }
            }
            catch (error) {
                if (error instanceof lib.DeserializationError) {
                    throw new lib.IncomingResponseEntityDeserializationError();
                }
                throw error;
            }
        }
    };
}
export function deserializeEcho200ResponseApplicationXWwwFormUrlencoded(stream: (signal?: AbortSignal) => AsyncIterable<Uint8Array>, validate: boolean): lib.IncomingFormContainer<shared.EchoMessageSchema> {
    return {
        stream,
        async entity() {
            try {
                const entity = await lib.deserializeFormEntity<shared.EchoMessageSchema>(stream);
                if (validate) {
                    const invalidPaths = [...shared.validateEchoMessageSchema(entity)];
                    if (invalidPaths.length > 0) {
                        throw new lib.IncomingResponseEntityValidationError(invalidPaths);
                    }
                }
                return entity;
            }
            catch (error) {
                if (error instanceof lib.DeserializationError) {
                    throw new lib.IncomingResponseEntityDeserializationError();
                }
                throw error;
            }
        }
    };
}
export function deserializeEcho200ResponseTextPlain(stream: (signal?: AbortSignal) => AsyncIterable<Uint8Array>, validate: boolean): lib.IncomingTextContainer {
    return {
        stream,
        async value() {
            try {
                const value = await lib.deserializeTextValue(stream);
                return value;
            }
            catch (error) {
                if (error instanceof lib.DeserializationError) {
                    throw new lib.IncomingResponseEntityDeserializationError();
                }
                throw error;
            }
        },
        async *lines(signal?: AbortSignal) {
            try {
                const lines = lib.deserializeTextLines(stream, signal);
                yield* lines;
            }
            catch (error) {
                if (error instanceof lib.DeserializationError) {
                    throw new lib.IncomingResponseEntityDeserializationError();
                }
                throw error;
            }
        }
    };
}
export function deserializeEcho200ResponseApplicationOctetStream(stream: (signal?: AbortSignal) => AsyncIterable<Uint8Array>, validate: boolean): lib.IncomingStreamContainer {
    return { stream };
}
