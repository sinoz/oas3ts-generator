/* eslint-disable */

import { Promisable } from "type-fest";
import { Route } from "goodrouter";
import * as lib from "@oas3/oas3ts-lib";
import * as shared from "./shared.js";
import * as internal from "./server-internal.js";
export class Server<Authorization extends ServerAuthorization = ServerAuthorization> extends lib.ServerBase {
    constructor(options: lib.ServerOptions) {
        super(options);
        this.router.insertRoute("/session-metrics/{organization}", "/session-metrics/{organization}");
        this.router.insertRoute("/metrics", "/metrics");
        this.router.insertRoute("/echo", "/echo");
        this.router.insertRoute("/auth", "/auth");
        this.router.insertRoute("/parameter/string/simple", "/parameter/string/simple");
        this.router.insertRoute("/parameter/number/simple", "/parameter/number/simple");
        this.router.insertRoute("/parameter/array/simple", "/parameter/array/simple");
        this.router.insertRoute("/parameter/array/simple-explode", "/parameter/array/simple-explode");
        this.router.insertRoute("/parameter/object/simple", "/parameter/object/simple");
        this.router.insertRoute("/parameter/object/simple-explode", "/parameter/object/simple-explode");
        this.router.insertRoute("/parameter/dictionary/simple", "/parameter/dictionary/simple");
        this.router.insertRoute("/parameter/dictionary/simple-explode", "/parameter/dictionary/simple-explode");
        this.router.insertRoute("/parameter/string/label/{V-Alue}", "/parameter/string/label/{V-Alue}");
        this.router.insertRoute("/parameter/number/label/{V-Alue}", "/parameter/number/label/{V-Alue}");
        this.router.insertRoute("/parameter/array/label/{V-Alue}", "/parameter/array/label/{V-Alue}");
        this.router.insertRoute("/parameter/array/label-explode/{V-Alue}", "/parameter/array/label-explode/{V-Alue}");
        this.router.insertRoute("/parameter/object/label/{V-Alue}", "/parameter/object/label/{V-Alue}");
        this.router.insertRoute("/parameter/object/label-explode/{V-Alue}", "/parameter/object/label-explode/{V-Alue}");
        this.router.insertRoute("/parameter/dictionary/label/{V-Alue}", "/parameter/dictionary/label/{V-Alue}");
        this.router.insertRoute("/parameter/dictionary/label-explode/{V-Alue}", "/parameter/dictionary/label-explode/{V-Alue}");
        this.router.insertRoute("/parameter/string/form", "/parameter/string/form");
        this.router.insertRoute("/parameter/number/form", "/parameter/number/form");
        this.router.insertRoute("/parameter/array/form", "/parameter/array/form");
        this.router.insertRoute("/parameter/array/form-explode", "/parameter/array/form-explode");
        this.router.insertRoute("/parameter/object/form", "/parameter/object/form");
        this.router.insertRoute("/parameter/object/form-explode", "/parameter/object/form-explode");
        this.router.insertRoute("/parameter/dictionary/form", "/parameter/dictionary/form");
        this.router.insertRoute("/parameter/dictionary/form-explode", "/parameter/dictionary/form-explode");
        this.router.insertRoute("/parameter/object/deep-object", "/parameter/object/deep-object");
        this.router.insertRoute("/parameter/dictionary/deep-object", "/parameter/dictionary/deep-object");
        this.registerOperation("/session-metrics/{organization}", "GET", this.sessionMetrics);
        this.registerOperation("/metrics", "GET", this.metrics);
        this.registerOperation("/echo", "POST", this.echo);
        this.registerOperation("/auth", "GET", this.basicAuth);
        this.registerOperation("/parameter/string/simple", "GET", this.simpleStringParameter);
        this.registerOperation("/parameter/number/simple", "GET", this.simpleNumberParameter);
        this.registerOperation("/parameter/array/simple", "GET", this.simpleArrayParameter);
        this.registerOperation("/parameter/array/simple-explode", "GET", this.simpleExplodeArrayParameter);
        this.registerOperation("/parameter/object/simple", "GET", this.simpleObjectParameter);
        this.registerOperation("/parameter/object/simple-explode", "GET", this.simpleExplodeObjectParameter);
        this.registerOperation("/parameter/dictionary/simple", "GET", this.simpleDictionaryParameter);
        this.registerOperation("/parameter/dictionary/simple-explode", "GET", this.simpleExplodeDictionaryParameter);
        this.registerOperation("/parameter/string/label/{V-Alue}", "GET", this.labelStringParameter);
        this.registerOperation("/parameter/number/label/{V-Alue}", "GET", this.labelNumberParameter);
        this.registerOperation("/parameter/array/label/{V-Alue}", "GET", this.labelArrayParameter);
        this.registerOperation("/parameter/array/label-explode/{V-Alue}", "GET", this.labelExplodeArrayParameter);
        this.registerOperation("/parameter/object/label/{V-Alue}", "GET", this.labelObjectParameter);
        this.registerOperation("/parameter/object/label-explode/{V-Alue}", "GET", this.labelExplodeObjectParameter);
        this.registerOperation("/parameter/dictionary/label/{V-Alue}", "GET", this.labelDictionaryParameter);
        this.registerOperation("/parameter/dictionary/label-explode/{V-Alue}", "GET", this.labelExplodeDictionaryParameter);
        this.registerOperation("/parameter/string/form", "GET", this.formStringParameter);
        this.registerOperation("/parameter/number/form", "GET", this.formNumberParameter);
        this.registerOperation("/parameter/array/form", "GET", this.formArrayParameter);
        this.registerOperation("/parameter/array/form-explode", "GET", this.formExplodeArrayParameter);
        this.registerOperation("/parameter/object/form", "GET", this.formObjectParameter);
        this.registerOperation("/parameter/object/form-explode", "GET", this.formExplodeObjectParameter);
        this.registerOperation("/parameter/dictionary/form", "GET", this.formDictionaryParameter);
        this.registerOperation("/parameter/dictionary/form-explode", "GET", this.formExplodeDictionaryParameter);
        this.registerOperation("/parameter/object/deep-object", "GET", this.deepObjectObjectParameter);
        this.registerOperation("/parameter/dictionary/deep-object", "GET", this.deepObjectDictionaryParameter);
    }
    protected basicAuthorizationHandler?: BasicAuthorizationHandler<Authorization>;
    protected apiTokenAuthorizationHandler?: ApiTokenAuthorizationHandler<Authorization>;
    protected apiKeyAuthorizationHandler?: ApiKeyAuthorizationHandler<Authorization>;
    protected accessTokenAuthorizationHandler?: AccessTokenAuthorizationHandler<Authorization>;
    protected sessionMetricsOperationHandler?: SessionMetricsOperationHandler<Authorization>;
    protected metricsOperationHandler?: MetricsOperationHandler<Authorization>;
    protected echoOperationHandler?: EchoOperationHandler<Authorization>;
    protected basicAuthOperationHandler?: BasicAuthOperationHandler<Authorization>;
    protected simpleStringParameterOperationHandler?: SimpleStringParameterOperationHandler<Authorization>;
    protected simpleNumberParameterOperationHandler?: SimpleNumberParameterOperationHandler<Authorization>;
    protected simpleArrayParameterOperationHandler?: SimpleArrayParameterOperationHandler<Authorization>;
    protected simpleExplodeArrayParameterOperationHandler?: SimpleExplodeArrayParameterOperationHandler<Authorization>;
    protected simpleObjectParameterOperationHandler?: SimpleObjectParameterOperationHandler<Authorization>;
    protected simpleExplodeObjectParameterOperationHandler?: SimpleExplodeObjectParameterOperationHandler<Authorization>;
    protected simpleDictionaryParameterOperationHandler?: SimpleDictionaryParameterOperationHandler<Authorization>;
    protected simpleExplodeDictionaryParameterOperationHandler?: SimpleExplodeDictionaryParameterOperationHandler<Authorization>;
    protected labelStringParameterOperationHandler?: LabelStringParameterOperationHandler<Authorization>;
    protected labelNumberParameterOperationHandler?: LabelNumberParameterOperationHandler<Authorization>;
    protected labelArrayParameterOperationHandler?: LabelArrayParameterOperationHandler<Authorization>;
    protected labelExplodeArrayParameterOperationHandler?: LabelExplodeArrayParameterOperationHandler<Authorization>;
    protected labelObjectParameterOperationHandler?: LabelObjectParameterOperationHandler<Authorization>;
    protected labelExplodeObjectParameterOperationHandler?: LabelExplodeObjectParameterOperationHandler<Authorization>;
    protected labelDictionaryParameterOperationHandler?: LabelDictionaryParameterOperationHandler<Authorization>;
    protected labelExplodeDictionaryParameterOperationHandler?: LabelExplodeDictionaryParameterOperationHandler<Authorization>;
    protected formStringParameterOperationHandler?: FormStringParameterOperationHandler<Authorization>;
    protected formNumberParameterOperationHandler?: FormNumberParameterOperationHandler<Authorization>;
    protected formArrayParameterOperationHandler?: FormArrayParameterOperationHandler<Authorization>;
    protected formExplodeArrayParameterOperationHandler?: FormExplodeArrayParameterOperationHandler<Authorization>;
    protected formObjectParameterOperationHandler?: FormObjectParameterOperationHandler<Authorization>;
    protected formExplodeObjectParameterOperationHandler?: FormExplodeObjectParameterOperationHandler<Authorization>;
    protected formDictionaryParameterOperationHandler?: FormDictionaryParameterOperationHandler<Authorization>;
    protected formExplodeDictionaryParameterOperationHandler?: FormExplodeDictionaryParameterOperationHandler<Authorization>;
    protected deepObjectObjectParameterOperationHandler?: DeepObjectObjectParameterOperationHandler<Authorization>;
    protected deepObjectDictionaryParameterOperationHandler?: DeepObjectDictionaryParameterOperationHandler<Authorization>;
    public registerBasicAuthorization(handler: BasicAuthorizationHandler<Authorization>) {
        this.basicAuthorizationHandler = handler;
    }
    public registerApiTokenAuthorization(handler: ApiTokenAuthorizationHandler<Authorization>) {
        this.apiTokenAuthorizationHandler = handler;
    }
    public registerApiKeyAuthorization(handler: ApiKeyAuthorizationHandler<Authorization>) {
        this.apiKeyAuthorizationHandler = handler;
    }
    public registerAccessTokenAuthorization(handler: AccessTokenAuthorizationHandler<Authorization>) {
        this.accessTokenAuthorizationHandler = handler;
    }
    /**
    */
    public registerSessionMetricsOperation(handler: SessionMetricsOperationHandler<Authorization>) {
        this.sessionMetricsOperationHandler = handler;
    }
    /**
    */
    public registerMetricsOperation(handler: MetricsOperationHandler<Authorization>) {
        this.metricsOperationHandler = handler;
    }
    /**
    */
    public registerEchoOperation(handler: EchoOperationHandler<Authorization>) {
        this.echoOperationHandler = handler;
    }
    /**
    */
    public registerBasicAuthOperation(handler: BasicAuthOperationHandler<Authorization>) {
        this.basicAuthOperationHandler = handler;
    }
    /**
    */
    public registerSimpleStringParameterOperation(handler: SimpleStringParameterOperationHandler<Authorization>) {
        this.simpleStringParameterOperationHandler = handler;
    }
    /**
    */
    public registerSimpleNumberParameterOperation(handler: SimpleNumberParameterOperationHandler<Authorization>) {
        this.simpleNumberParameterOperationHandler = handler;
    }
    /**
    */
    public registerSimpleArrayParameterOperation(handler: SimpleArrayParameterOperationHandler<Authorization>) {
        this.simpleArrayParameterOperationHandler = handler;
    }
    /**
    */
    public registerSimpleExplodeArrayParameterOperation(handler: SimpleExplodeArrayParameterOperationHandler<Authorization>) {
        this.simpleExplodeArrayParameterOperationHandler = handler;
    }
    /**
    */
    public registerSimpleObjectParameterOperation(handler: SimpleObjectParameterOperationHandler<Authorization>) {
        this.simpleObjectParameterOperationHandler = handler;
    }
    /**
    */
    public registerSimpleExplodeObjectParameterOperation(handler: SimpleExplodeObjectParameterOperationHandler<Authorization>) {
        this.simpleExplodeObjectParameterOperationHandler = handler;
    }
    /**
    */
    public registerSimpleDictionaryParameterOperation(handler: SimpleDictionaryParameterOperationHandler<Authorization>) {
        this.simpleDictionaryParameterOperationHandler = handler;
    }
    /**
    */
    public registerSimpleExplodeDictionaryParameterOperation(handler: SimpleExplodeDictionaryParameterOperationHandler<Authorization>) {
        this.simpleExplodeDictionaryParameterOperationHandler = handler;
    }
    /**
    */
    public registerLabelStringParameterOperation(handler: LabelStringParameterOperationHandler<Authorization>) {
        this.labelStringParameterOperationHandler = handler;
    }
    /**
    */
    public registerLabelNumberParameterOperation(handler: LabelNumberParameterOperationHandler<Authorization>) {
        this.labelNumberParameterOperationHandler = handler;
    }
    /**
    */
    public registerLabelArrayParameterOperation(handler: LabelArrayParameterOperationHandler<Authorization>) {
        this.labelArrayParameterOperationHandler = handler;
    }
    /**
    */
    public registerLabelExplodeArrayParameterOperation(handler: LabelExplodeArrayParameterOperationHandler<Authorization>) {
        this.labelExplodeArrayParameterOperationHandler = handler;
    }
    /**
    */
    public registerLabelObjectParameterOperation(handler: LabelObjectParameterOperationHandler<Authorization>) {
        this.labelObjectParameterOperationHandler = handler;
    }
    /**
    */
    public registerLabelExplodeObjectParameterOperation(handler: LabelExplodeObjectParameterOperationHandler<Authorization>) {
        this.labelExplodeObjectParameterOperationHandler = handler;
    }
    /**
    */
    public registerLabelDictionaryParameterOperation(handler: LabelDictionaryParameterOperationHandler<Authorization>) {
        this.labelDictionaryParameterOperationHandler = handler;
    }
    /**
    */
    public registerLabelExplodeDictionaryParameterOperation(handler: LabelExplodeDictionaryParameterOperationHandler<Authorization>) {
        this.labelExplodeDictionaryParameterOperationHandler = handler;
    }
    /**
    */
    public registerFormStringParameterOperation(handler: FormStringParameterOperationHandler<Authorization>) {
        this.formStringParameterOperationHandler = handler;
    }
    /**
    */
    public registerFormNumberParameterOperation(handler: FormNumberParameterOperationHandler<Authorization>) {
        this.formNumberParameterOperationHandler = handler;
    }
    /**
    */
    public registerFormArrayParameterOperation(handler: FormArrayParameterOperationHandler<Authorization>) {
        this.formArrayParameterOperationHandler = handler;
    }
    /**
    */
    public registerFormExplodeArrayParameterOperation(handler: FormExplodeArrayParameterOperationHandler<Authorization>) {
        this.formExplodeArrayParameterOperationHandler = handler;
    }
    /**
    */
    public registerFormObjectParameterOperation(handler: FormObjectParameterOperationHandler<Authorization>) {
        this.formObjectParameterOperationHandler = handler;
    }
    /**
    */
    public registerFormExplodeObjectParameterOperation(handler: FormExplodeObjectParameterOperationHandler<Authorization>) {
        this.formExplodeObjectParameterOperationHandler = handler;
    }
    /**
    */
    public registerFormDictionaryParameterOperation(handler: FormDictionaryParameterOperationHandler<Authorization>) {
        this.formDictionaryParameterOperationHandler = handler;
    }
    /**
    */
    public registerFormExplodeDictionaryParameterOperation(handler: FormExplodeDictionaryParameterOperationHandler<Authorization>) {
        this.formExplodeDictionaryParameterOperationHandler = handler;
    }
    /**
    */
    public registerDeepObjectObjectParameterOperation(handler: DeepObjectObjectParameterOperationHandler<Authorization>) {
        this.deepObjectObjectParameterOperationHandler = handler;
    }
    /**
    */
    public registerDeepObjectDictionaryParameterOperation(handler: DeepObjectDictionaryParameterOperationHandler<Authorization>) {
        this.deepObjectDictionaryParameterOperationHandler = handler;
    }
    protected async authorizeSessionMetrics(credentials: SessionMetricsCredentials) {
        return {} as SessionMetricsAuthorization;
    }
    protected async authorizeMetrics(credentials: MetricsCredentials) {
        return {} as MetricsAuthorization;
    }
    protected async authorizeEcho(credentials: EchoCredentials) {
        return {} as EchoAuthorization;
    }
    protected async authorizeBasicAuth(credentials: BasicAuthCredentials) {
        const authorization: Partial<Authorization> = {};
        if (credentials.basic != null && this.basicAuthorizationHandler != null) {
            authorization.basic = await this.basicAuthorizationHandler(credentials.basic);
        }
        if (authorization.basic != null) {
            return authorization as BasicAuthAuthorization<Authorization>;
        }
        return;
    }
    protected async authorizeSimpleStringParameter(credentials: SimpleStringParameterCredentials) {
        return {} as SimpleStringParameterAuthorization;
    }
    protected async authorizeSimpleNumberParameter(credentials: SimpleNumberParameterCredentials) {
        return {} as SimpleNumberParameterAuthorization;
    }
    protected async authorizeSimpleArrayParameter(credentials: SimpleArrayParameterCredentials) {
        return {} as SimpleArrayParameterAuthorization;
    }
    protected async authorizeSimpleExplodeArrayParameter(credentials: SimpleExplodeArrayParameterCredentials) {
        return {} as SimpleExplodeArrayParameterAuthorization;
    }
    protected async authorizeSimpleObjectParameter(credentials: SimpleObjectParameterCredentials) {
        return {} as SimpleObjectParameterAuthorization;
    }
    protected async authorizeSimpleExplodeObjectParameter(credentials: SimpleExplodeObjectParameterCredentials) {
        return {} as SimpleExplodeObjectParameterAuthorization;
    }
    protected async authorizeSimpleDictionaryParameter(credentials: SimpleDictionaryParameterCredentials) {
        return {} as SimpleDictionaryParameterAuthorization;
    }
    protected async authorizeSimpleExplodeDictionaryParameter(credentials: SimpleExplodeDictionaryParameterCredentials) {
        return {} as SimpleExplodeDictionaryParameterAuthorization;
    }
    protected async authorizeLabelStringParameter(credentials: LabelStringParameterCredentials) {
        return {} as LabelStringParameterAuthorization;
    }
    protected async authorizeLabelNumberParameter(credentials: LabelNumberParameterCredentials) {
        return {} as LabelNumberParameterAuthorization;
    }
    protected async authorizeLabelArrayParameter(credentials: LabelArrayParameterCredentials) {
        return {} as LabelArrayParameterAuthorization;
    }
    protected async authorizeLabelExplodeArrayParameter(credentials: LabelExplodeArrayParameterCredentials) {
        return {} as LabelExplodeArrayParameterAuthorization;
    }
    protected async authorizeLabelObjectParameter(credentials: LabelObjectParameterCredentials) {
        return {} as LabelObjectParameterAuthorization;
    }
    protected async authorizeLabelExplodeObjectParameter(credentials: LabelExplodeObjectParameterCredentials) {
        return {} as LabelExplodeObjectParameterAuthorization;
    }
    protected async authorizeLabelDictionaryParameter(credentials: LabelDictionaryParameterCredentials) {
        return {} as LabelDictionaryParameterAuthorization;
    }
    protected async authorizeLabelExplodeDictionaryParameter(credentials: LabelExplodeDictionaryParameterCredentials) {
        return {} as LabelExplodeDictionaryParameterAuthorization;
    }
    protected async authorizeFormStringParameter(credentials: FormStringParameterCredentials) {
        return {} as FormStringParameterAuthorization;
    }
    protected async authorizeFormNumberParameter(credentials: FormNumberParameterCredentials) {
        return {} as FormNumberParameterAuthorization;
    }
    protected async authorizeFormArrayParameter(credentials: FormArrayParameterCredentials) {
        return {} as FormArrayParameterAuthorization;
    }
    protected async authorizeFormExplodeArrayParameter(credentials: FormExplodeArrayParameterCredentials) {
        return {} as FormExplodeArrayParameterAuthorization;
    }
    protected async authorizeFormObjectParameter(credentials: FormObjectParameterCredentials) {
        return {} as FormObjectParameterAuthorization;
    }
    protected async authorizeFormExplodeObjectParameter(credentials: FormExplodeObjectParameterCredentials) {
        return {} as FormExplodeObjectParameterAuthorization;
    }
    protected async authorizeFormDictionaryParameter(credentials: FormDictionaryParameterCredentials) {
        return {} as FormDictionaryParameterAuthorization;
    }
    protected async authorizeFormExplodeDictionaryParameter(credentials: FormExplodeDictionaryParameterCredentials) {
        return {} as FormExplodeDictionaryParameterAuthorization;
    }
    protected async authorizeDeepObjectObjectParameter(credentials: DeepObjectObjectParameterCredentials) {
        return {} as DeepObjectObjectParameterAuthorization;
    }
    protected async authorizeDeepObjectDictionaryParameter(credentials: DeepObjectDictionaryParameterCredentials) {
        return {} as DeepObjectDictionaryParameterAuthorization;
    }
    protected async sessionMetrics(route: Route, incomingRequest: lib.ServerIncomingRequest) {
        const handler = this.sessionMetricsOperationHandler;
        if (handler == null) {
            throw new lib.OperationNotRegisteredError("session-metrics");
        }
        const routeParameters = { ...route.parameters };
        const requestQuery = lib.parseParameters(incomingRequest.query);
        const requestHeaders = { ...incomingRequest.headers };
        const credentials = {};
        const authorization = await this.authorizeSessionMetrics(credentials);
        if (authorization == null) {
            throw new lib.AuthorizationFailedError("session-metrics");
        }
        let requestParameters;
        try {
            requestParameters = {
                organization: internal.extractSessionMetricsOrganizationRequestParameter(routeParameters)
            } as shared.SessionMetricsRequestParameters;
        }
        catch {
            throw new lib.IncomingRequestParameterExtractionError();
        }
        if (this.validateIncomingParameters) {
            const invalidPaths = [...shared.validateSessionMetricsRequestParameters(requestParameters)];
            if (invalidPaths.length > 0) {
                throw new lib.IncomingRequestParametersValidationError(invalidPaths);
            }
        }
        const requestAcceptTypes = lib.parseAcceptHeader(lib.getParameterValues(incomingRequest.headers, "accept"), shared.sessionMetricsAcceptTypes);
        const incomingRequestModel = {
            parameters: requestParameters,
            contentType: null
        };
        const outgoingResponseModel = await handler(incomingRequestModel, authorization, requestAcceptTypes);
        const responseHeaders: lib.Parameters = {};
        let outgoingResponse: lib.ServerOutgoingResponse;
        const responseStatus = outgoingResponseModel.status;
        switch (responseStatus) {
            case 200: {
                if (this.validateOutgoingParameters) {
                    const invalidPaths = [...shared.validateSessionMetrics200ResponseResponseParameters(outgoingResponseModel.parameters)];
                    if (invalidPaths.length > 0) {
                        throw new lib.OutgoingResponseParametersValidationError(invalidPaths);
                    }
                }
                try {
                }
                catch {
                    throw new lib.OutgoingResponseParameterInjectionError();
                }
                if ("contentType" in outgoingResponseModel) {
                    lib.addParameter(responseHeaders, "content-type", outgoingResponseModel.contentType);
                    switch (outgoingResponseModel.contentType) {
                        case "application/json":
                            outgoingResponse = {
                                headers: responseHeaders,
                                status: outgoingResponseModel.status,
                                stream: internal.serializeSessionMetrics200ResponseApplicationJson(outgoingResponseModel, this.validateOutgoingEntities)
                            };
                            break;
                        default: throw new TypeError("invalid content-type");
                    }
                }
                else {
                    lib.addParameter(responseHeaders, "content-type", "application/json");
                    outgoingResponse = {
                        headers: responseHeaders,
                        status: outgoingResponseModel.status,
                        stream: internal.serializeSessionMetrics200ResponseApplicationJson(outgoingResponseModel, this.validateOutgoingEntities)
                    };
                }
                break;
            }
            default: throw new TypeError("status code not supported");
        }
        return outgoingResponse;
    }
    protected async metrics(route: Route, incomingRequest: lib.ServerIncomingRequest) {
        const handler = this.metricsOperationHandler;
        if (handler == null) {
            throw new lib.OperationNotRegisteredError("metrics");
        }
        const routeParameters = { ...route.parameters };
        const requestQuery = lib.parseParameters(incomingRequest.query);
        const requestHeaders = { ...incomingRequest.headers };
        const credentials = {};
        const authorization = await this.authorizeMetrics(credentials);
        if (authorization == null) {
            throw new lib.AuthorizationFailedError("metrics");
        }
        let requestParameters;
        try {
            requestParameters = {} as shared.MetricsRequestParameters;
        }
        catch {
            throw new lib.IncomingRequestParameterExtractionError();
        }
        if (this.validateIncomingParameters) {
            const invalidPaths = [...shared.validateMetricsRequestParameters(requestParameters)];
            if (invalidPaths.length > 0) {
                throw new lib.IncomingRequestParametersValidationError(invalidPaths);
            }
        }
        const requestAcceptTypes = lib.parseAcceptHeader(lib.getParameterValues(incomingRequest.headers, "accept"), shared.metricsAcceptTypes);
        const incomingRequestModel = {
            parameters: requestParameters,
            contentType: null
        };
        const outgoingResponseModel = await handler(incomingRequestModel, authorization, requestAcceptTypes);
        const responseHeaders: lib.Parameters = {};
        let outgoingResponse: lib.ServerOutgoingResponse;
        const responseStatus = outgoingResponseModel.status;
        switch (responseStatus) {
            case 200: {
                if (this.validateOutgoingParameters) {
                    const invalidPaths = [...shared.validateMetrics200ResponseResponseParameters(outgoingResponseModel.parameters)];
                    if (invalidPaths.length > 0) {
                        throw new lib.OutgoingResponseParametersValidationError(invalidPaths);
                    }
                }
                try {
                }
                catch {
                    throw new lib.OutgoingResponseParameterInjectionError();
                }
                if ("contentType" in outgoingResponseModel) {
                    lib.addParameter(responseHeaders, "content-type", outgoingResponseModel.contentType);
                    switch (outgoingResponseModel.contentType) {
                        case "text/plain":
                            outgoingResponse = {
                                headers: responseHeaders,
                                status: outgoingResponseModel.status,
                                stream: internal.serializeMetrics200ResponseTextPlain(outgoingResponseModel, this.validateOutgoingEntities)
                            };
                            break;
                        default: throw new TypeError("invalid content-type");
                    }
                }
                else {
                    lib.addParameter(responseHeaders, "content-type", "text/plain");
                    outgoingResponse = {
                        headers: responseHeaders,
                        status: outgoingResponseModel.status,
                        stream: internal.serializeMetrics200ResponseTextPlain(outgoingResponseModel, this.validateOutgoingEntities)
                    };
                }
                break;
            }
            default: throw new TypeError("status code not supported");
        }
        return outgoingResponse;
    }
    protected async echo(route: Route, incomingRequest: lib.ServerIncomingRequest) {
        const handler = this.echoOperationHandler;
        if (handler == null) {
            throw new lib.OperationNotRegisteredError("echo");
        }
        const routeParameters = { ...route.parameters };
        const requestQuery = lib.parseParameters(incomingRequest.query);
        const requestHeaders = { ...incomingRequest.headers };
        const credentials = {};
        const authorization = await this.authorizeEcho(credentials);
        if (authorization == null) {
            throw new lib.AuthorizationFailedError("echo");
        }
        let requestParameters;
        try {
            requestParameters = {} as shared.EchoRequestParameters;
        }
        catch {
            throw new lib.IncomingRequestParameterExtractionError();
        }
        if (this.validateIncomingParameters) {
            const invalidPaths = [...shared.validateEchoRequestParameters(requestParameters)];
            if (invalidPaths.length > 0) {
                throw new lib.IncomingRequestParametersValidationError(invalidPaths);
            }
        }
        const requestAcceptTypes = lib.parseAcceptHeader(lib.getParameterValues(incomingRequest.headers, "accept"), shared.echoAcceptTypes);
        let incomingRequestModel: EchoIncomingRequest;
        const requestContentTypeHeader = lib.getParameterValue(incomingRequest.headers, "content-type");
        if (requestContentTypeHeader == null) {
            throw new lib.MissingRequestContentTypeError();
        }
        const requestContentType = lib.parseContentTypeHeader(requestContentTypeHeader);
        if (requestContentType == null) {
            throw new lib.MissingRequestContentTypeError();
        }
        switch (requestContentType) {
            case "application/json":
                incomingRequestModel = {
                    ...{
                        parameters: requestParameters,
                        contentType: "application/json"
                    },
                    ...internal.deserializeEchoApplicationJson(incomingRequest.stream, this.validateIncomingEntities)
                };
                break;
            case "application/x-www-form-urlencoded":
                incomingRequestModel = {
                    ...{
                        parameters: requestParameters,
                        contentType: "application/x-www-form-urlencoded"
                    },
                    ...internal.deserializeEchoApplicationXWwwFormUrlencoded(incomingRequest.stream, this.validateIncomingEntities)
                };
                break;
            case "text/plain":
                incomingRequestModel = {
                    ...{
                        parameters: requestParameters,
                        contentType: "text/plain"
                    },
                    ...internal.deserializeEchoTextPlain(incomingRequest.stream, this.validateIncomingEntities)
                };
                break;
            case "application/octet-stream":
                incomingRequestModel = {
                    ...{
                        parameters: requestParameters,
                        contentType: "application/octet-stream"
                    },
                    ...internal.deserializeEchoApplicationOctetStream(incomingRequest.stream, this.validateIncomingEntities)
                };
                break;
            default: throw new lib.UnexpectedRequestContentType(requestContentType);
        }
        const outgoingResponseModel = await handler(incomingRequestModel, authorization, requestAcceptTypes);
        const responseHeaders: lib.Parameters = {};
        let outgoingResponse: lib.ServerOutgoingResponse;
        const responseStatus = outgoingResponseModel.status;
        switch (responseStatus) {
            case 200: {
                if (this.validateOutgoingParameters) {
                    const invalidPaths = [...shared.validateEcho200ResponseResponseParameters(outgoingResponseModel.parameters)];
                    if (invalidPaths.length > 0) {
                        throw new lib.OutgoingResponseParametersValidationError(invalidPaths);
                    }
                }
                try {
                }
                catch {
                    throw new lib.OutgoingResponseParameterInjectionError();
                }
                if ("contentType" in outgoingResponseModel) {
                    lib.addParameter(responseHeaders, "content-type", outgoingResponseModel.contentType);
                    switch (outgoingResponseModel.contentType) {
                        case "application/json":
                            outgoingResponse = {
                                headers: responseHeaders,
                                status: outgoingResponseModel.status,
                                stream: internal.serializeEcho200ResponseApplicationJson(outgoingResponseModel, this.validateOutgoingEntities)
                            };
                            break;
                        case "application/x-www-form-urlencoded":
                            outgoingResponse = {
                                headers: responseHeaders,
                                status: outgoingResponseModel.status,
                                stream: internal.serializeEcho200ResponseApplicationXWwwFormUrlencoded(outgoingResponseModel, this.validateOutgoingEntities)
                            };
                            break;
                        case "text/plain":
                            outgoingResponse = {
                                headers: responseHeaders,
                                status: outgoingResponseModel.status,
                                stream: internal.serializeEcho200ResponseTextPlain(outgoingResponseModel, this.validateOutgoingEntities)
                            };
                            break;
                        case "application/octet-stream":
                            outgoingResponse = {
                                headers: responseHeaders,
                                status: outgoingResponseModel.status,
                                stream: internal.serializeEcho200ResponseApplicationOctetStream(outgoingResponseModel, this.validateOutgoingEntities)
                            };
                            break;
                        default: throw new TypeError("invalid content-type");
                    }
                }
                else {
                    lib.addParameter(responseHeaders, "content-type", "application/json");
                    outgoingResponse = {
                        headers: responseHeaders,
                        status: outgoingResponseModel.status,
                        stream: internal.serializeEcho200ResponseApplicationJson(outgoingResponseModel, this.validateOutgoingEntities)
                    };
                }
                break;
            }
            default: throw new TypeError("status code not supported");
        }
        return outgoingResponse;
    }
    protected async basicAuth(route: Route, incomingRequest: lib.ServerIncomingRequest) {
        const handler = this.basicAuthOperationHandler;
        if (handler == null) {
            throw new lib.OperationNotRegisteredError("basic-auth");
        }
        const routeParameters = { ...route.parameters };
        const requestQuery = lib.parseParameters(incomingRequest.query);
        const requestHeaders = { ...incomingRequest.headers };
        const credentials = {
            basic: internal.extractBasicCredential(requestHeaders)
        };
        const authorization = await this.authorizeBasicAuth(credentials);
        if (authorization == null) {
            throw new lib.AuthorizationFailedError("basic-auth");
        }
        let requestParameters;
        try {
            requestParameters = {} as shared.BasicAuthRequestParameters;
        }
        catch {
            throw new lib.IncomingRequestParameterExtractionError();
        }
        if (this.validateIncomingParameters) {
            const invalidPaths = [...shared.validateBasicAuthRequestParameters(requestParameters)];
            if (invalidPaths.length > 0) {
                throw new lib.IncomingRequestParametersValidationError(invalidPaths);
            }
        }
        const requestAcceptTypes = lib.parseAcceptHeader(lib.getParameterValues(incomingRequest.headers, "accept"), shared.basicAuthAcceptTypes);
        const incomingRequestModel = {
            parameters: requestParameters,
            contentType: null
        };
        const outgoingResponseModel = await handler(incomingRequestModel, authorization, requestAcceptTypes);
        const responseHeaders: lib.Parameters = {};
        let outgoingResponse: lib.ServerOutgoingResponse;
        const responseStatus = outgoingResponseModel.status;
        switch (responseStatus) {
            case 204:
                outgoingResponse = {
                    headers: responseHeaders,
                    status: outgoingResponseModel.status
                };
                if (this.validateOutgoingParameters) {
                    const invalidPaths = [...shared.validateBasicAuth204ResponseResponseParameters(outgoingResponseModel.parameters)];
                    if (invalidPaths.length > 0) {
                        throw new lib.OutgoingResponseParametersValidationError(invalidPaths);
                    }
                }
                try {
                }
                catch {
                    throw new lib.OutgoingResponseParameterInjectionError();
                }
                break;
            default: throw new TypeError("status code not supported");
        }
        return outgoingResponse;
    }
    protected async simpleStringParameter(route: Route, incomingRequest: lib.ServerIncomingRequest) {
        const handler = this.simpleStringParameterOperationHandler;
        if (handler == null) {
            throw new lib.OperationNotRegisteredError("simple-string-parameter");
        }
        const routeParameters = { ...route.parameters };
        const requestQuery = lib.parseParameters(incomingRequest.query);
        const requestHeaders = { ...incomingRequest.headers };
        const credentials = {};
        const authorization = await this.authorizeSimpleStringParameter(credentials);
        if (authorization == null) {
            throw new lib.AuthorizationFailedError("simple-string-parameter");
        }
        let requestParameters;
        try {
            requestParameters = {
                vAlue: internal.extractSimpleStringParameterVAlueRequestParameter(requestHeaders)
            } as shared.SimpleStringParameterRequestParameters;
        }
        catch {
            throw new lib.IncomingRequestParameterExtractionError();
        }
        if (this.validateIncomingParameters) {
            const invalidPaths = [...shared.validateSimpleStringParameterRequestParameters(requestParameters)];
            if (invalidPaths.length > 0) {
                throw new lib.IncomingRequestParametersValidationError(invalidPaths);
            }
        }
        const requestAcceptTypes = lib.parseAcceptHeader(lib.getParameterValues(incomingRequest.headers, "accept"), shared.simpleStringParameterAcceptTypes);
        const incomingRequestModel = {
            parameters: requestParameters,
            contentType: null
        };
        const outgoingResponseModel = await handler(incomingRequestModel, authorization, requestAcceptTypes);
        const responseHeaders: lib.Parameters = {};
        let outgoingResponse: lib.ServerOutgoingResponse;
        const responseStatus = outgoingResponseModel.status;
        switch (responseStatus) {
            case 204:
                outgoingResponse = {
                    headers: responseHeaders,
                    status: outgoingResponseModel.status
                };
                if (this.validateOutgoingParameters) {
                    const invalidPaths = [...shared.validateSimpleStringParameter204ResponseResponseParameters(outgoingResponseModel.parameters)];
                    if (invalidPaths.length > 0) {
                        throw new lib.OutgoingResponseParametersValidationError(invalidPaths);
                    }
                }
                try {
                }
                catch {
                    throw new lib.OutgoingResponseParameterInjectionError();
                }
                break;
            default: throw new TypeError("status code not supported");
        }
        return outgoingResponse;
    }
    protected async simpleNumberParameter(route: Route, incomingRequest: lib.ServerIncomingRequest) {
        const handler = this.simpleNumberParameterOperationHandler;
        if (handler == null) {
            throw new lib.OperationNotRegisteredError("simple-number-parameter");
        }
        const routeParameters = { ...route.parameters };
        const requestQuery = lib.parseParameters(incomingRequest.query);
        const requestHeaders = { ...incomingRequest.headers };
        const credentials = {};
        const authorization = await this.authorizeSimpleNumberParameter(credentials);
        if (authorization == null) {
            throw new lib.AuthorizationFailedError("simple-number-parameter");
        }
        let requestParameters;
        try {
            requestParameters = {
                vAlue: internal.extractSimpleNumberParameterVAlueRequestParameter(requestHeaders)
            } as shared.SimpleNumberParameterRequestParameters;
        }
        catch {
            throw new lib.IncomingRequestParameterExtractionError();
        }
        if (this.validateIncomingParameters) {
            const invalidPaths = [...shared.validateSimpleNumberParameterRequestParameters(requestParameters)];
            if (invalidPaths.length > 0) {
                throw new lib.IncomingRequestParametersValidationError(invalidPaths);
            }
        }
        const requestAcceptTypes = lib.parseAcceptHeader(lib.getParameterValues(incomingRequest.headers, "accept"), shared.simpleNumberParameterAcceptTypes);
        const incomingRequestModel = {
            parameters: requestParameters,
            contentType: null
        };
        const outgoingResponseModel = await handler(incomingRequestModel, authorization, requestAcceptTypes);
        const responseHeaders: lib.Parameters = {};
        let outgoingResponse: lib.ServerOutgoingResponse;
        const responseStatus = outgoingResponseModel.status;
        switch (responseStatus) {
            case 204:
                outgoingResponse = {
                    headers: responseHeaders,
                    status: outgoingResponseModel.status
                };
                if (this.validateOutgoingParameters) {
                    const invalidPaths = [...shared.validateSimpleNumberParameter204ResponseResponseParameters(outgoingResponseModel.parameters)];
                    if (invalidPaths.length > 0) {
                        throw new lib.OutgoingResponseParametersValidationError(invalidPaths);
                    }
                }
                try {
                }
                catch {
                    throw new lib.OutgoingResponseParameterInjectionError();
                }
                break;
            default: throw new TypeError("status code not supported");
        }
        return outgoingResponse;
    }
    protected async simpleArrayParameter(route: Route, incomingRequest: lib.ServerIncomingRequest) {
        const handler = this.simpleArrayParameterOperationHandler;
        if (handler == null) {
            throw new lib.OperationNotRegisteredError("simple-array-parameter");
        }
        const routeParameters = { ...route.parameters };
        const requestQuery = lib.parseParameters(incomingRequest.query);
        const requestHeaders = { ...incomingRequest.headers };
        const credentials = {};
        const authorization = await this.authorizeSimpleArrayParameter(credentials);
        if (authorization == null) {
            throw new lib.AuthorizationFailedError("simple-array-parameter");
        }
        let requestParameters;
        try {
            requestParameters = {
                vAlue: internal.extractSimpleArrayParameterVAlueRequestParameter(requestHeaders)
            } as shared.SimpleArrayParameterRequestParameters;
        }
        catch {
            throw new lib.IncomingRequestParameterExtractionError();
        }
        if (this.validateIncomingParameters) {
            const invalidPaths = [...shared.validateSimpleArrayParameterRequestParameters(requestParameters)];
            if (invalidPaths.length > 0) {
                throw new lib.IncomingRequestParametersValidationError(invalidPaths);
            }
        }
        const requestAcceptTypes = lib.parseAcceptHeader(lib.getParameterValues(incomingRequest.headers, "accept"), shared.simpleArrayParameterAcceptTypes);
        const incomingRequestModel = {
            parameters: requestParameters,
            contentType: null
        };
        const outgoingResponseModel = await handler(incomingRequestModel, authorization, requestAcceptTypes);
        const responseHeaders: lib.Parameters = {};
        let outgoingResponse: lib.ServerOutgoingResponse;
        const responseStatus = outgoingResponseModel.status;
        switch (responseStatus) {
            case 204:
                outgoingResponse = {
                    headers: responseHeaders,
                    status: outgoingResponseModel.status
                };
                if (this.validateOutgoingParameters) {
                    const invalidPaths = [...shared.validateSimpleArrayParameter204ResponseResponseParameters(outgoingResponseModel.parameters)];
                    if (invalidPaths.length > 0) {
                        throw new lib.OutgoingResponseParametersValidationError(invalidPaths);
                    }
                }
                try {
                }
                catch {
                    throw new lib.OutgoingResponseParameterInjectionError();
                }
                break;
            default: throw new TypeError("status code not supported");
        }
        return outgoingResponse;
    }
    protected async simpleExplodeArrayParameter(route: Route, incomingRequest: lib.ServerIncomingRequest) {
        const handler = this.simpleExplodeArrayParameterOperationHandler;
        if (handler == null) {
            throw new lib.OperationNotRegisteredError("simple-explode-array-parameter");
        }
        const routeParameters = { ...route.parameters };
        const requestQuery = lib.parseParameters(incomingRequest.query);
        const requestHeaders = { ...incomingRequest.headers };
        const credentials = {};
        const authorization = await this.authorizeSimpleExplodeArrayParameter(credentials);
        if (authorization == null) {
            throw new lib.AuthorizationFailedError("simple-explode-array-parameter");
        }
        let requestParameters;
        try {
            requestParameters = {
                vAlue: internal.extractSimpleExplodeArrayParameterVAlueRequestParameter(requestHeaders)
            } as shared.SimpleExplodeArrayParameterRequestParameters;
        }
        catch {
            throw new lib.IncomingRequestParameterExtractionError();
        }
        if (this.validateIncomingParameters) {
            const invalidPaths = [...shared.validateSimpleExplodeArrayParameterRequestParameters(requestParameters)];
            if (invalidPaths.length > 0) {
                throw new lib.IncomingRequestParametersValidationError(invalidPaths);
            }
        }
        const requestAcceptTypes = lib.parseAcceptHeader(lib.getParameterValues(incomingRequest.headers, "accept"), shared.simpleExplodeArrayParameterAcceptTypes);
        const incomingRequestModel = {
            parameters: requestParameters,
            contentType: null
        };
        const outgoingResponseModel = await handler(incomingRequestModel, authorization, requestAcceptTypes);
        const responseHeaders: lib.Parameters = {};
        let outgoingResponse: lib.ServerOutgoingResponse;
        const responseStatus = outgoingResponseModel.status;
        switch (responseStatus) {
            case 204:
                outgoingResponse = {
                    headers: responseHeaders,
                    status: outgoingResponseModel.status
                };
                if (this.validateOutgoingParameters) {
                    const invalidPaths = [...shared.validateSimpleExplodeArrayParameter204ResponseResponseParameters(outgoingResponseModel.parameters)];
                    if (invalidPaths.length > 0) {
                        throw new lib.OutgoingResponseParametersValidationError(invalidPaths);
                    }
                }
                try {
                }
                catch {
                    throw new lib.OutgoingResponseParameterInjectionError();
                }
                break;
            default: throw new TypeError("status code not supported");
        }
        return outgoingResponse;
    }
    protected async simpleObjectParameter(route: Route, incomingRequest: lib.ServerIncomingRequest) {
        const handler = this.simpleObjectParameterOperationHandler;
        if (handler == null) {
            throw new lib.OperationNotRegisteredError("simple-object-parameter");
        }
        const routeParameters = { ...route.parameters };
        const requestQuery = lib.parseParameters(incomingRequest.query);
        const requestHeaders = { ...incomingRequest.headers };
        const credentials = {};
        const authorization = await this.authorizeSimpleObjectParameter(credentials);
        if (authorization == null) {
            throw new lib.AuthorizationFailedError("simple-object-parameter");
        }
        let requestParameters;
        try {
            requestParameters = {
                vAlue: internal.extractSimpleObjectParameterVAlueRequestParameter(requestHeaders)
            } as shared.SimpleObjectParameterRequestParameters;
        }
        catch {
            throw new lib.IncomingRequestParameterExtractionError();
        }
        if (this.validateIncomingParameters) {
            const invalidPaths = [...shared.validateSimpleObjectParameterRequestParameters(requestParameters)];
            if (invalidPaths.length > 0) {
                throw new lib.IncomingRequestParametersValidationError(invalidPaths);
            }
        }
        const requestAcceptTypes = lib.parseAcceptHeader(lib.getParameterValues(incomingRequest.headers, "accept"), shared.simpleObjectParameterAcceptTypes);
        const incomingRequestModel = {
            parameters: requestParameters,
            contentType: null
        };
        const outgoingResponseModel = await handler(incomingRequestModel, authorization, requestAcceptTypes);
        const responseHeaders: lib.Parameters = {};
        let outgoingResponse: lib.ServerOutgoingResponse;
        const responseStatus = outgoingResponseModel.status;
        switch (responseStatus) {
            case 204:
                outgoingResponse = {
                    headers: responseHeaders,
                    status: outgoingResponseModel.status
                };
                if (this.validateOutgoingParameters) {
                    const invalidPaths = [...shared.validateSimpleObjectParameter204ResponseResponseParameters(outgoingResponseModel.parameters)];
                    if (invalidPaths.length > 0) {
                        throw new lib.OutgoingResponseParametersValidationError(invalidPaths);
                    }
                }
                try {
                }
                catch {
                    throw new lib.OutgoingResponseParameterInjectionError();
                }
                break;
            default: throw new TypeError("status code not supported");
        }
        return outgoingResponse;
    }
    protected async simpleExplodeObjectParameter(route: Route, incomingRequest: lib.ServerIncomingRequest) {
        const handler = this.simpleExplodeObjectParameterOperationHandler;
        if (handler == null) {
            throw new lib.OperationNotRegisteredError("simple-explode-object-parameter");
        }
        const routeParameters = { ...route.parameters };
        const requestQuery = lib.parseParameters(incomingRequest.query);
        const requestHeaders = { ...incomingRequest.headers };
        const credentials = {};
        const authorization = await this.authorizeSimpleExplodeObjectParameter(credentials);
        if (authorization == null) {
            throw new lib.AuthorizationFailedError("simple-explode-object-parameter");
        }
        let requestParameters;
        try {
            requestParameters = {
                vAlue: internal.extractSimpleExplodeObjectParameterVAlueRequestParameter(requestHeaders)
            } as shared.SimpleExplodeObjectParameterRequestParameters;
        }
        catch {
            throw new lib.IncomingRequestParameterExtractionError();
        }
        if (this.validateIncomingParameters) {
            const invalidPaths = [...shared.validateSimpleExplodeObjectParameterRequestParameters(requestParameters)];
            if (invalidPaths.length > 0) {
                throw new lib.IncomingRequestParametersValidationError(invalidPaths);
            }
        }
        const requestAcceptTypes = lib.parseAcceptHeader(lib.getParameterValues(incomingRequest.headers, "accept"), shared.simpleExplodeObjectParameterAcceptTypes);
        const incomingRequestModel = {
            parameters: requestParameters,
            contentType: null
        };
        const outgoingResponseModel = await handler(incomingRequestModel, authorization, requestAcceptTypes);
        const responseHeaders: lib.Parameters = {};
        let outgoingResponse: lib.ServerOutgoingResponse;
        const responseStatus = outgoingResponseModel.status;
        switch (responseStatus) {
            case 204:
                outgoingResponse = {
                    headers: responseHeaders,
                    status: outgoingResponseModel.status
                };
                if (this.validateOutgoingParameters) {
                    const invalidPaths = [...shared.validateSimpleExplodeObjectParameter204ResponseResponseParameters(outgoingResponseModel.parameters)];
                    if (invalidPaths.length > 0) {
                        throw new lib.OutgoingResponseParametersValidationError(invalidPaths);
                    }
                }
                try {
                }
                catch {
                    throw new lib.OutgoingResponseParameterInjectionError();
                }
                break;
            default: throw new TypeError("status code not supported");
        }
        return outgoingResponse;
    }
    protected async simpleDictionaryParameter(route: Route, incomingRequest: lib.ServerIncomingRequest) {
        const handler = this.simpleDictionaryParameterOperationHandler;
        if (handler == null) {
            throw new lib.OperationNotRegisteredError("simple-dictionary-parameter");
        }
        const routeParameters = { ...route.parameters };
        const requestQuery = lib.parseParameters(incomingRequest.query);
        const requestHeaders = { ...incomingRequest.headers };
        const credentials = {};
        const authorization = await this.authorizeSimpleDictionaryParameter(credentials);
        if (authorization == null) {
            throw new lib.AuthorizationFailedError("simple-dictionary-parameter");
        }
        let requestParameters;
        try {
            requestParameters = {
                vAlue: internal.extractSimpleDictionaryParameterVAlueRequestParameter(requestHeaders)
            } as shared.SimpleDictionaryParameterRequestParameters;
        }
        catch {
            throw new lib.IncomingRequestParameterExtractionError();
        }
        if (this.validateIncomingParameters) {
            const invalidPaths = [...shared.validateSimpleDictionaryParameterRequestParameters(requestParameters)];
            if (invalidPaths.length > 0) {
                throw new lib.IncomingRequestParametersValidationError(invalidPaths);
            }
        }
        const requestAcceptTypes = lib.parseAcceptHeader(lib.getParameterValues(incomingRequest.headers, "accept"), shared.simpleDictionaryParameterAcceptTypes);
        const incomingRequestModel = {
            parameters: requestParameters,
            contentType: null
        };
        const outgoingResponseModel = await handler(incomingRequestModel, authorization, requestAcceptTypes);
        const responseHeaders: lib.Parameters = {};
        let outgoingResponse: lib.ServerOutgoingResponse;
        const responseStatus = outgoingResponseModel.status;
        switch (responseStatus) {
            case 204:
                outgoingResponse = {
                    headers: responseHeaders,
                    status: outgoingResponseModel.status
                };
                if (this.validateOutgoingParameters) {
                    const invalidPaths = [...shared.validateSimpleDictionaryParameter204ResponseResponseParameters(outgoingResponseModel.parameters)];
                    if (invalidPaths.length > 0) {
                        throw new lib.OutgoingResponseParametersValidationError(invalidPaths);
                    }
                }
                try {
                }
                catch {
                    throw new lib.OutgoingResponseParameterInjectionError();
                }
                break;
            default: throw new TypeError("status code not supported");
        }
        return outgoingResponse;
    }
    protected async simpleExplodeDictionaryParameter(route: Route, incomingRequest: lib.ServerIncomingRequest) {
        const handler = this.simpleExplodeDictionaryParameterOperationHandler;
        if (handler == null) {
            throw new lib.OperationNotRegisteredError("simple-explode-dictionary-parameter");
        }
        const routeParameters = { ...route.parameters };
        const requestQuery = lib.parseParameters(incomingRequest.query);
        const requestHeaders = { ...incomingRequest.headers };
        const credentials = {};
        const authorization = await this.authorizeSimpleExplodeDictionaryParameter(credentials);
        if (authorization == null) {
            throw new lib.AuthorizationFailedError("simple-explode-dictionary-parameter");
        }
        let requestParameters;
        try {
            requestParameters = {
                vAlue: internal.extractSimpleExplodeDictionaryParameterVAlueRequestParameter(requestHeaders)
            } as shared.SimpleExplodeDictionaryParameterRequestParameters;
        }
        catch {
            throw new lib.IncomingRequestParameterExtractionError();
        }
        if (this.validateIncomingParameters) {
            const invalidPaths = [...shared.validateSimpleExplodeDictionaryParameterRequestParameters(requestParameters)];
            if (invalidPaths.length > 0) {
                throw new lib.IncomingRequestParametersValidationError(invalidPaths);
            }
        }
        const requestAcceptTypes = lib.parseAcceptHeader(lib.getParameterValues(incomingRequest.headers, "accept"), shared.simpleExplodeDictionaryParameterAcceptTypes);
        const incomingRequestModel = {
            parameters: requestParameters,
            contentType: null
        };
        const outgoingResponseModel = await handler(incomingRequestModel, authorization, requestAcceptTypes);
        const responseHeaders: lib.Parameters = {};
        let outgoingResponse: lib.ServerOutgoingResponse;
        const responseStatus = outgoingResponseModel.status;
        switch (responseStatus) {
            case 204:
                outgoingResponse = {
                    headers: responseHeaders,
                    status: outgoingResponseModel.status
                };
                if (this.validateOutgoingParameters) {
                    const invalidPaths = [...shared.validateSimpleExplodeDictionaryParameter204ResponseResponseParameters(outgoingResponseModel.parameters)];
                    if (invalidPaths.length > 0) {
                        throw new lib.OutgoingResponseParametersValidationError(invalidPaths);
                    }
                }
                try {
                }
                catch {
                    throw new lib.OutgoingResponseParameterInjectionError();
                }
                break;
            default: throw new TypeError("status code not supported");
        }
        return outgoingResponse;
    }
    protected async labelStringParameter(route: Route, incomingRequest: lib.ServerIncomingRequest) {
        const handler = this.labelStringParameterOperationHandler;
        if (handler == null) {
            throw new lib.OperationNotRegisteredError("label-string-parameter");
        }
        const routeParameters = { ...route.parameters };
        const requestQuery = lib.parseParameters(incomingRequest.query);
        const requestHeaders = { ...incomingRequest.headers };
        const credentials = {};
        const authorization = await this.authorizeLabelStringParameter(credentials);
        if (authorization == null) {
            throw new lib.AuthorizationFailedError("label-string-parameter");
        }
        let requestParameters;
        try {
            requestParameters = {
                vAlue: internal.extractLabelStringParameterVAlueRequestParameter(routeParameters)
            } as shared.LabelStringParameterRequestParameters;
        }
        catch {
            throw new lib.IncomingRequestParameterExtractionError();
        }
        if (this.validateIncomingParameters) {
            const invalidPaths = [...shared.validateLabelStringParameterRequestParameters(requestParameters)];
            if (invalidPaths.length > 0) {
                throw new lib.IncomingRequestParametersValidationError(invalidPaths);
            }
        }
        const requestAcceptTypes = lib.parseAcceptHeader(lib.getParameterValues(incomingRequest.headers, "accept"), shared.labelStringParameterAcceptTypes);
        const incomingRequestModel = {
            parameters: requestParameters,
            contentType: null
        };
        const outgoingResponseModel = await handler(incomingRequestModel, authorization, requestAcceptTypes);
        const responseHeaders: lib.Parameters = {};
        let outgoingResponse: lib.ServerOutgoingResponse;
        const responseStatus = outgoingResponseModel.status;
        switch (responseStatus) {
            case 204:
                outgoingResponse = {
                    headers: responseHeaders,
                    status: outgoingResponseModel.status
                };
                if (this.validateOutgoingParameters) {
                    const invalidPaths = [...shared.validateLabelStringParameter204ResponseResponseParameters(outgoingResponseModel.parameters)];
                    if (invalidPaths.length > 0) {
                        throw new lib.OutgoingResponseParametersValidationError(invalidPaths);
                    }
                }
                try {
                }
                catch {
                    throw new lib.OutgoingResponseParameterInjectionError();
                }
                break;
            default: throw new TypeError("status code not supported");
        }
        return outgoingResponse;
    }
    protected async labelNumberParameter(route: Route, incomingRequest: lib.ServerIncomingRequest) {
        const handler = this.labelNumberParameterOperationHandler;
        if (handler == null) {
            throw new lib.OperationNotRegisteredError("label-number-parameter");
        }
        const routeParameters = { ...route.parameters };
        const requestQuery = lib.parseParameters(incomingRequest.query);
        const requestHeaders = { ...incomingRequest.headers };
        const credentials = {};
        const authorization = await this.authorizeLabelNumberParameter(credentials);
        if (authorization == null) {
            throw new lib.AuthorizationFailedError("label-number-parameter");
        }
        let requestParameters;
        try {
            requestParameters = {
                vAlue: internal.extractLabelNumberParameterVAlueRequestParameter(routeParameters)
            } as shared.LabelNumberParameterRequestParameters;
        }
        catch {
            throw new lib.IncomingRequestParameterExtractionError();
        }
        if (this.validateIncomingParameters) {
            const invalidPaths = [...shared.validateLabelNumberParameterRequestParameters(requestParameters)];
            if (invalidPaths.length > 0) {
                throw new lib.IncomingRequestParametersValidationError(invalidPaths);
            }
        }
        const requestAcceptTypes = lib.parseAcceptHeader(lib.getParameterValues(incomingRequest.headers, "accept"), shared.labelNumberParameterAcceptTypes);
        const incomingRequestModel = {
            parameters: requestParameters,
            contentType: null
        };
        const outgoingResponseModel = await handler(incomingRequestModel, authorization, requestAcceptTypes);
        const responseHeaders: lib.Parameters = {};
        let outgoingResponse: lib.ServerOutgoingResponse;
        const responseStatus = outgoingResponseModel.status;
        switch (responseStatus) {
            case 204:
                outgoingResponse = {
                    headers: responseHeaders,
                    status: outgoingResponseModel.status
                };
                if (this.validateOutgoingParameters) {
                    const invalidPaths = [...shared.validateLabelNumberParameter204ResponseResponseParameters(outgoingResponseModel.parameters)];
                    if (invalidPaths.length > 0) {
                        throw new lib.OutgoingResponseParametersValidationError(invalidPaths);
                    }
                }
                try {
                }
                catch {
                    throw new lib.OutgoingResponseParameterInjectionError();
                }
                break;
            default: throw new TypeError("status code not supported");
        }
        return outgoingResponse;
    }
    protected async labelArrayParameter(route: Route, incomingRequest: lib.ServerIncomingRequest) {
        const handler = this.labelArrayParameterOperationHandler;
        if (handler == null) {
            throw new lib.OperationNotRegisteredError("label-array-parameter");
        }
        const routeParameters = { ...route.parameters };
        const requestQuery = lib.parseParameters(incomingRequest.query);
        const requestHeaders = { ...incomingRequest.headers };
        const credentials = {};
        const authorization = await this.authorizeLabelArrayParameter(credentials);
        if (authorization == null) {
            throw new lib.AuthorizationFailedError("label-array-parameter");
        }
        let requestParameters;
        try {
            requestParameters = {
                vAlue: internal.extractLabelArrayParameterVAlueRequestParameter(routeParameters)
            } as shared.LabelArrayParameterRequestParameters;
        }
        catch {
            throw new lib.IncomingRequestParameterExtractionError();
        }
        if (this.validateIncomingParameters) {
            const invalidPaths = [...shared.validateLabelArrayParameterRequestParameters(requestParameters)];
            if (invalidPaths.length > 0) {
                throw new lib.IncomingRequestParametersValidationError(invalidPaths);
            }
        }
        const requestAcceptTypes = lib.parseAcceptHeader(lib.getParameterValues(incomingRequest.headers, "accept"), shared.labelArrayParameterAcceptTypes);
        const incomingRequestModel = {
            parameters: requestParameters,
            contentType: null
        };
        const outgoingResponseModel = await handler(incomingRequestModel, authorization, requestAcceptTypes);
        const responseHeaders: lib.Parameters = {};
        let outgoingResponse: lib.ServerOutgoingResponse;
        const responseStatus = outgoingResponseModel.status;
        switch (responseStatus) {
            case 204:
                outgoingResponse = {
                    headers: responseHeaders,
                    status: outgoingResponseModel.status
                };
                if (this.validateOutgoingParameters) {
                    const invalidPaths = [...shared.validateLabelArrayParameter204ResponseResponseParameters(outgoingResponseModel.parameters)];
                    if (invalidPaths.length > 0) {
                        throw new lib.OutgoingResponseParametersValidationError(invalidPaths);
                    }
                }
                try {
                }
                catch {
                    throw new lib.OutgoingResponseParameterInjectionError();
                }
                break;
            default: throw new TypeError("status code not supported");
        }
        return outgoingResponse;
    }
    protected async labelExplodeArrayParameter(route: Route, incomingRequest: lib.ServerIncomingRequest) {
        const handler = this.labelExplodeArrayParameterOperationHandler;
        if (handler == null) {
            throw new lib.OperationNotRegisteredError("label-explode-array-parameter");
        }
        const routeParameters = { ...route.parameters };
        const requestQuery = lib.parseParameters(incomingRequest.query);
        const requestHeaders = { ...incomingRequest.headers };
        const credentials = {};
        const authorization = await this.authorizeLabelExplodeArrayParameter(credentials);
        if (authorization == null) {
            throw new lib.AuthorizationFailedError("label-explode-array-parameter");
        }
        let requestParameters;
        try {
            requestParameters = {
                vAlue: internal.extractLabelExplodeArrayParameterVAlueRequestParameter(routeParameters)
            } as shared.LabelExplodeArrayParameterRequestParameters;
        }
        catch {
            throw new lib.IncomingRequestParameterExtractionError();
        }
        if (this.validateIncomingParameters) {
            const invalidPaths = [...shared.validateLabelExplodeArrayParameterRequestParameters(requestParameters)];
            if (invalidPaths.length > 0) {
                throw new lib.IncomingRequestParametersValidationError(invalidPaths);
            }
        }
        const requestAcceptTypes = lib.parseAcceptHeader(lib.getParameterValues(incomingRequest.headers, "accept"), shared.labelExplodeArrayParameterAcceptTypes);
        const incomingRequestModel = {
            parameters: requestParameters,
            contentType: null
        };
        const outgoingResponseModel = await handler(incomingRequestModel, authorization, requestAcceptTypes);
        const responseHeaders: lib.Parameters = {};
        let outgoingResponse: lib.ServerOutgoingResponse;
        const responseStatus = outgoingResponseModel.status;
        switch (responseStatus) {
            case 204:
                outgoingResponse = {
                    headers: responseHeaders,
                    status: outgoingResponseModel.status
                };
                if (this.validateOutgoingParameters) {
                    const invalidPaths = [...shared.validateLabelExplodeArrayParameter204ResponseResponseParameters(outgoingResponseModel.parameters)];
                    if (invalidPaths.length > 0) {
                        throw new lib.OutgoingResponseParametersValidationError(invalidPaths);
                    }
                }
                try {
                }
                catch {
                    throw new lib.OutgoingResponseParameterInjectionError();
                }
                break;
            default: throw new TypeError("status code not supported");
        }
        return outgoingResponse;
    }
    protected async labelObjectParameter(route: Route, incomingRequest: lib.ServerIncomingRequest) {
        const handler = this.labelObjectParameterOperationHandler;
        if (handler == null) {
            throw new lib.OperationNotRegisteredError("label-object-parameter");
        }
        const routeParameters = { ...route.parameters };
        const requestQuery = lib.parseParameters(incomingRequest.query);
        const requestHeaders = { ...incomingRequest.headers };
        const credentials = {};
        const authorization = await this.authorizeLabelObjectParameter(credentials);
        if (authorization == null) {
            throw new lib.AuthorizationFailedError("label-object-parameter");
        }
        let requestParameters;
        try {
            requestParameters = {
                vAlue: internal.extractLabelObjectParameterVAlueRequestParameter(routeParameters)
            } as shared.LabelObjectParameterRequestParameters;
        }
        catch {
            throw new lib.IncomingRequestParameterExtractionError();
        }
        if (this.validateIncomingParameters) {
            const invalidPaths = [...shared.validateLabelObjectParameterRequestParameters(requestParameters)];
            if (invalidPaths.length > 0) {
                throw new lib.IncomingRequestParametersValidationError(invalidPaths);
            }
        }
        const requestAcceptTypes = lib.parseAcceptHeader(lib.getParameterValues(incomingRequest.headers, "accept"), shared.labelObjectParameterAcceptTypes);
        const incomingRequestModel = {
            parameters: requestParameters,
            contentType: null
        };
        const outgoingResponseModel = await handler(incomingRequestModel, authorization, requestAcceptTypes);
        const responseHeaders: lib.Parameters = {};
        let outgoingResponse: lib.ServerOutgoingResponse;
        const responseStatus = outgoingResponseModel.status;
        switch (responseStatus) {
            case 204:
                outgoingResponse = {
                    headers: responseHeaders,
                    status: outgoingResponseModel.status
                };
                if (this.validateOutgoingParameters) {
                    const invalidPaths = [...shared.validateLabelObjectParameter204ResponseResponseParameters(outgoingResponseModel.parameters)];
                    if (invalidPaths.length > 0) {
                        throw new lib.OutgoingResponseParametersValidationError(invalidPaths);
                    }
                }
                try {
                }
                catch {
                    throw new lib.OutgoingResponseParameterInjectionError();
                }
                break;
            default: throw new TypeError("status code not supported");
        }
        return outgoingResponse;
    }
    protected async labelExplodeObjectParameter(route: Route, incomingRequest: lib.ServerIncomingRequest) {
        const handler = this.labelExplodeObjectParameterOperationHandler;
        if (handler == null) {
            throw new lib.OperationNotRegisteredError("label-explode-object-parameter");
        }
        const routeParameters = { ...route.parameters };
        const requestQuery = lib.parseParameters(incomingRequest.query);
        const requestHeaders = { ...incomingRequest.headers };
        const credentials = {};
        const authorization = await this.authorizeLabelExplodeObjectParameter(credentials);
        if (authorization == null) {
            throw new lib.AuthorizationFailedError("label-explode-object-parameter");
        }
        let requestParameters;
        try {
            requestParameters = {
                vAlue: internal.extractLabelExplodeObjectParameterVAlueRequestParameter(routeParameters)
            } as shared.LabelExplodeObjectParameterRequestParameters;
        }
        catch {
            throw new lib.IncomingRequestParameterExtractionError();
        }
        if (this.validateIncomingParameters) {
            const invalidPaths = [...shared.validateLabelExplodeObjectParameterRequestParameters(requestParameters)];
            if (invalidPaths.length > 0) {
                throw new lib.IncomingRequestParametersValidationError(invalidPaths);
            }
        }
        const requestAcceptTypes = lib.parseAcceptHeader(lib.getParameterValues(incomingRequest.headers, "accept"), shared.labelExplodeObjectParameterAcceptTypes);
        const incomingRequestModel = {
            parameters: requestParameters,
            contentType: null
        };
        const outgoingResponseModel = await handler(incomingRequestModel, authorization, requestAcceptTypes);
        const responseHeaders: lib.Parameters = {};
        let outgoingResponse: lib.ServerOutgoingResponse;
        const responseStatus = outgoingResponseModel.status;
        switch (responseStatus) {
            case 204:
                outgoingResponse = {
                    headers: responseHeaders,
                    status: outgoingResponseModel.status
                };
                if (this.validateOutgoingParameters) {
                    const invalidPaths = [...shared.validateLabelExplodeObjectParameter204ResponseResponseParameters(outgoingResponseModel.parameters)];
                    if (invalidPaths.length > 0) {
                        throw new lib.OutgoingResponseParametersValidationError(invalidPaths);
                    }
                }
                try {
                }
                catch {
                    throw new lib.OutgoingResponseParameterInjectionError();
                }
                break;
            default: throw new TypeError("status code not supported");
        }
        return outgoingResponse;
    }
    protected async labelDictionaryParameter(route: Route, incomingRequest: lib.ServerIncomingRequest) {
        const handler = this.labelDictionaryParameterOperationHandler;
        if (handler == null) {
            throw new lib.OperationNotRegisteredError("label-dictionary-parameter");
        }
        const routeParameters = { ...route.parameters };
        const requestQuery = lib.parseParameters(incomingRequest.query);
        const requestHeaders = { ...incomingRequest.headers };
        const credentials = {};
        const authorization = await this.authorizeLabelDictionaryParameter(credentials);
        if (authorization == null) {
            throw new lib.AuthorizationFailedError("label-dictionary-parameter");
        }
        let requestParameters;
        try {
            requestParameters = {
                vAlue: internal.extractLabelDictionaryParameterVAlueRequestParameter(routeParameters)
            } as shared.LabelDictionaryParameterRequestParameters;
        }
        catch {
            throw new lib.IncomingRequestParameterExtractionError();
        }
        if (this.validateIncomingParameters) {
            const invalidPaths = [...shared.validateLabelDictionaryParameterRequestParameters(requestParameters)];
            if (invalidPaths.length > 0) {
                throw new lib.IncomingRequestParametersValidationError(invalidPaths);
            }
        }
        const requestAcceptTypes = lib.parseAcceptHeader(lib.getParameterValues(incomingRequest.headers, "accept"), shared.labelDictionaryParameterAcceptTypes);
        const incomingRequestModel = {
            parameters: requestParameters,
            contentType: null
        };
        const outgoingResponseModel = await handler(incomingRequestModel, authorization, requestAcceptTypes);
        const responseHeaders: lib.Parameters = {};
        let outgoingResponse: lib.ServerOutgoingResponse;
        const responseStatus = outgoingResponseModel.status;
        switch (responseStatus) {
            case 204:
                outgoingResponse = {
                    headers: responseHeaders,
                    status: outgoingResponseModel.status
                };
                if (this.validateOutgoingParameters) {
                    const invalidPaths = [...shared.validateLabelDictionaryParameter204ResponseResponseParameters(outgoingResponseModel.parameters)];
                    if (invalidPaths.length > 0) {
                        throw new lib.OutgoingResponseParametersValidationError(invalidPaths);
                    }
                }
                try {
                }
                catch {
                    throw new lib.OutgoingResponseParameterInjectionError();
                }
                break;
            default: throw new TypeError("status code not supported");
        }
        return outgoingResponse;
    }
    protected async labelExplodeDictionaryParameter(route: Route, incomingRequest: lib.ServerIncomingRequest) {
        const handler = this.labelExplodeDictionaryParameterOperationHandler;
        if (handler == null) {
            throw new lib.OperationNotRegisteredError("label-explode-dictionary-parameter");
        }
        const routeParameters = { ...route.parameters };
        const requestQuery = lib.parseParameters(incomingRequest.query);
        const requestHeaders = { ...incomingRequest.headers };
        const credentials = {};
        const authorization = await this.authorizeLabelExplodeDictionaryParameter(credentials);
        if (authorization == null) {
            throw new lib.AuthorizationFailedError("label-explode-dictionary-parameter");
        }
        let requestParameters;
        try {
            requestParameters = {
                vAlue: internal.extractLabelExplodeDictionaryParameterVAlueRequestParameter(routeParameters)
            } as shared.LabelExplodeDictionaryParameterRequestParameters;
        }
        catch {
            throw new lib.IncomingRequestParameterExtractionError();
        }
        if (this.validateIncomingParameters) {
            const invalidPaths = [...shared.validateLabelExplodeDictionaryParameterRequestParameters(requestParameters)];
            if (invalidPaths.length > 0) {
                throw new lib.IncomingRequestParametersValidationError(invalidPaths);
            }
        }
        const requestAcceptTypes = lib.parseAcceptHeader(lib.getParameterValues(incomingRequest.headers, "accept"), shared.labelExplodeDictionaryParameterAcceptTypes);
        const incomingRequestModel = {
            parameters: requestParameters,
            contentType: null
        };
        const outgoingResponseModel = await handler(incomingRequestModel, authorization, requestAcceptTypes);
        const responseHeaders: lib.Parameters = {};
        let outgoingResponse: lib.ServerOutgoingResponse;
        const responseStatus = outgoingResponseModel.status;
        switch (responseStatus) {
            case 204:
                outgoingResponse = {
                    headers: responseHeaders,
                    status: outgoingResponseModel.status
                };
                if (this.validateOutgoingParameters) {
                    const invalidPaths = [...shared.validateLabelExplodeDictionaryParameter204ResponseResponseParameters(outgoingResponseModel.parameters)];
                    if (invalidPaths.length > 0) {
                        throw new lib.OutgoingResponseParametersValidationError(invalidPaths);
                    }
                }
                try {
                }
                catch {
                    throw new lib.OutgoingResponseParameterInjectionError();
                }
                break;
            default: throw new TypeError("status code not supported");
        }
        return outgoingResponse;
    }
    protected async formStringParameter(route: Route, incomingRequest: lib.ServerIncomingRequest) {
        const handler = this.formStringParameterOperationHandler;
        if (handler == null) {
            throw new lib.OperationNotRegisteredError("form-string-parameter");
        }
        const routeParameters = { ...route.parameters };
        const requestQuery = lib.parseParameters(incomingRequest.query);
        const requestHeaders = { ...incomingRequest.headers };
        const credentials = {};
        const authorization = await this.authorizeFormStringParameter(credentials);
        if (authorization == null) {
            throw new lib.AuthorizationFailedError("form-string-parameter");
        }
        let requestParameters;
        try {
            requestParameters = {
                vAlue: internal.extractFormStringParameterVAlueRequestParameter(requestQuery)
            } as shared.FormStringParameterRequestParameters;
        }
        catch {
            throw new lib.IncomingRequestParameterExtractionError();
        }
        if (this.validateIncomingParameters) {
            const invalidPaths = [...shared.validateFormStringParameterRequestParameters(requestParameters)];
            if (invalidPaths.length > 0) {
                throw new lib.IncomingRequestParametersValidationError(invalidPaths);
            }
        }
        const requestAcceptTypes = lib.parseAcceptHeader(lib.getParameterValues(incomingRequest.headers, "accept"), shared.formStringParameterAcceptTypes);
        const incomingRequestModel = {
            parameters: requestParameters,
            contentType: null
        };
        const outgoingResponseModel = await handler(incomingRequestModel, authorization, requestAcceptTypes);
        const responseHeaders: lib.Parameters = {};
        let outgoingResponse: lib.ServerOutgoingResponse;
        const responseStatus = outgoingResponseModel.status;
        switch (responseStatus) {
            case 204:
                outgoingResponse = {
                    headers: responseHeaders,
                    status: outgoingResponseModel.status
                };
                if (this.validateOutgoingParameters) {
                    const invalidPaths = [...shared.validateFormStringParameter204ResponseResponseParameters(outgoingResponseModel.parameters)];
                    if (invalidPaths.length > 0) {
                        throw new lib.OutgoingResponseParametersValidationError(invalidPaths);
                    }
                }
                try {
                }
                catch {
                    throw new lib.OutgoingResponseParameterInjectionError();
                }
                break;
            default: throw new TypeError("status code not supported");
        }
        return outgoingResponse;
    }
    protected async formNumberParameter(route: Route, incomingRequest: lib.ServerIncomingRequest) {
        const handler = this.formNumberParameterOperationHandler;
        if (handler == null) {
            throw new lib.OperationNotRegisteredError("form-number-parameter");
        }
        const routeParameters = { ...route.parameters };
        const requestQuery = lib.parseParameters(incomingRequest.query);
        const requestHeaders = { ...incomingRequest.headers };
        const credentials = {};
        const authorization = await this.authorizeFormNumberParameter(credentials);
        if (authorization == null) {
            throw new lib.AuthorizationFailedError("form-number-parameter");
        }
        let requestParameters;
        try {
            requestParameters = {
                vAlue: internal.extractFormNumberParameterVAlueRequestParameter(requestQuery)
            } as shared.FormNumberParameterRequestParameters;
        }
        catch {
            throw new lib.IncomingRequestParameterExtractionError();
        }
        if (this.validateIncomingParameters) {
            const invalidPaths = [...shared.validateFormNumberParameterRequestParameters(requestParameters)];
            if (invalidPaths.length > 0) {
                throw new lib.IncomingRequestParametersValidationError(invalidPaths);
            }
        }
        const requestAcceptTypes = lib.parseAcceptHeader(lib.getParameterValues(incomingRequest.headers, "accept"), shared.formNumberParameterAcceptTypes);
        const incomingRequestModel = {
            parameters: requestParameters,
            contentType: null
        };
        const outgoingResponseModel = await handler(incomingRequestModel, authorization, requestAcceptTypes);
        const responseHeaders: lib.Parameters = {};
        let outgoingResponse: lib.ServerOutgoingResponse;
        const responseStatus = outgoingResponseModel.status;
        switch (responseStatus) {
            case 204:
                outgoingResponse = {
                    headers: responseHeaders,
                    status: outgoingResponseModel.status
                };
                if (this.validateOutgoingParameters) {
                    const invalidPaths = [...shared.validateFormNumberParameter204ResponseResponseParameters(outgoingResponseModel.parameters)];
                    if (invalidPaths.length > 0) {
                        throw new lib.OutgoingResponseParametersValidationError(invalidPaths);
                    }
                }
                try {
                }
                catch {
                    throw new lib.OutgoingResponseParameterInjectionError();
                }
                break;
            default: throw new TypeError("status code not supported");
        }
        return outgoingResponse;
    }
    protected async formArrayParameter(route: Route, incomingRequest: lib.ServerIncomingRequest) {
        const handler = this.formArrayParameterOperationHandler;
        if (handler == null) {
            throw new lib.OperationNotRegisteredError("form-array-parameter");
        }
        const routeParameters = { ...route.parameters };
        const requestQuery = lib.parseParameters(incomingRequest.query);
        const requestHeaders = { ...incomingRequest.headers };
        const credentials = {};
        const authorization = await this.authorizeFormArrayParameter(credentials);
        if (authorization == null) {
            throw new lib.AuthorizationFailedError("form-array-parameter");
        }
        let requestParameters;
        try {
            requestParameters = {
                vAlue: internal.extractFormArrayParameterVAlueRequestParameter(requestQuery)
            } as shared.FormArrayParameterRequestParameters;
        }
        catch {
            throw new lib.IncomingRequestParameterExtractionError();
        }
        if (this.validateIncomingParameters) {
            const invalidPaths = [...shared.validateFormArrayParameterRequestParameters(requestParameters)];
            if (invalidPaths.length > 0) {
                throw new lib.IncomingRequestParametersValidationError(invalidPaths);
            }
        }
        const requestAcceptTypes = lib.parseAcceptHeader(lib.getParameterValues(incomingRequest.headers, "accept"), shared.formArrayParameterAcceptTypes);
        const incomingRequestModel = {
            parameters: requestParameters,
            contentType: null
        };
        const outgoingResponseModel = await handler(incomingRequestModel, authorization, requestAcceptTypes);
        const responseHeaders: lib.Parameters = {};
        let outgoingResponse: lib.ServerOutgoingResponse;
        const responseStatus = outgoingResponseModel.status;
        switch (responseStatus) {
            case 204:
                outgoingResponse = {
                    headers: responseHeaders,
                    status: outgoingResponseModel.status
                };
                if (this.validateOutgoingParameters) {
                    const invalidPaths = [...shared.validateFormArrayParameter204ResponseResponseParameters(outgoingResponseModel.parameters)];
                    if (invalidPaths.length > 0) {
                        throw new lib.OutgoingResponseParametersValidationError(invalidPaths);
                    }
                }
                try {
                }
                catch {
                    throw new lib.OutgoingResponseParameterInjectionError();
                }
                break;
            default: throw new TypeError("status code not supported");
        }
        return outgoingResponse;
    }
    protected async formExplodeArrayParameter(route: Route, incomingRequest: lib.ServerIncomingRequest) {
        const handler = this.formExplodeArrayParameterOperationHandler;
        if (handler == null) {
            throw new lib.OperationNotRegisteredError("form-explode-array-parameter");
        }
        const routeParameters = { ...route.parameters };
        const requestQuery = lib.parseParameters(incomingRequest.query);
        const requestHeaders = { ...incomingRequest.headers };
        const credentials = {};
        const authorization = await this.authorizeFormExplodeArrayParameter(credentials);
        if (authorization == null) {
            throw new lib.AuthorizationFailedError("form-explode-array-parameter");
        }
        let requestParameters;
        try {
            requestParameters = {
                vAlue: internal.extractFormExplodeArrayParameterVAlueRequestParameter(requestQuery)
            } as shared.FormExplodeArrayParameterRequestParameters;
        }
        catch {
            throw new lib.IncomingRequestParameterExtractionError();
        }
        if (this.validateIncomingParameters) {
            const invalidPaths = [...shared.validateFormExplodeArrayParameterRequestParameters(requestParameters)];
            if (invalidPaths.length > 0) {
                throw new lib.IncomingRequestParametersValidationError(invalidPaths);
            }
        }
        const requestAcceptTypes = lib.parseAcceptHeader(lib.getParameterValues(incomingRequest.headers, "accept"), shared.formExplodeArrayParameterAcceptTypes);
        const incomingRequestModel = {
            parameters: requestParameters,
            contentType: null
        };
        const outgoingResponseModel = await handler(incomingRequestModel, authorization, requestAcceptTypes);
        const responseHeaders: lib.Parameters = {};
        let outgoingResponse: lib.ServerOutgoingResponse;
        const responseStatus = outgoingResponseModel.status;
        switch (responseStatus) {
            case 204:
                outgoingResponse = {
                    headers: responseHeaders,
                    status: outgoingResponseModel.status
                };
                if (this.validateOutgoingParameters) {
                    const invalidPaths = [...shared.validateFormExplodeArrayParameter204ResponseResponseParameters(outgoingResponseModel.parameters)];
                    if (invalidPaths.length > 0) {
                        throw new lib.OutgoingResponseParametersValidationError(invalidPaths);
                    }
                }
                try {
                }
                catch {
                    throw new lib.OutgoingResponseParameterInjectionError();
                }
                break;
            default: throw new TypeError("status code not supported");
        }
        return outgoingResponse;
    }
    protected async formObjectParameter(route: Route, incomingRequest: lib.ServerIncomingRequest) {
        const handler = this.formObjectParameterOperationHandler;
        if (handler == null) {
            throw new lib.OperationNotRegisteredError("form-object-parameter");
        }
        const routeParameters = { ...route.parameters };
        const requestQuery = lib.parseParameters(incomingRequest.query);
        const requestHeaders = { ...incomingRequest.headers };
        const credentials = {};
        const authorization = await this.authorizeFormObjectParameter(credentials);
        if (authorization == null) {
            throw new lib.AuthorizationFailedError("form-object-parameter");
        }
        let requestParameters;
        try {
            requestParameters = {
                vAlue: internal.extractFormObjectParameterVAlueRequestParameter(requestQuery)
            } as shared.FormObjectParameterRequestParameters;
        }
        catch {
            throw new lib.IncomingRequestParameterExtractionError();
        }
        if (this.validateIncomingParameters) {
            const invalidPaths = [...shared.validateFormObjectParameterRequestParameters(requestParameters)];
            if (invalidPaths.length > 0) {
                throw new lib.IncomingRequestParametersValidationError(invalidPaths);
            }
        }
        const requestAcceptTypes = lib.parseAcceptHeader(lib.getParameterValues(incomingRequest.headers, "accept"), shared.formObjectParameterAcceptTypes);
        const incomingRequestModel = {
            parameters: requestParameters,
            contentType: null
        };
        const outgoingResponseModel = await handler(incomingRequestModel, authorization, requestAcceptTypes);
        const responseHeaders: lib.Parameters = {};
        let outgoingResponse: lib.ServerOutgoingResponse;
        const responseStatus = outgoingResponseModel.status;
        switch (responseStatus) {
            case 204:
                outgoingResponse = {
                    headers: responseHeaders,
                    status: outgoingResponseModel.status
                };
                if (this.validateOutgoingParameters) {
                    const invalidPaths = [...shared.validateFormObjectParameter204ResponseResponseParameters(outgoingResponseModel.parameters)];
                    if (invalidPaths.length > 0) {
                        throw new lib.OutgoingResponseParametersValidationError(invalidPaths);
                    }
                }
                try {
                }
                catch {
                    throw new lib.OutgoingResponseParameterInjectionError();
                }
                break;
            default: throw new TypeError("status code not supported");
        }
        return outgoingResponse;
    }
    protected async formExplodeObjectParameter(route: Route, incomingRequest: lib.ServerIncomingRequest) {
        const handler = this.formExplodeObjectParameterOperationHandler;
        if (handler == null) {
            throw new lib.OperationNotRegisteredError("form-explode-object-parameter");
        }
        const routeParameters = { ...route.parameters };
        const requestQuery = lib.parseParameters(incomingRequest.query);
        const requestHeaders = { ...incomingRequest.headers };
        const credentials = {};
        const authorization = await this.authorizeFormExplodeObjectParameter(credentials);
        if (authorization == null) {
            throw new lib.AuthorizationFailedError("form-explode-object-parameter");
        }
        let requestParameters;
        try {
            requestParameters = {
                vAlue: internal.extractFormExplodeObjectParameterVAlueRequestParameter(requestQuery)
            } as shared.FormExplodeObjectParameterRequestParameters;
        }
        catch {
            throw new lib.IncomingRequestParameterExtractionError();
        }
        if (this.validateIncomingParameters) {
            const invalidPaths = [...shared.validateFormExplodeObjectParameterRequestParameters(requestParameters)];
            if (invalidPaths.length > 0) {
                throw new lib.IncomingRequestParametersValidationError(invalidPaths);
            }
        }
        const requestAcceptTypes = lib.parseAcceptHeader(lib.getParameterValues(incomingRequest.headers, "accept"), shared.formExplodeObjectParameterAcceptTypes);
        const incomingRequestModel = {
            parameters: requestParameters,
            contentType: null
        };
        const outgoingResponseModel = await handler(incomingRequestModel, authorization, requestAcceptTypes);
        const responseHeaders: lib.Parameters = {};
        let outgoingResponse: lib.ServerOutgoingResponse;
        const responseStatus = outgoingResponseModel.status;
        switch (responseStatus) {
            case 204:
                outgoingResponse = {
                    headers: responseHeaders,
                    status: outgoingResponseModel.status
                };
                if (this.validateOutgoingParameters) {
                    const invalidPaths = [...shared.validateFormExplodeObjectParameter204ResponseResponseParameters(outgoingResponseModel.parameters)];
                    if (invalidPaths.length > 0) {
                        throw new lib.OutgoingResponseParametersValidationError(invalidPaths);
                    }
                }
                try {
                }
                catch {
                    throw new lib.OutgoingResponseParameterInjectionError();
                }
                break;
            default: throw new TypeError("status code not supported");
        }
        return outgoingResponse;
    }
    protected async formDictionaryParameter(route: Route, incomingRequest: lib.ServerIncomingRequest) {
        const handler = this.formDictionaryParameterOperationHandler;
        if (handler == null) {
            throw new lib.OperationNotRegisteredError("form-dictionary-parameter");
        }
        const routeParameters = { ...route.parameters };
        const requestQuery = lib.parseParameters(incomingRequest.query);
        const requestHeaders = { ...incomingRequest.headers };
        const credentials = {};
        const authorization = await this.authorizeFormDictionaryParameter(credentials);
        if (authorization == null) {
            throw new lib.AuthorizationFailedError("form-dictionary-parameter");
        }
        let requestParameters;
        try {
            requestParameters = {
                vAlue: internal.extractFormDictionaryParameterVAlueRequestParameter(requestQuery)
            } as shared.FormDictionaryParameterRequestParameters;
        }
        catch {
            throw new lib.IncomingRequestParameterExtractionError();
        }
        if (this.validateIncomingParameters) {
            const invalidPaths = [...shared.validateFormDictionaryParameterRequestParameters(requestParameters)];
            if (invalidPaths.length > 0) {
                throw new lib.IncomingRequestParametersValidationError(invalidPaths);
            }
        }
        const requestAcceptTypes = lib.parseAcceptHeader(lib.getParameterValues(incomingRequest.headers, "accept"), shared.formDictionaryParameterAcceptTypes);
        const incomingRequestModel = {
            parameters: requestParameters,
            contentType: null
        };
        const outgoingResponseModel = await handler(incomingRequestModel, authorization, requestAcceptTypes);
        const responseHeaders: lib.Parameters = {};
        let outgoingResponse: lib.ServerOutgoingResponse;
        const responseStatus = outgoingResponseModel.status;
        switch (responseStatus) {
            case 204:
                outgoingResponse = {
                    headers: responseHeaders,
                    status: outgoingResponseModel.status
                };
                if (this.validateOutgoingParameters) {
                    const invalidPaths = [...shared.validateFormDictionaryParameter204ResponseResponseParameters(outgoingResponseModel.parameters)];
                    if (invalidPaths.length > 0) {
                        throw new lib.OutgoingResponseParametersValidationError(invalidPaths);
                    }
                }
                try {
                }
                catch {
                    throw new lib.OutgoingResponseParameterInjectionError();
                }
                break;
            default: throw new TypeError("status code not supported");
        }
        return outgoingResponse;
    }
    protected async formExplodeDictionaryParameter(route: Route, incomingRequest: lib.ServerIncomingRequest) {
        const handler = this.formExplodeDictionaryParameterOperationHandler;
        if (handler == null) {
            throw new lib.OperationNotRegisteredError("form-explode-dictionary-parameter");
        }
        const routeParameters = { ...route.parameters };
        const requestQuery = lib.parseParameters(incomingRequest.query);
        const requestHeaders = { ...incomingRequest.headers };
        const credentials = {};
        const authorization = await this.authorizeFormExplodeDictionaryParameter(credentials);
        if (authorization == null) {
            throw new lib.AuthorizationFailedError("form-explode-dictionary-parameter");
        }
        let requestParameters;
        try {
            requestParameters = {
                vAlue: internal.extractFormExplodeDictionaryParameterVAlueRequestParameter(requestQuery)
            } as shared.FormExplodeDictionaryParameterRequestParameters;
        }
        catch {
            throw new lib.IncomingRequestParameterExtractionError();
        }
        if (this.validateIncomingParameters) {
            const invalidPaths = [...shared.validateFormExplodeDictionaryParameterRequestParameters(requestParameters)];
            if (invalidPaths.length > 0) {
                throw new lib.IncomingRequestParametersValidationError(invalidPaths);
            }
        }
        const requestAcceptTypes = lib.parseAcceptHeader(lib.getParameterValues(incomingRequest.headers, "accept"), shared.formExplodeDictionaryParameterAcceptTypes);
        const incomingRequestModel = {
            parameters: requestParameters,
            contentType: null
        };
        const outgoingResponseModel = await handler(incomingRequestModel, authorization, requestAcceptTypes);
        const responseHeaders: lib.Parameters = {};
        let outgoingResponse: lib.ServerOutgoingResponse;
        const responseStatus = outgoingResponseModel.status;
        switch (responseStatus) {
            case 204:
                outgoingResponse = {
                    headers: responseHeaders,
                    status: outgoingResponseModel.status
                };
                if (this.validateOutgoingParameters) {
                    const invalidPaths = [...shared.validateFormExplodeDictionaryParameter204ResponseResponseParameters(outgoingResponseModel.parameters)];
                    if (invalidPaths.length > 0) {
                        throw new lib.OutgoingResponseParametersValidationError(invalidPaths);
                    }
                }
                try {
                }
                catch {
                    throw new lib.OutgoingResponseParameterInjectionError();
                }
                break;
            default: throw new TypeError("status code not supported");
        }
        return outgoingResponse;
    }
    protected async deepObjectObjectParameter(route: Route, incomingRequest: lib.ServerIncomingRequest) {
        const handler = this.deepObjectObjectParameterOperationHandler;
        if (handler == null) {
            throw new lib.OperationNotRegisteredError("deep-object-object-parameter");
        }
        const routeParameters = { ...route.parameters };
        const requestQuery = lib.parseParameters(incomingRequest.query);
        const requestHeaders = { ...incomingRequest.headers };
        const credentials = {};
        const authorization = await this.authorizeDeepObjectObjectParameter(credentials);
        if (authorization == null) {
            throw new lib.AuthorizationFailedError("deep-object-object-parameter");
        }
        let requestParameters;
        try {
            requestParameters = {
                vAlue: internal.extractDeepObjectObjectParameterVAlueRequestParameter(requestQuery)
            } as shared.DeepObjectObjectParameterRequestParameters;
        }
        catch {
            throw new lib.IncomingRequestParameterExtractionError();
        }
        if (this.validateIncomingParameters) {
            const invalidPaths = [...shared.validateDeepObjectObjectParameterRequestParameters(requestParameters)];
            if (invalidPaths.length > 0) {
                throw new lib.IncomingRequestParametersValidationError(invalidPaths);
            }
        }
        const requestAcceptTypes = lib.parseAcceptHeader(lib.getParameterValues(incomingRequest.headers, "accept"), shared.deepObjectObjectParameterAcceptTypes);
        const incomingRequestModel = {
            parameters: requestParameters,
            contentType: null
        };
        const outgoingResponseModel = await handler(incomingRequestModel, authorization, requestAcceptTypes);
        const responseHeaders: lib.Parameters = {};
        let outgoingResponse: lib.ServerOutgoingResponse;
        const responseStatus = outgoingResponseModel.status;
        switch (responseStatus) {
            case 204:
                outgoingResponse = {
                    headers: responseHeaders,
                    status: outgoingResponseModel.status
                };
                if (this.validateOutgoingParameters) {
                    const invalidPaths = [...shared.validateDeepObjectObjectParameter204ResponseResponseParameters(outgoingResponseModel.parameters)];
                    if (invalidPaths.length > 0) {
                        throw new lib.OutgoingResponseParametersValidationError(invalidPaths);
                    }
                }
                try {
                }
                catch {
                    throw new lib.OutgoingResponseParameterInjectionError();
                }
                break;
            default: throw new TypeError("status code not supported");
        }
        return outgoingResponse;
    }
    protected async deepObjectDictionaryParameter(route: Route, incomingRequest: lib.ServerIncomingRequest) {
        const handler = this.deepObjectDictionaryParameterOperationHandler;
        if (handler == null) {
            throw new lib.OperationNotRegisteredError("deep-object-dictionary-parameter");
        }
        const routeParameters = { ...route.parameters };
        const requestQuery = lib.parseParameters(incomingRequest.query);
        const requestHeaders = { ...incomingRequest.headers };
        const credentials = {};
        const authorization = await this.authorizeDeepObjectDictionaryParameter(credentials);
        if (authorization == null) {
            throw new lib.AuthorizationFailedError("deep-object-dictionary-parameter");
        }
        let requestParameters;
        try {
            requestParameters = {
                vAlue: internal.extractDeepObjectDictionaryParameterVAlueRequestParameter(requestQuery)
            } as shared.DeepObjectDictionaryParameterRequestParameters;
        }
        catch {
            throw new lib.IncomingRequestParameterExtractionError();
        }
        if (this.validateIncomingParameters) {
            const invalidPaths = [...shared.validateDeepObjectDictionaryParameterRequestParameters(requestParameters)];
            if (invalidPaths.length > 0) {
                throw new lib.IncomingRequestParametersValidationError(invalidPaths);
            }
        }
        const requestAcceptTypes = lib.parseAcceptHeader(lib.getParameterValues(incomingRequest.headers, "accept"), shared.deepObjectDictionaryParameterAcceptTypes);
        const incomingRequestModel = {
            parameters: requestParameters,
            contentType: null
        };
        const outgoingResponseModel = await handler(incomingRequestModel, authorization, requestAcceptTypes);
        const responseHeaders: lib.Parameters = {};
        let outgoingResponse: lib.ServerOutgoingResponse;
        const responseStatus = outgoingResponseModel.status;
        switch (responseStatus) {
            case 204:
                outgoingResponse = {
                    headers: responseHeaders,
                    status: outgoingResponseModel.status
                };
                if (this.validateOutgoingParameters) {
                    const invalidPaths = [...shared.validateDeepObjectDictionaryParameter204ResponseResponseParameters(outgoingResponseModel.parameters)];
                    if (invalidPaths.length > 0) {
                        throw new lib.OutgoingResponseParametersValidationError(invalidPaths);
                    }
                }
                try {
                }
                catch {
                    throw new lib.OutgoingResponseParameterInjectionError();
                }
                break;
            default: throw new TypeError("status code not supported");
        }
        return outgoingResponse;
    }
}
export type ServerAuthorization = Record<"basic" | "apiToken" | "apiKey" | "accessToken", unknown>;
export type SessionMetricsCredentials = object;
export type MetricsCredentials = object;
export type EchoCredentials = object;
export interface BasicAuthCredentials {
    basic?: shared.BasicCredential;
}
export type SimpleStringParameterCredentials = object;
export type SimpleNumberParameterCredentials = object;
export type SimpleArrayParameterCredentials = object;
export type SimpleExplodeArrayParameterCredentials = object;
export type SimpleObjectParameterCredentials = object;
export type SimpleExplodeObjectParameterCredentials = object;
export type SimpleDictionaryParameterCredentials = object;
export type SimpleExplodeDictionaryParameterCredentials = object;
export type LabelStringParameterCredentials = object;
export type LabelNumberParameterCredentials = object;
export type LabelArrayParameterCredentials = object;
export type LabelExplodeArrayParameterCredentials = object;
export type LabelObjectParameterCredentials = object;
export type LabelExplodeObjectParameterCredentials = object;
export type LabelDictionaryParameterCredentials = object;
export type LabelExplodeDictionaryParameterCredentials = object;
export type FormStringParameterCredentials = object;
export type FormNumberParameterCredentials = object;
export type FormArrayParameterCredentials = object;
export type FormExplodeArrayParameterCredentials = object;
export type FormObjectParameterCredentials = object;
export type FormExplodeObjectParameterCredentials = object;
export type FormDictionaryParameterCredentials = object;
export type FormExplodeDictionaryParameterCredentials = object;
export type DeepObjectObjectParameterCredentials = object;
export type DeepObjectDictionaryParameterCredentials = object;
export type SessionMetricsAuthorization<Authorization extends ServerAuthorization = ServerAuthorization> = object;
export type MetricsAuthorization<Authorization extends ServerAuthorization = ServerAuthorization> = object;
export type EchoAuthorization<Authorization extends ServerAuthorization = ServerAuthorization> = object;
export type BasicAuthAuthorization<Authorization extends ServerAuthorization = ServerAuthorization> = Pick<Authorization, "basic">;
export type SimpleStringParameterAuthorization<Authorization extends ServerAuthorization = ServerAuthorization> = object;
export type SimpleNumberParameterAuthorization<Authorization extends ServerAuthorization = ServerAuthorization> = object;
export type SimpleArrayParameterAuthorization<Authorization extends ServerAuthorization = ServerAuthorization> = object;
export type SimpleExplodeArrayParameterAuthorization<Authorization extends ServerAuthorization = ServerAuthorization> = object;
export type SimpleObjectParameterAuthorization<Authorization extends ServerAuthorization = ServerAuthorization> = object;
export type SimpleExplodeObjectParameterAuthorization<Authorization extends ServerAuthorization = ServerAuthorization> = object;
export type SimpleDictionaryParameterAuthorization<Authorization extends ServerAuthorization = ServerAuthorization> = object;
export type SimpleExplodeDictionaryParameterAuthorization<Authorization extends ServerAuthorization = ServerAuthorization> = object;
export type LabelStringParameterAuthorization<Authorization extends ServerAuthorization = ServerAuthorization> = object;
export type LabelNumberParameterAuthorization<Authorization extends ServerAuthorization = ServerAuthorization> = object;
export type LabelArrayParameterAuthorization<Authorization extends ServerAuthorization = ServerAuthorization> = object;
export type LabelExplodeArrayParameterAuthorization<Authorization extends ServerAuthorization = ServerAuthorization> = object;
export type LabelObjectParameterAuthorization<Authorization extends ServerAuthorization = ServerAuthorization> = object;
export type LabelExplodeObjectParameterAuthorization<Authorization extends ServerAuthorization = ServerAuthorization> = object;
export type LabelDictionaryParameterAuthorization<Authorization extends ServerAuthorization = ServerAuthorization> = object;
export type LabelExplodeDictionaryParameterAuthorization<Authorization extends ServerAuthorization = ServerAuthorization> = object;
export type FormStringParameterAuthorization<Authorization extends ServerAuthorization = ServerAuthorization> = object;
export type FormNumberParameterAuthorization<Authorization extends ServerAuthorization = ServerAuthorization> = object;
export type FormArrayParameterAuthorization<Authorization extends ServerAuthorization = ServerAuthorization> = object;
export type FormExplodeArrayParameterAuthorization<Authorization extends ServerAuthorization = ServerAuthorization> = object;
export type FormObjectParameterAuthorization<Authorization extends ServerAuthorization = ServerAuthorization> = object;
export type FormExplodeObjectParameterAuthorization<Authorization extends ServerAuthorization = ServerAuthorization> = object;
export type FormDictionaryParameterAuthorization<Authorization extends ServerAuthorization = ServerAuthorization> = object;
export type FormExplodeDictionaryParameterAuthorization<Authorization extends ServerAuthorization = ServerAuthorization> = object;
export type DeepObjectObjectParameterAuthorization<Authorization extends ServerAuthorization = ServerAuthorization> = object;
export type DeepObjectDictionaryParameterAuthorization<Authorization extends ServerAuthorization = ServerAuthorization> = object;
export type SessionMetricsIncomingRequest = lib.IncomingEmptyRequest<shared.SessionMetricsRequestParameters>;
export type MetricsIncomingRequest = lib.IncomingEmptyRequest<shared.MetricsRequestParameters>;
export type EchoIncomingRequest = lib.IncomingJsonRequest<shared.EchoRequestParameters, "application/json", shared.EchoMessageSchema> | lib.IncomingFormRequest<shared.EchoRequestParameters, "application/x-www-form-urlencoded", shared.EchoMessageSchema> | lib.IncomingTextRequest<shared.EchoRequestParameters, "text/plain"> | lib.IncomingStreamRequest<shared.EchoRequestParameters, "application/octet-stream">;
export type BasicAuthIncomingRequest = lib.IncomingEmptyRequest<shared.BasicAuthRequestParameters>;
export type SimpleStringParameterIncomingRequest = lib.IncomingEmptyRequest<shared.SimpleStringParameterRequestParameters>;
export type SimpleNumberParameterIncomingRequest = lib.IncomingEmptyRequest<shared.SimpleNumberParameterRequestParameters>;
export type SimpleArrayParameterIncomingRequest = lib.IncomingEmptyRequest<shared.SimpleArrayParameterRequestParameters>;
export type SimpleExplodeArrayParameterIncomingRequest = lib.IncomingEmptyRequest<shared.SimpleExplodeArrayParameterRequestParameters>;
export type SimpleObjectParameterIncomingRequest = lib.IncomingEmptyRequest<shared.SimpleObjectParameterRequestParameters>;
export type SimpleExplodeObjectParameterIncomingRequest = lib.IncomingEmptyRequest<shared.SimpleExplodeObjectParameterRequestParameters>;
export type SimpleDictionaryParameterIncomingRequest = lib.IncomingEmptyRequest<shared.SimpleDictionaryParameterRequestParameters>;
export type SimpleExplodeDictionaryParameterIncomingRequest = lib.IncomingEmptyRequest<shared.SimpleExplodeDictionaryParameterRequestParameters>;
export type LabelStringParameterIncomingRequest = lib.IncomingEmptyRequest<shared.LabelStringParameterRequestParameters>;
export type LabelNumberParameterIncomingRequest = lib.IncomingEmptyRequest<shared.LabelNumberParameterRequestParameters>;
export type LabelArrayParameterIncomingRequest = lib.IncomingEmptyRequest<shared.LabelArrayParameterRequestParameters>;
export type LabelExplodeArrayParameterIncomingRequest = lib.IncomingEmptyRequest<shared.LabelExplodeArrayParameterRequestParameters>;
export type LabelObjectParameterIncomingRequest = lib.IncomingEmptyRequest<shared.LabelObjectParameterRequestParameters>;
export type LabelExplodeObjectParameterIncomingRequest = lib.IncomingEmptyRequest<shared.LabelExplodeObjectParameterRequestParameters>;
export type LabelDictionaryParameterIncomingRequest = lib.IncomingEmptyRequest<shared.LabelDictionaryParameterRequestParameters>;
export type LabelExplodeDictionaryParameterIncomingRequest = lib.IncomingEmptyRequest<shared.LabelExplodeDictionaryParameterRequestParameters>;
export type FormStringParameterIncomingRequest = lib.IncomingEmptyRequest<shared.FormStringParameterRequestParameters>;
export type FormNumberParameterIncomingRequest = lib.IncomingEmptyRequest<shared.FormNumberParameterRequestParameters>;
export type FormArrayParameterIncomingRequest = lib.IncomingEmptyRequest<shared.FormArrayParameterRequestParameters>;
export type FormExplodeArrayParameterIncomingRequest = lib.IncomingEmptyRequest<shared.FormExplodeArrayParameterRequestParameters>;
export type FormObjectParameterIncomingRequest = lib.IncomingEmptyRequest<shared.FormObjectParameterRequestParameters>;
export type FormExplodeObjectParameterIncomingRequest = lib.IncomingEmptyRequest<shared.FormExplodeObjectParameterRequestParameters>;
export type FormDictionaryParameterIncomingRequest = lib.IncomingEmptyRequest<shared.FormDictionaryParameterRequestParameters>;
export type FormExplodeDictionaryParameterIncomingRequest = lib.IncomingEmptyRequest<shared.FormExplodeDictionaryParameterRequestParameters>;
export type DeepObjectObjectParameterIncomingRequest = lib.IncomingEmptyRequest<shared.DeepObjectObjectParameterRequestParameters>;
export type DeepObjectDictionaryParameterIncomingRequest = lib.IncomingEmptyRequest<shared.DeepObjectDictionaryParameterRequestParameters>;
export type SessionMetricsOutgoingResponse = lib.OutgoingJsonResponseDefault<200, shared.SessionMetrics200ResponseResponseParameters, shared.SessionMetrics200ApplicationJsonResponseSchema> | lib.OutgoingJsonResponse<200, shared.SessionMetrics200ResponseResponseParameters, "application/json", shared.SessionMetrics200ApplicationJsonResponseSchema>;
export type MetricsOutgoingResponse = lib.OutgoingTextResponseDefault<200, shared.Metrics200ResponseResponseParameters> | lib.OutgoingTextResponse<200, shared.Metrics200ResponseResponseParameters, "text/plain">;
export type EchoOutgoingResponse = lib.OutgoingJsonResponseDefault<200, shared.Echo200ResponseResponseParameters, shared.EchoMessageSchema> | lib.OutgoingJsonResponse<200, shared.Echo200ResponseResponseParameters, "application/json", shared.EchoMessageSchema> | lib.OutgoingFormResponse<200, shared.Echo200ResponseResponseParameters, "application/x-www-form-urlencoded", shared.EchoMessageSchema> | lib.OutgoingTextResponse<200, shared.Echo200ResponseResponseParameters, "text/plain"> | lib.OutgoingStreamResponse<200, shared.Echo200ResponseResponseParameters, "application/octet-stream">;
export type BasicAuthOutgoingResponse = lib.OutgoingEmptyResponseDefault<204, shared.BasicAuth204ResponseResponseParameters> | lib.OutgoingEmptyResponse<204, shared.BasicAuth204ResponseResponseParameters>;
export type SimpleStringParameterOutgoingResponse = lib.OutgoingEmptyResponseDefault<204, shared.SimpleStringParameter204ResponseResponseParameters> | lib.OutgoingEmptyResponse<204, shared.SimpleStringParameter204ResponseResponseParameters>;
export type SimpleNumberParameterOutgoingResponse = lib.OutgoingEmptyResponseDefault<204, shared.SimpleNumberParameter204ResponseResponseParameters> | lib.OutgoingEmptyResponse<204, shared.SimpleNumberParameter204ResponseResponseParameters>;
export type SimpleArrayParameterOutgoingResponse = lib.OutgoingEmptyResponseDefault<204, shared.SimpleArrayParameter204ResponseResponseParameters> | lib.OutgoingEmptyResponse<204, shared.SimpleArrayParameter204ResponseResponseParameters>;
export type SimpleExplodeArrayParameterOutgoingResponse = lib.OutgoingEmptyResponseDefault<204, shared.SimpleExplodeArrayParameter204ResponseResponseParameters> | lib.OutgoingEmptyResponse<204, shared.SimpleExplodeArrayParameter204ResponseResponseParameters>;
export type SimpleObjectParameterOutgoingResponse = lib.OutgoingEmptyResponseDefault<204, shared.SimpleObjectParameter204ResponseResponseParameters> | lib.OutgoingEmptyResponse<204, shared.SimpleObjectParameter204ResponseResponseParameters>;
export type SimpleExplodeObjectParameterOutgoingResponse = lib.OutgoingEmptyResponseDefault<204, shared.SimpleExplodeObjectParameter204ResponseResponseParameters> | lib.OutgoingEmptyResponse<204, shared.SimpleExplodeObjectParameter204ResponseResponseParameters>;
export type SimpleDictionaryParameterOutgoingResponse = lib.OutgoingEmptyResponseDefault<204, shared.SimpleDictionaryParameter204ResponseResponseParameters> | lib.OutgoingEmptyResponse<204, shared.SimpleDictionaryParameter204ResponseResponseParameters>;
export type SimpleExplodeDictionaryParameterOutgoingResponse = lib.OutgoingEmptyResponseDefault<204, shared.SimpleExplodeDictionaryParameter204ResponseResponseParameters> | lib.OutgoingEmptyResponse<204, shared.SimpleExplodeDictionaryParameter204ResponseResponseParameters>;
export type LabelStringParameterOutgoingResponse = lib.OutgoingEmptyResponseDefault<204, shared.LabelStringParameter204ResponseResponseParameters> | lib.OutgoingEmptyResponse<204, shared.LabelStringParameter204ResponseResponseParameters>;
export type LabelNumberParameterOutgoingResponse = lib.OutgoingEmptyResponseDefault<204, shared.LabelNumberParameter204ResponseResponseParameters> | lib.OutgoingEmptyResponse<204, shared.LabelNumberParameter204ResponseResponseParameters>;
export type LabelArrayParameterOutgoingResponse = lib.OutgoingEmptyResponseDefault<204, shared.LabelArrayParameter204ResponseResponseParameters> | lib.OutgoingEmptyResponse<204, shared.LabelArrayParameter204ResponseResponseParameters>;
export type LabelExplodeArrayParameterOutgoingResponse = lib.OutgoingEmptyResponseDefault<204, shared.LabelExplodeArrayParameter204ResponseResponseParameters> | lib.OutgoingEmptyResponse<204, shared.LabelExplodeArrayParameter204ResponseResponseParameters>;
export type LabelObjectParameterOutgoingResponse = lib.OutgoingEmptyResponseDefault<204, shared.LabelObjectParameter204ResponseResponseParameters> | lib.OutgoingEmptyResponse<204, shared.LabelObjectParameter204ResponseResponseParameters>;
export type LabelExplodeObjectParameterOutgoingResponse = lib.OutgoingEmptyResponseDefault<204, shared.LabelExplodeObjectParameter204ResponseResponseParameters> | lib.OutgoingEmptyResponse<204, shared.LabelExplodeObjectParameter204ResponseResponseParameters>;
export type LabelDictionaryParameterOutgoingResponse = lib.OutgoingEmptyResponseDefault<204, shared.LabelDictionaryParameter204ResponseResponseParameters> | lib.OutgoingEmptyResponse<204, shared.LabelDictionaryParameter204ResponseResponseParameters>;
export type LabelExplodeDictionaryParameterOutgoingResponse = lib.OutgoingEmptyResponseDefault<204, shared.LabelExplodeDictionaryParameter204ResponseResponseParameters> | lib.OutgoingEmptyResponse<204, shared.LabelExplodeDictionaryParameter204ResponseResponseParameters>;
export type FormStringParameterOutgoingResponse = lib.OutgoingEmptyResponseDefault<204, shared.FormStringParameter204ResponseResponseParameters> | lib.OutgoingEmptyResponse<204, shared.FormStringParameter204ResponseResponseParameters>;
export type FormNumberParameterOutgoingResponse = lib.OutgoingEmptyResponseDefault<204, shared.FormNumberParameter204ResponseResponseParameters> | lib.OutgoingEmptyResponse<204, shared.FormNumberParameter204ResponseResponseParameters>;
export type FormArrayParameterOutgoingResponse = lib.OutgoingEmptyResponseDefault<204, shared.FormArrayParameter204ResponseResponseParameters> | lib.OutgoingEmptyResponse<204, shared.FormArrayParameter204ResponseResponseParameters>;
export type FormExplodeArrayParameterOutgoingResponse = lib.OutgoingEmptyResponseDefault<204, shared.FormExplodeArrayParameter204ResponseResponseParameters> | lib.OutgoingEmptyResponse<204, shared.FormExplodeArrayParameter204ResponseResponseParameters>;
export type FormObjectParameterOutgoingResponse = lib.OutgoingEmptyResponseDefault<204, shared.FormObjectParameter204ResponseResponseParameters> | lib.OutgoingEmptyResponse<204, shared.FormObjectParameter204ResponseResponseParameters>;
export type FormExplodeObjectParameterOutgoingResponse = lib.OutgoingEmptyResponseDefault<204, shared.FormExplodeObjectParameter204ResponseResponseParameters> | lib.OutgoingEmptyResponse<204, shared.FormExplodeObjectParameter204ResponseResponseParameters>;
export type FormDictionaryParameterOutgoingResponse = lib.OutgoingEmptyResponseDefault<204, shared.FormDictionaryParameter204ResponseResponseParameters> | lib.OutgoingEmptyResponse<204, shared.FormDictionaryParameter204ResponseResponseParameters>;
export type FormExplodeDictionaryParameterOutgoingResponse = lib.OutgoingEmptyResponseDefault<204, shared.FormExplodeDictionaryParameter204ResponseResponseParameters> | lib.OutgoingEmptyResponse<204, shared.FormExplodeDictionaryParameter204ResponseResponseParameters>;
export type DeepObjectObjectParameterOutgoingResponse = lib.OutgoingEmptyResponseDefault<204, shared.DeepObjectObjectParameter204ResponseResponseParameters> | lib.OutgoingEmptyResponse<204, shared.DeepObjectObjectParameter204ResponseResponseParameters>;
export type DeepObjectDictionaryParameterOutgoingResponse = lib.OutgoingEmptyResponseDefault<204, shared.DeepObjectDictionaryParameter204ResponseResponseParameters> | lib.OutgoingEmptyResponse<204, shared.DeepObjectDictionaryParameter204ResponseResponseParameters>;
export type SessionMetricsOperationHandler<Authorization extends ServerAuthorization = ServerAuthorization> = (incomingRequest: SessionMetricsIncomingRequest, authorization: SessionMetricsAuthorization<Authorization>, acceptTypes: shared.SessionMetricsAcceptTypes[]) => Promisable<SessionMetricsOutgoingResponse>;
export type MetricsOperationHandler<Authorization extends ServerAuthorization = ServerAuthorization> = (incomingRequest: MetricsIncomingRequest, authorization: MetricsAuthorization<Authorization>, acceptTypes: shared.MetricsAcceptTypes[]) => Promisable<MetricsOutgoingResponse>;
export type EchoOperationHandler<Authorization extends ServerAuthorization = ServerAuthorization> = (incomingRequest: EchoIncomingRequest, authorization: EchoAuthorization<Authorization>, acceptTypes: shared.EchoAcceptTypes[]) => Promisable<EchoOutgoingResponse>;
export type BasicAuthOperationHandler<Authorization extends ServerAuthorization = ServerAuthorization> = (incomingRequest: BasicAuthIncomingRequest, authorization: BasicAuthAuthorization<Authorization>, acceptTypes: shared.BasicAuthAcceptTypes[]) => Promisable<BasicAuthOutgoingResponse>;
export type SimpleStringParameterOperationHandler<Authorization extends ServerAuthorization = ServerAuthorization> = (incomingRequest: SimpleStringParameterIncomingRequest, authorization: SimpleStringParameterAuthorization<Authorization>, acceptTypes: shared.SimpleStringParameterAcceptTypes[]) => Promisable<SimpleStringParameterOutgoingResponse>;
export type SimpleNumberParameterOperationHandler<Authorization extends ServerAuthorization = ServerAuthorization> = (incomingRequest: SimpleNumberParameterIncomingRequest, authorization: SimpleNumberParameterAuthorization<Authorization>, acceptTypes: shared.SimpleNumberParameterAcceptTypes[]) => Promisable<SimpleNumberParameterOutgoingResponse>;
export type SimpleArrayParameterOperationHandler<Authorization extends ServerAuthorization = ServerAuthorization> = (incomingRequest: SimpleArrayParameterIncomingRequest, authorization: SimpleArrayParameterAuthorization<Authorization>, acceptTypes: shared.SimpleArrayParameterAcceptTypes[]) => Promisable<SimpleArrayParameterOutgoingResponse>;
export type SimpleExplodeArrayParameterOperationHandler<Authorization extends ServerAuthorization = ServerAuthorization> = (incomingRequest: SimpleExplodeArrayParameterIncomingRequest, authorization: SimpleExplodeArrayParameterAuthorization<Authorization>, acceptTypes: shared.SimpleExplodeArrayParameterAcceptTypes[]) => Promisable<SimpleExplodeArrayParameterOutgoingResponse>;
export type SimpleObjectParameterOperationHandler<Authorization extends ServerAuthorization = ServerAuthorization> = (incomingRequest: SimpleObjectParameterIncomingRequest, authorization: SimpleObjectParameterAuthorization<Authorization>, acceptTypes: shared.SimpleObjectParameterAcceptTypes[]) => Promisable<SimpleObjectParameterOutgoingResponse>;
export type SimpleExplodeObjectParameterOperationHandler<Authorization extends ServerAuthorization = ServerAuthorization> = (incomingRequest: SimpleExplodeObjectParameterIncomingRequest, authorization: SimpleExplodeObjectParameterAuthorization<Authorization>, acceptTypes: shared.SimpleExplodeObjectParameterAcceptTypes[]) => Promisable<SimpleExplodeObjectParameterOutgoingResponse>;
export type SimpleDictionaryParameterOperationHandler<Authorization extends ServerAuthorization = ServerAuthorization> = (incomingRequest: SimpleDictionaryParameterIncomingRequest, authorization: SimpleDictionaryParameterAuthorization<Authorization>, acceptTypes: shared.SimpleDictionaryParameterAcceptTypes[]) => Promisable<SimpleDictionaryParameterOutgoingResponse>;
export type SimpleExplodeDictionaryParameterOperationHandler<Authorization extends ServerAuthorization = ServerAuthorization> = (incomingRequest: SimpleExplodeDictionaryParameterIncomingRequest, authorization: SimpleExplodeDictionaryParameterAuthorization<Authorization>, acceptTypes: shared.SimpleExplodeDictionaryParameterAcceptTypes[]) => Promisable<SimpleExplodeDictionaryParameterOutgoingResponse>;
export type LabelStringParameterOperationHandler<Authorization extends ServerAuthorization = ServerAuthorization> = (incomingRequest: LabelStringParameterIncomingRequest, authorization: LabelStringParameterAuthorization<Authorization>, acceptTypes: shared.LabelStringParameterAcceptTypes[]) => Promisable<LabelStringParameterOutgoingResponse>;
export type LabelNumberParameterOperationHandler<Authorization extends ServerAuthorization = ServerAuthorization> = (incomingRequest: LabelNumberParameterIncomingRequest, authorization: LabelNumberParameterAuthorization<Authorization>, acceptTypes: shared.LabelNumberParameterAcceptTypes[]) => Promisable<LabelNumberParameterOutgoingResponse>;
export type LabelArrayParameterOperationHandler<Authorization extends ServerAuthorization = ServerAuthorization> = (incomingRequest: LabelArrayParameterIncomingRequest, authorization: LabelArrayParameterAuthorization<Authorization>, acceptTypes: shared.LabelArrayParameterAcceptTypes[]) => Promisable<LabelArrayParameterOutgoingResponse>;
export type LabelExplodeArrayParameterOperationHandler<Authorization extends ServerAuthorization = ServerAuthorization> = (incomingRequest: LabelExplodeArrayParameterIncomingRequest, authorization: LabelExplodeArrayParameterAuthorization<Authorization>, acceptTypes: shared.LabelExplodeArrayParameterAcceptTypes[]) => Promisable<LabelExplodeArrayParameterOutgoingResponse>;
export type LabelObjectParameterOperationHandler<Authorization extends ServerAuthorization = ServerAuthorization> = (incomingRequest: LabelObjectParameterIncomingRequest, authorization: LabelObjectParameterAuthorization<Authorization>, acceptTypes: shared.LabelObjectParameterAcceptTypes[]) => Promisable<LabelObjectParameterOutgoingResponse>;
export type LabelExplodeObjectParameterOperationHandler<Authorization extends ServerAuthorization = ServerAuthorization> = (incomingRequest: LabelExplodeObjectParameterIncomingRequest, authorization: LabelExplodeObjectParameterAuthorization<Authorization>, acceptTypes: shared.LabelExplodeObjectParameterAcceptTypes[]) => Promisable<LabelExplodeObjectParameterOutgoingResponse>;
export type LabelDictionaryParameterOperationHandler<Authorization extends ServerAuthorization = ServerAuthorization> = (incomingRequest: LabelDictionaryParameterIncomingRequest, authorization: LabelDictionaryParameterAuthorization<Authorization>, acceptTypes: shared.LabelDictionaryParameterAcceptTypes[]) => Promisable<LabelDictionaryParameterOutgoingResponse>;
export type LabelExplodeDictionaryParameterOperationHandler<Authorization extends ServerAuthorization = ServerAuthorization> = (incomingRequest: LabelExplodeDictionaryParameterIncomingRequest, authorization: LabelExplodeDictionaryParameterAuthorization<Authorization>, acceptTypes: shared.LabelExplodeDictionaryParameterAcceptTypes[]) => Promisable<LabelExplodeDictionaryParameterOutgoingResponse>;
export type FormStringParameterOperationHandler<Authorization extends ServerAuthorization = ServerAuthorization> = (incomingRequest: FormStringParameterIncomingRequest, authorization: FormStringParameterAuthorization<Authorization>, acceptTypes: shared.FormStringParameterAcceptTypes[]) => Promisable<FormStringParameterOutgoingResponse>;
export type FormNumberParameterOperationHandler<Authorization extends ServerAuthorization = ServerAuthorization> = (incomingRequest: FormNumberParameterIncomingRequest, authorization: FormNumberParameterAuthorization<Authorization>, acceptTypes: shared.FormNumberParameterAcceptTypes[]) => Promisable<FormNumberParameterOutgoingResponse>;
export type FormArrayParameterOperationHandler<Authorization extends ServerAuthorization = ServerAuthorization> = (incomingRequest: FormArrayParameterIncomingRequest, authorization: FormArrayParameterAuthorization<Authorization>, acceptTypes: shared.FormArrayParameterAcceptTypes[]) => Promisable<FormArrayParameterOutgoingResponse>;
export type FormExplodeArrayParameterOperationHandler<Authorization extends ServerAuthorization = ServerAuthorization> = (incomingRequest: FormExplodeArrayParameterIncomingRequest, authorization: FormExplodeArrayParameterAuthorization<Authorization>, acceptTypes: shared.FormExplodeArrayParameterAcceptTypes[]) => Promisable<FormExplodeArrayParameterOutgoingResponse>;
export type FormObjectParameterOperationHandler<Authorization extends ServerAuthorization = ServerAuthorization> = (incomingRequest: FormObjectParameterIncomingRequest, authorization: FormObjectParameterAuthorization<Authorization>, acceptTypes: shared.FormObjectParameterAcceptTypes[]) => Promisable<FormObjectParameterOutgoingResponse>;
export type FormExplodeObjectParameterOperationHandler<Authorization extends ServerAuthorization = ServerAuthorization> = (incomingRequest: FormExplodeObjectParameterIncomingRequest, authorization: FormExplodeObjectParameterAuthorization<Authorization>, acceptTypes: shared.FormExplodeObjectParameterAcceptTypes[]) => Promisable<FormExplodeObjectParameterOutgoingResponse>;
export type FormDictionaryParameterOperationHandler<Authorization extends ServerAuthorization = ServerAuthorization> = (incomingRequest: FormDictionaryParameterIncomingRequest, authorization: FormDictionaryParameterAuthorization<Authorization>, acceptTypes: shared.FormDictionaryParameterAcceptTypes[]) => Promisable<FormDictionaryParameterOutgoingResponse>;
export type FormExplodeDictionaryParameterOperationHandler<Authorization extends ServerAuthorization = ServerAuthorization> = (incomingRequest: FormExplodeDictionaryParameterIncomingRequest, authorization: FormExplodeDictionaryParameterAuthorization<Authorization>, acceptTypes: shared.FormExplodeDictionaryParameterAcceptTypes[]) => Promisable<FormExplodeDictionaryParameterOutgoingResponse>;
export type DeepObjectObjectParameterOperationHandler<Authorization extends ServerAuthorization = ServerAuthorization> = (incomingRequest: DeepObjectObjectParameterIncomingRequest, authorization: DeepObjectObjectParameterAuthorization<Authorization>, acceptTypes: shared.DeepObjectObjectParameterAcceptTypes[]) => Promisable<DeepObjectObjectParameterOutgoingResponse>;
export type DeepObjectDictionaryParameterOperationHandler<Authorization extends ServerAuthorization = ServerAuthorization> = (incomingRequest: DeepObjectDictionaryParameterIncomingRequest, authorization: DeepObjectDictionaryParameterAuthorization<Authorization>, acceptTypes: shared.DeepObjectDictionaryParameterAcceptTypes[]) => Promisable<DeepObjectDictionaryParameterOutgoingResponse>;
export type BasicAuthorizationHandler<Authorization extends ServerAuthorization = ServerAuthorization> = (credential: shared.BasicCredential) => Promisable<Authorization["basic"] | void>;
export type ApiTokenAuthorizationHandler<Authorization extends ServerAuthorization = ServerAuthorization> = (credential: shared.ApiTokenCredential) => Promisable<Authorization["apiToken"] | void>;
export type ApiKeyAuthorizationHandler<Authorization extends ServerAuthorization = ServerAuthorization> = (credential: shared.ApiKeyCredential) => Promisable<Authorization["apiKey"] | void>;
export type AccessTokenAuthorizationHandler<Authorization extends ServerAuthorization = ServerAuthorization> = (credential: shared.AccessTokenCredential) => Promisable<Authorization["accessToken"] | void>;
