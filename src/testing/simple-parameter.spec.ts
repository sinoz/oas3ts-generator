import test from "tape-promise/tape.js";
import { injectSimpleArrayParameterVAlueRequestParameter, injectSimpleDictionaryParameterVAlueRequestParameter, injectSimpleExplodeArrayParameterVAlueRequestParameter, injectSimpleExplodeDictionaryParameterVAlueRequestParameter, injectSimpleExplodeObjectParameterVAlueRequestParameter, injectSimpleNumberParameterVAlueRequestParameter, injectSimpleObjectParameterVAlueRequestParameter, injectSimpleStringParameterVAlueRequestParameter } from "./package/src/client-internal.js";
import { extractSimpleArrayParameterVAlueRequestParameter, extractSimpleDictionaryParameterVAlueRequestParameter, extractSimpleExplodeArrayParameterVAlueRequestParameter, extractSimpleExplodeDictionaryParameterVAlueRequestParameter, extractSimpleExplodeObjectParameterVAlueRequestParameter, extractSimpleNumberParameterVAlueRequestParameter, extractSimpleObjectParameterVAlueRequestParameter, extractSimpleStringParameterVAlueRequestParameter } from "./package/src/server-internal.js";

test("inject simple string", async t => {
    const parameters = {};
    const value = ":/?#[]@!$&'()*+ ";
    injectSimpleStringParameterVAlueRequestParameter(parameters, value);

    t.deepEqual(parameters, {
        "v-alue": ":/?#[]@!$&'()*+ ",
    });
});

test("extract simple string", async t => {
    const parameters = {
        "v-alue": ":/?#[]@!$&'()*+ ",
    };
    const value = extractSimpleStringParameterVAlueRequestParameter(parameters);

    t.deepEqual(parameters, {});
    t.deepEqual(value, ":/?#[]@!$&'()*+ ");
});

test("inject simple number", async t => {
    const parameters = {};
    const value = 10;
    injectSimpleNumberParameterVAlueRequestParameter(parameters, value);

    t.deepEqual(parameters, {
        "v-alue": "10",
    });
});

test("extract simple number", async t => {
    const parameters = {
        "v-alue": "10",
    };
    const value = extractSimpleNumberParameterVAlueRequestParameter(parameters);

    t.deepEqual(parameters, {});
    t.deepEqual(value, 10);
});

test("inject simple array", async t => {
    const parameters = {};
    const value = [
        ":/?#[]@!$&'()*+ ",
        "2",
    ];
    injectSimpleArrayParameterVAlueRequestParameter(parameters, value);

    t.deepEqual(parameters, {
        "v-alue": ":/?#[]@!$&'()*+ ,2",
    });
});

test("extract simple array", async t => {
    const parameters = {
        "v-alue": ":/?#[]@!$&'()*+ ,2",
    };
    const value = extractSimpleArrayParameterVAlueRequestParameter(parameters);

    t.deepEqual(parameters, {});
    t.deepEqual(value, [
        ":/?#[]@!$&'()*+ ",
        "2",
    ]);
});

test("inject simple-explode array", async t => {
    const parameters = {};
    const value = [
        ":/?#[]@!$&'()*+ ",
        "2",
    ];
    injectSimpleExplodeArrayParameterVAlueRequestParameter(parameters, value);

    t.deepEqual(parameters, {
        "v-alue": ":/?#[]@!$&'()*+ ,2",
    });
});

test("extract simple-explode array", async t => {
    const parameters = {
        "v-alue": ":/?#[]@!$&'()*+ ,2",
    };
    const value = extractSimpleExplodeArrayParameterVAlueRequestParameter(parameters);

    t.deepEqual(parameters, {});
    t.deepEqual(value, [
        ":/?#[]@!$&'()*+ ",
        "2",
    ]);
});

test("inject simple object", async t => {
    const parameters = {};
    const value = {
        str: ":/?#[]@!$&'()*+ ",
        num: 2,
    };
    injectSimpleObjectParameterVAlueRequestParameter(parameters, value);

    t.deepEqual(parameters, {
        "v-alue": "str,:/?#[]@!$&'()*+ ,num,2",
    });
});

test("extract simple object", async t => {
    const parameters = {
        "v-alue": "str,:/?#[]@!$&'()*+ ,num,2",
    };
    const value = extractSimpleObjectParameterVAlueRequestParameter(parameters);

    t.deepEqual(parameters, {});
    t.deepEqual(value, {
        str: ":/?#[]@!$&'()*+ ",
        num: 2,
    });
});

test("inject simple-explode object", async t => {
    const parameters = {};
    const value = {
        str: ":/?#[]@!$&'()*+ ",
        num: 2,
    };
    injectSimpleExplodeObjectParameterVAlueRequestParameter(parameters, value);

    t.deepEqual(parameters, {
        "v-alue": "str=:/?#[]@!$&'()*+ ,num=2",
    });
});

test("extract simple-explode object", async t => {
    const parameters = {
        "v-alue": "str=:/?#[]@!$&'()*+ ,num=2",
    };
    const value = extractSimpleExplodeObjectParameterVAlueRequestParameter(parameters);

    t.deepEqual(parameters, {});
    t.deepEqual(value, {
        str: ":/?#[]@!$&'()*+ ",
        num: 2,
    });
});

test("inject simple dictionary", async t => {
    const parameters = {};
    const value = {
        a: ":/?#[]@!$&'()*+ ",
        b: "2",
    };
    injectSimpleDictionaryParameterVAlueRequestParameter(parameters, value);

    t.deepEqual(parameters, {
        "v-alue": "a,:/?#[]@!$&'()*+ ,b,2",
    });
});

test("extract simple dictionary", async t => {
    const parameters = {
        "v-alue": "a,:/?#[]@!$&'()*+ ,b,2",
    };
    const value = extractSimpleDictionaryParameterVAlueRequestParameter(parameters);

    t.deepEqual(parameters, {});
    t.deepEqual(value, {
        a: ":/?#[]@!$&'()*+ ",
        b: "2",
    });
});

test("inject simple-explode dictionary", async t => {
    const parameters = {};
    const value = {
        a: ":/?#[]@!$&'()*+ ",
        b: "2",
    };
    injectSimpleExplodeDictionaryParameterVAlueRequestParameter(parameters, value);

    t.deepEqual(parameters, {
        "v-alue": "a=:/?#[]@!$&'()*+ ,b=2",
    });
});

test("extract simple-explode dictionary", async t => {
    const parameters = {
        "v-alue": "a=:/?#[]@!$&'()*+ ,b=2",
    };
    const value = extractSimpleExplodeDictionaryParameterVAlueRequestParameter(parameters);

    t.deepEqual(parameters, {});
    t.deepEqual(value, {
        a: ":/?#[]@!$&'()*+ ",
        b: "2",
    });
});
