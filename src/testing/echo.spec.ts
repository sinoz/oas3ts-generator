import assert from "assert";
import test from "tape-promise/tape.js";
import { withContext } from "./context.js";

test("echo", t => withContext(async context => {
    initializeMocks();

    const client = context.createClient();

    const echoResult = await client.echo({
        parameters: {},
        contentType: "application/json",
        entity() {
            return { message: "hi" };
        },
    });
    assert(echoResult.status === 200);
    assert(echoResult.contentType === "application/json");

    const echoEntity = await echoResult.entity();
    t.equal(
        echoEntity.message,
        "hi",
    );

    function initializeMocks() {
        context.server.registerEchoOperation(async incomingRequest => {
            assert(incomingRequest.contentType === "application/json");

            const entity = await incomingRequest.entity();
            return {
                status: 200,
                parameters: {},
                contentType: "application/json",
                entity() {
                    return entity;
                },
            };
        });
    }
}));

